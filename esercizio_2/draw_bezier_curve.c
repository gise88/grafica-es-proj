#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "../GCGraLib2/GCGraLib2.h"

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define DIM 200
#define DISTANCE_THRESHOLD 25.0
#define DEFAULT_PTSIZE  15

#define WINDOWS_WIDTH 800
#define WINDOWS_HEIGHT 600

double evaluate_casteljau(int values[], int n, double t) {
	int i, j;
	double b[DIM][DIM];

	for (i = 0; i < n; i++) {
		b[0][i] = (double) values[i];
	}
	for (j = 1; j < n; j++) {
		for (i = 0; i < n - j; i++) {
			b[j][i] = b[j - 1][i] * (1 - t) + b[j - 1][i + 1] * t;
		}
	}
	return (b[n - 1][0]);
}

void draw_casteljau(SDL_Renderer *ren, int x[], int y[], int n) {
	double sx = x[0];
	double sy = y[0];
	double dx;
	double dy;
	double t;

	for (t = 0.0; t < 1.0; t += 0.01) {
		dx = evaluate_casteljau(x, n, t);
		dy = evaluate_casteljau(y, n, t);

		SDL_RenderDrawLine(ren, (int) round(sx), (int) round(sy), (int) round(dx), (int) round(dy));
		sx = dx;
		sy = dy;
	}
	SDL_RenderDrawLine(ren, (int) round(sx), (int) round(sy), x[n - 1], y[n - 1]);
}

/* funzione di redraw totale*/
void redraw_all(int n, int x[], int y[], SDL_Renderer *ren, TTF_Font *font, int vymax) {
	int i;
	char current_drawn_points[50] = {0};
	if (n > 0) {
		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);
		SDL_SetRenderDrawColor(ren, 255, 0, 50, 255);
		GC_FillCircle(ren, x[0], y[0], 3);
		for (i = 1; i <= n - 1; i++) {
			SDL_RenderDrawLine(ren, x[i - 1], y[i - 1], x[i], y[i]);
			GC_FillCircle(ren, x[i], y[i], 3);
		}
		SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
		if (n > 2)
			draw_casteljau(ren, x, y, n);
		SDL_SetRenderDrawColor(ren, 255, 0, 50, 255);
	}
	
	sprintf(current_drawn_points, "Drawn Points: %d", n);
	GC_DrawText(ren, font, 255, 0, 0, 255, 0, 0, 0, 255, current_drawn_points, 10, vymax - 40, shaded);
	GC_DrawText(ren, font, 255, 0, 0, 255, 0, 0, 0, 255, "MAX POINTS: " STR(DIM), 10, vymax - 20, shaded);
	SDL_SetRenderDrawColor(ren, 255, 0, 50, 255);
}

int get_nearest(int mouse_x, int mouse_y, int x[], int y[], int n) {
	int i;
	int nearest = -1;
	double distance;
	double dist_to_nearest;
	
	if (n == 0)
		return -1;
	
	dist_to_nearest = sqrt((mouse_x - x[0]) * (mouse_x - x[0]) + (mouse_y - y[0]) *(mouse_y - y[0]));
	if (dist_to_nearest < DISTANCE_THRESHOLD)
		nearest = 0;
	
	for (i=1; i<n; i++) {
	
		distance = sqrt((mouse_x - x[i]) * (mouse_x - x[i]) + (mouse_y - y[i]) *(mouse_y - y[i]));
		if (distance < DISTANCE_THRESHOLD && distance < dist_to_nearest) {
			dist_to_nearest = distance;
			nearest = i;
		}
	}	
	return nearest;
}

/* semplice programma di prova di input da mouse e tastiera */
int main(void) {
	SDL_Window *win;
	SDL_Renderer *ren;
	TTF_Font *font;
	SDL_Event event;
	int vxmax, vymax;
	int quit_program = 1, n = 0;
	int x[DIM], y[DIM];
	Uint32 windowID;
	int point_to_drag = -1;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "Couldn't init video: %s\n", SDL_GetError());
		return (1);
	}
	
	/* Initialize the TTF library */
	if (TTF_Init() < 0) {
		fprintf(stderr, "Couldn't initialize TTF: %s\n", SDL_GetError());
		SDL_Quit();
		return (2);
	}

	font = TTF_OpenFont("FreeSans.ttf", DEFAULT_PTSIZE);
	if (font == NULL) {
		fprintf(stderr, "Couldn't load font\n");
	}

	vxmax = WINDOWS_WIDTH;
	vymax = WINDOWS_HEIGHT;

	win = SDL_CreateWindow("Inter_Polygon", 100, 100, vxmax, vymax,
			SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
	if (win == NULL) {
		fprintf(stderr, "SDL_CreateWindow Error: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		fprintf(stderr, "SDL_CreateRenderer Error: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	/* Workaround to initialize the view with my nvidia driver */
	SDL_Delay(100);
	
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);
	SDL_RenderPresent(ren);
	redraw_all(n, x, y, ren, font, vymax);
	SDL_RenderPresent(ren);

	windowID = SDL_GetWindowID(win);
	printf("\nWindowID: %d\n", windowID);


	printf("\nDisegno di una curva di Bezier di grado n di punti di controllo:\n");
	printf("Dare i punti di controllo sulla finestra grafica con il mouse (button sinistro) \n");
	printf("Spostare i punti di controllo con il button destro \n");
	printf("Uscire dal programma con il tasto <ESC> o click sulla 'X' della finestra  \n");

	while (quit_program) {
		if (SDL_WaitEvent(&event)) {

			SDL_EventState(0xff, SDL_IGNORE); // Ignore all events

			switch (event.type) {
				case SDL_MOUSEBUTTONDOWN:
					if (event.button.button == SDL_BUTTON_LEFT) {
						if (n == DIM)
							continue;
						x[n] = event.button.x;
						y[n] = event.button.y;
						GC_FillCircle(ren, x[n], y[n], 3);
						n++;
						redraw_all(n, x, y, ren, font, vymax);
					}
					if (event.button.button == SDL_BUTTON_RIGHT) {
						point_to_drag = get_nearest(event.button.x, event.button.y, x, y, n);
					}
					SDL_RenderPresent(ren);
					break;
				case SDL_MOUSEBUTTONUP:
					if (event.button.button == SDL_BUTTON_RIGHT) {
						point_to_drag = -1;
					}
					break;
				case SDL_MOUSEMOTION:
					if (point_to_drag != -1) {
						x[point_to_drag] = event.button.x;
						y[point_to_drag] = event.button.y;
						redraw_all(n, x, y, ren, font, vymax);
					}
					SDL_RenderPresent(ren);
					break;
				case SDL_KEYDOWN:
					if (event.key.keysym.sym == SDLK_ESCAPE)
						quit_program = 0;
					break;
				case SDL_QUIT:
					quit_program = 0;
					break;
				case SDL_WINDOWEVENT:
					if (event.window.windowID == windowID) {
						switch (event.window.event) {
							case SDL_WINDOWEVENT_SIZE_CHANGED:
							{
								vxmax = event.window.data1;
								vymax = event.window.data2;

								redraw_all(n, x, y, ren, font, vymax);

								SDL_RenderPresent(ren);
								break;
							}
						}
					}
					break;

			}
		}
		SDL_EventState(0xff, SDL_ENABLE); // Enable all events
	}

	TTF_CloseFont(font);
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	TTF_Quit();
	SDL_Quit();
	return (0);
}
