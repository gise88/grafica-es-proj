# Copyright (C) 2016 Gianluca Iselli <gianluca.iselli@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


apps = esercizio_1 esercizio_2 esercizio_3 hungrywolf

# EXECUTABLES
MKDIR_P = mkdir -p

# BUILD/DIST PATH
BUILDPATH = build
DISTPATH = dist

all: builddir distdir
	for app in $(apps); do $(MAKE) -C $${app}; done;
	
builddir: 
	$(MKDIR_P) $(BUILDPATH)

distdir:
	$(MKDIR_P) $(DISTPATH)

clean:
	for app in $(apps); do $(MAKE) -C $${app} clean; done;
