/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Camera.h
 * Author: ise
 *
 * Created on February 10, 2017, 1:19 PM
 */

#ifndef HUNGRYWOLF_CAMERA_H
#define HUNGRYWOLF_CAMERA_H


#include <vector>


class Camera {
public:

    enum class CameraType {
        BACK_FIXED,
        RIGHT_FIXED,
        TOP_FIXED,
        HEAD,
        MOUSE
    };

    Camera(CameraType initCameraType);

    virtual ~Camera();

    void ResetCamera();

    Camera::CameraType GetCameraType();
    void NextCameraType();

    void ZoomIn();
    void ZoomOut();

    void MoveUp();
    void MoveDown();

    void RotateLeft();
    void RotateRight();

    void SetViewAngles(int xrel, int yrel);

    void Update(float px, float py, float pz, float facing);

private:

    std::vector<CameraType> m_cameraTypes = {
        CameraType::BACK_FIXED,
        CameraType::RIGHT_FIXED,
        CameraType::TOP_FIXED,
        CameraType::HEAD,
        CameraType::MOUSE
    };

    uint32_t m_indexCameraType;

    float m_eyeDist;
    float m_viewAlpha;
    float m_viewBeta;
};


#endif /* HUNGRYWOLF_CAMERA_H */
