/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   AnimationsManager.cpp
 * Author: ise
 *
 * Created on March 21, 2017, 11:46 PM
 */


#include <exception>

#include "AnimationsManager.h"


// global singleton instance of AnimationsManager class
static AnimationsManager gs_instance_animation_manager;

AnimationsManager::AnimationsManager() {
}

AnimationsManager::~AnimationsManager() {
    for(auto& n : m_meshesMap) {
        printf("Deleting %ld meshes of `%s` tag\n", n.second.size(), n.first.c_str());
        for (Mesh *mesh : n.second)
            delete mesh;
    }
}

void AnimationsManager::LoadAnimation(std::string tag, const char* pattern, int start, int end, float scale) {
    if (m_meshesMap.find(tag) != m_meshesMap.end())
        std::invalid_argument("The key \"" + tag + "\" already exists in the map of Animations");

    m_meshesMap[tag] = Animation::LoadAnimationMeshes(pattern, start, end, scale);
}

Animation* AnimationsManager::NewAnimationFromTag(std::string tag) {
    if (m_meshesMap.find(tag) != m_meshesMap.end())
        std::invalid_argument("The key \"" + tag + "\" does not exist in the map of Animations");

    return new Animation(m_meshesMap[tag]);
}




