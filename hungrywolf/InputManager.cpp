/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   InputManager.cpp
 * Author: ise
 *
 * Created on February 09, 2017, 6:48 PM
 */


#include <GL/gl.h>

#include "InputManager.h"



InputManager::InputManager(std::function<void()> quitHandler,
                           std::function<void(SDL_Event *)> windowEventHandler) {

    m_camera = nullptr;
    m_settings = Settings::InstancePtr();

    m_quitHandler = quitHandler;
    m_windowEventHandler = windowEventHandler;
    ResetUnhandledEventHandler();
}

InputManager::~InputManager() {
}

void InputManager::ResetUnhandledEventHandler() {
    m_unhandledEventHandler = [=](SDL_Event *) -> void {
//        printf("UnhandledEventHandler has not been set yet\n");
    };
}

void InputManager::SetCamera(Camera *camera) {
    m_camera = camera;
}

void InputManager::SetUI(UI *ui) {
    m_ui = ui;
}

void InputManager::SetUnhandledEventHandler(std::function<void(SDL_Event *)> unhandledEventHandler) {
    m_unhandledEventHandler = unhandledEventHandler;
}


void InputManager::ProcessEvent(SDL_Event *event) {

    switch (event->type) {
        case SDL_KEYDOWN:
            switch (event->key.keysym.sym) {
                case SDLK_q:
                    m_camera->RotateLeft();
                    break;
                case SDLK_e:
                    m_camera->RotateRight();
                    break;
                case SDLK_r:
                    m_camera->ZoomIn();
                    break;
                case SDLK_f:
                    m_camera->ZoomOut();
                    break;
                case SDLK_t:
                    m_camera->MoveUp();
                    break;
                case SDLK_g:
                    m_camera->MoveDown();
                    break;
                case SDLK_i:
                    m_settings->showInfoWindow = !m_settings->showInfoWindow;
                    break;
                case SDLK_F1:
                    m_camera->NextCameraType();
                    break;
                case SDLK_F2:
                    m_settings->useWireframe = !m_settings->useWireframe;
                    break;
                case SDLK_F3:
                    m_settings->useBoundingBox = !m_settings->useBoundingBox;
                    break;
                case SDLK_F4:
                    m_settings->useShadow = !m_settings->useShadow;
                    break;
                case SDLK_F5:
                    m_settings->billboardBack = !m_settings->billboardBack;
                    break;
                case SDLK_F6:
                    if (glIsEnabled(GL_FOG))
                        glDisable(GL_FOG);
                    else
                        glEnable(GL_FOG);
                    break;
                case SDLK_F7:
                    m_settings->useOnlyHeadCollider = !m_settings->useOnlyHeadCollider;
                    break;
                case SDLK_F8:
                    m_settings->debugInfo = !m_settings->debugInfo;
                    break;
                case SDLK_ESCAPE:
                    m_quitHandler();
                    break;
                default:
                    m_unhandledEventHandler(event);
            }
            break;

        case SDL_KEYUP:
            m_unhandledEventHandler(event);
            break;

        case SDL_MOUSEMOTION:
            if (m_camera->GetCameraType() == Camera::CameraType::MOUSE)
                m_camera->SetViewAngles(event->motion.xrel, event->motion.yrel);
            if (!m_ui->ProcessEvent(event))
                m_unhandledEventHandler(event);
            break;

        case SDL_MOUSEWHEEL:
            if (event->wheel.y < 0)
                m_camera->ZoomIn();
            else if (event->wheel.y > 0)
                m_camera->ZoomOut();
            else
                m_unhandledEventHandler(event);
            break;

        case SDL_QUIT:
            m_quitHandler();
            break;

        case SDL_WINDOWEVENT:
            m_windowEventHandler(event);
            break;

        default:
            if (!m_ui->ProcessEvent(event))
                m_unhandledEventHandler(event);
    }
}



