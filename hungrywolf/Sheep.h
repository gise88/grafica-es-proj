/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Sheep.h
 * Author: ise
 *
 * Created on March 02, 2017, 9:44 PM
 */

#ifndef HUNGRYWOLF_SHEEP_H
#define HUNGRYWOLF_SHEEP_H


#include <GL/gl.h>
#include <GL/glu.h>

#include <cstdlib>
#include <string>
#include <vector>

#include "Rigidbody.h"
#include "IRenderable.h"

#include "Colliders.h"

#include "mesh.h"
#include "Animation.h"

// OnRequestCollisionCheck


class Sheep : public Rigidbody, public IRenderable {
public:

    enum class Mode {
        IDLE,
        WALK
    };

    enum class Direction {
        AHEAD,
        LEFT,
        RIGHT,
    };

    static void LoadMeshAndTexture();
    static void UnloadMeshAndTexture();

    Sheep(Colliders *colliders);
    virtual ~Sheep();

    void SetMode(Mode mode);
    void SetDirection(Direction direction);

    void DoStep();
    void Render() override;

    Colliders* GetColliders();

private:

    static GLuint m_texId;
    static Mesh *m_idle;
    Animation *m_walk;

    Mesh *m_currentMesh;

    Mode m_currMode;

    Direction m_currDirection;

    Colliders *m_colliders;

    bool m_firstDoStepAfterRender;

    float m_vx;
    float m_vy;
    float m_vz;

    float frictionX;
    float frictionY;
    float frictionZWalk;
    float frictionZTurn;
    float frictionZIdle;

    float velSterzo, velRitornoSterzo, accMax,
        grip, sterzo; // attriti

    void SetupNextMesh();
};

#endif /* HUNGRYWOLF_SHEEP_H */
