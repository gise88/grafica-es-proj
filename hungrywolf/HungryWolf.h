/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   HungryWolf.h
 * Author: ise
 *
 * Created on March 18, 2017, 2:09 AM
 */

#ifndef HUNGRYWOLF_HUNGRYWOLF_H
#define HUNGRYWOLF_HUNGRYWOLF_H


#include <list>

#include "Game.h"

#include "Timer.h"
#include "Countdown.h"
#include "Environment.h"
#include "Minimap.h"
#include "WolfColliders.h"
#include "Wolf.h"
#include "SheepColliders.h"
#include "Sheep.h"
#include "Combo.h"
#include "CollisionsManager.h"
#include "HungryWolfInputManager.h"
#include "ScoresManager.h"


class HungryWolf : public Game {
public:
    HungryWolf(SDL_Window *win);

    virtual ~HungryWolf();

    virtual void DoStep() override;

    virtual void Render() override;

private:

    enum class GameState {
        INTRO,
        PLAY,
        PAUSE,
        PRE_LOSE,
        PRE_WIN,
        LOSE,
        WIN
    };

    GameState m_gameState;

    HungryWolfInputManager *m_hungryWolfInputManager;

    Wolf *m_wolf;

    WolfColliders *m_wolfColliders;

    std::list<Sheep *> m_allSheepList;

    std::list<Sheep *> m_spawnedSheepList;

    CollisionsManager *m_collisionsManager;

    Environment *m_environment;

    Minimap *m_minimap;

    Countdown *m_countdown;

    Countdown *m_exitGameCountdown;

    UIListView *m_listViewInfo;

    Timer m_comboTimer;

    GLuint m_introTexId;

    GLuint m_winTexId;

    GLuint m_loseTexId;

    Combo *m_combo;

    ScoresManager m_scoresManager;

    int m_score;

    int m_level;

    bool m_bonusLevelEnabled;

    int m_comboCount;

    int m_currentLevelSheepCount;

    int m_remainingTime;

    bool m_resetCameraOnFirstRender;

    virtual void QuitHandler() override;

    void SetGameState(GameState newGameState);

    void EnterGame();

    void ExitGame();

    void SetCoordToPixel();

    void CreateUI();

    void InitGame();

    void CountdownEventHandler(int remainingTime);

    void StartNewGame();

    void StartNextLevel();

    void SpawnNSheep(int count);

    void Render2D();

    void RenderMain();
};


#endif /* HUNGRYWOLF_HUNGRYWOLF_H */
