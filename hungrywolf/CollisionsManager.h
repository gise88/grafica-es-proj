/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   CollisionsManager.h
 * Author: ise
 *
 * Created on March 13, 2017, 11:14 AM
 */

#ifndef HUNGRYWOLF_COLLISIONSMANAGER_H
#define HUNGRYWOLF_COLLISIONSMANAGER_H


#include <list>

#include "Wolf.h"
#include "Sheep.h"
#include "Colliders.h"
#include "CollisionInfo.h"
#include "SphereCollider.h"
#include "BoxCollider.h"



class CollisionsManager {
public:
    CollisionsManager();

    virtual ~CollisionsManager();

    Sheep* WolfCollisions(Wolf *wolf, std::list<Sheep *> sheepList);

    static CollisionInfo CheckBetweenColliders(Colliders *c, BoxCollider *bc);

private:

    static CollisionInfo CheckBetweenColliders(Colliders *c1, Colliders *c2);

    static float CalculateDistance(SphereCollider *s1, SphereCollider *s2);

    static bool CheckCollision(SphereCollider *s1, SphereCollider *s2, CollisionInfo *i);

    static bool CheckCollision(SphereCollider *s, Point3 *bbmin, Point3 *bbmax, CollisionInfo *i);

};


#endif /* HUNGRYWOLF_COLLISIONSMANAGER_H */
