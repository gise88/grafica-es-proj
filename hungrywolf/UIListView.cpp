/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   UIListView.cpp
 * Author: ise
 *
 * Created on March 31, 2017, 10:28 AM
 */


#include <string>

#include "UIListView.h"

#include "GLUtils.h"


UIListView::UIListView() {
    SetFontSize(0.12);
}

UIListView::~UIListView() {
    m_items.clear();
}

#define Y_SPACE 5
#define Y_PADDING_BOTTOM 20


void UIListView::SetFontSize(float fontSize) {
    m_fontSize = fontSize;
    m_fontHeight = glutStrokeHeight(GLUT_STROKE_ROMAN) * fontSize;
    SetHeight((m_fontHeight + Y_SPACE) * m_items.size() + Y_PADDING_BOTTOM);
}

void UIListView::SetListItems(const std::list<int> list) {
    m_items.clear();
    for (int x : list)
        m_items.push_back(std::to_string(x));

    SetHeight((m_fontHeight + Y_SPACE) * m_items.size() + Y_PADDING_BOTTOM);
}

void UIListView::SetListItems(const std::list<std::string> list) {
    m_items.clear();
    for (std::string x : list)
        m_items.push_back(x);

    SetHeight((m_fontHeight + Y_SPACE) * m_items.size() + Y_PADDING_BOTTOM);
}

void UIListView::DrawItems(float xCenter, float y) {
    y += m_fontHeight + Y_SPACE;
    for (std::string x : m_items) {
        GLUtils::DrawTextShadow(
            GLUT_STROKE_ROMAN,
            x.c_str(),
            xCenter, y,
            2, 1, m_fontSize, true
        );
        y += m_fontHeight + Y_SPACE;
    }
}