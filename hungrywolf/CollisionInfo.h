/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   CollisionInfo.h
 * Author: ise
 *
 * Created on March 14, 2017, 8:22 PM
 */

#ifndef HUNGRYWOLF_COLLISIONINFO_H
#define HUNGRYWOLF_COLLISIONINFO_H


#include "SphereCollider.h"


class CollisionInfo {
public:

    enum Direction {
        NONE        = 0x00,
        Z_MAX       = 0x01,
        Z_MIN       = 0x02,
        X_MAX       = 0x04,
        X_MIN       = 0x08
    };

    CollisionInfo(Direction direction);

    virtual ~CollisionInfo();

    void SetDirection(Direction direction);

    Direction GetDirection();

    void SetColliderSphere(SphereCollider* sphereCollider);

    SphereCollider* GetColliderSphere();

private:
    Direction m_direction;

    SphereCollider *m_colliderSphere;
};



inline constexpr CollisionInfo::Direction
operator&(CollisionInfo::Direction lhs, CollisionInfo::Direction rhs)
{
    return static_cast<CollisionInfo::Direction>
    (static_cast<int>(lhs) & static_cast<int>(rhs));
}

inline constexpr CollisionInfo::Direction
operator|(CollisionInfo::Direction lhs, CollisionInfo::Direction rhs)
{
    return static_cast<CollisionInfo::Direction>
    (static_cast<int>(lhs) | static_cast<int>(rhs));
}

inline constexpr CollisionInfo::Direction
operator^(CollisionInfo::Direction lhs, CollisionInfo::Direction rhs)
{
    return static_cast<CollisionInfo::Direction>
    (static_cast<int>(lhs) ^ static_cast<int>(rhs));
}

inline constexpr CollisionInfo::Direction
operator~(CollisionInfo::Direction rhs)
{
    return static_cast<CollisionInfo::Direction>(~static_cast<int>(rhs));
}

inline CollisionInfo::Direction &
operator&=(CollisionInfo::Direction & lhs, CollisionInfo::Direction rhs)
{
    lhs = lhs & rhs;
    return lhs;
}

inline CollisionInfo::Direction &
operator|=(CollisionInfo::Direction & lhs, CollisionInfo::Direction rhs)
{
    lhs = lhs | rhs;
    return lhs;
}

inline CollisionInfo::Direction &
operator^=(CollisionInfo::Direction & lhs, CollisionInfo::Direction rhs)
{
    lhs = lhs ^ rhs;
    return lhs;
}


#endif /* HUNGRYWOLF_COLLISIONINFO_H */
