/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   ScoresManager.h
 * Author: ise
 *
 * Created on April 04, 2017, 2:25 PM
 */

#ifndef HUNGRYWOLF_SCORESMANAGER_H
#define HUNGRYWOLF_SCORESMANAGER_H


#include <list>


class ScoresManager {
public:

    ScoresManager(int maxCountScores);

    virtual ~ScoresManager();

    std::list<int> GetScores();

    void AddScore(int score);

private:

    int m_maxCountScores;

    std::list<int> m_scoreList;


    std::list<int> ReadScoresFromFile();

    void WriteScoresToFile(std::list<int> scores);
};


#endif /* HUNGRYWOLF_SCORESMANAGER_H */
