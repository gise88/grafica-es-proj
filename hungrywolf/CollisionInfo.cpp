/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   CollisionInfo.cpp
 * Author: ise
 *
 * Created on March 14, 2017, 8:22 PM
 */



#include <string.h>

#include "CollisionInfo.h"



CollisionInfo::CollisionInfo(Direction direction) {
    m_direction = direction;
    m_colliderSphere = nullptr;
}

CollisionInfo::~CollisionInfo() {
    if (m_colliderSphere)
        delete m_colliderSphere;
}

void CollisionInfo::SetDirection(Direction direction){
    m_direction = direction;
}

CollisionInfo::Direction CollisionInfo::GetDirection() {
    return m_direction;
}

void CollisionInfo::SetColliderSphere(SphereCollider *sc) {
    m_colliderSphere = new SphereCollider(sc);
}

SphereCollider* CollisionInfo::GetColliderSphere() {
    return m_colliderSphere;
}


