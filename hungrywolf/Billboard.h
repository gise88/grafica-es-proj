/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Billboard.h
 * Author: ise
 *
 * Created on March 19, 2017, 2:37 AM
 */

#ifndef HUNGRYWOLF_BILLBOARD_H
#define HUNGRYWOLF_BILLBOARD_H

#include <GL/gl.h>

#include <cstdlib>
#include <string>
#include <vector>

#include "IRenderable.h"

#include "mesh.h"



class Billboard : public IRenderable{
public:
    Billboard(float px, float pz);

    virtual ~Billboard();

    void Render() override;

private:

    enum class State {
        FRONT_SIDE,
        BACK_SIDE,
        GO_TO_FRONT,
        GO_TO_BACK
    };

    GLuint m_texId;
    Mesh *m_billboard;
    Mesh *m_base;

    State m_state;

    float m_px;
    float m_pz;
    float m_offsY;

    float m_rot;
};


#endif /* HUNGRYWOLF_BILLBOARD_H */
