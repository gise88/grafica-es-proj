#include <sstream>

#include <GL/gl.h>
#include <SDL2/SDL.h>
#include <GL/freeglut.h>

#include "Settings.h"

#include "HungryWolf.h"
#include "Utils.h"



Settings *settings;


// setta le matrici di trasformazione in modo
// che le coordinate in spazio oggetto siano le coord 
// del pixel sullo schemo


int main(int argc, char *argv[]) {
    SDL_Window *win;
    SDL_GLContext mainContext;

    Utils::initRandom();
    settings = Settings::InstancePtr();

    // inizializzazione di SDL
    SDL_Init(SDL_INIT_VIDEO); // TODO: Joystick?
    glutInit(&argc, argv);

    SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

    // Set the correct attributes for MASK and MAJOR version
    //	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_MASK);
    //	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);

    // facciamo una finestra di scrW x scrH pixels
    win = SDL_CreateWindow(settings->title,
                           400, 0, // x, y
                           settings->screenWidth,
                           settings->screenHeight,
                           SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

    //Create our opengl context and attach it to our window
    mainContext = SDL_GL_CreateContext(win);

    printf("%s\n", glGetString(GL_VERSION));
    printf("%s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));


    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_NORMALIZE); // opengl, per favore, rinormalizza le normali prima di usarle
    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_POLYGON_OFFSET_FILL); // caro openGL sposta i frammenti generati dalla rasterizzazione poligoni
    glPolygonOffset(1, 1); // indietro di 1
    glEnable(GL_ALPHA_TEST);
    glAlphaFunc(GL_GREATER, 0.002);

    glEnable(GL_FOG);
    glFogi(GL_FOG_MODE, GL_LINEAR);
    glFogf(GL_FOG_START, 180.0f);
    glFogf(GL_FOG_END, 200.0f);
    glFogi(GL_FOG_MODE, GL_EXP2);
    glFogf(GL_FOG_DENSITY, 0.002);
    float color[] = {0.83, 0.95, 0.98, 0.1};
    glFogfv(GL_FOG_COLOR, color);

    HungryWolf hungryWolf(win);
    hungryWolf.Start();

    SDL_GL_DeleteContext(mainContext);
    SDL_DestroyWindow(win);
    SDL_Quit();
    return 0;
}

