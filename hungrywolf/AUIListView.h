/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   AUIListView.h
 * Author: ise
 *
 * Created on March 31, 2017, 9:45 PM
 */

#ifndef HUNGRYWOLF_AUILISTVIEW_H
#define HUNGRYWOLF_AUILISTVIEW_H


#define UILISTVIEW_LABEL_MAX_LENGTH 50


#include <list>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>


class AUIListView {
public:
    AUIListView();

    virtual ~AUIListView();


    void SetLabel(const char *label);

    const char* GetLabel();

    void SetLabelFontSize(float fontSize);

    void SetPosition(float x, float y);

    void GetPosition(float *x, float *y);

    void SetWidth(float w);

    void SetTransparencyAlpha(float alpha);

    virtual void Draw();

protected:

    char m_label[UILISTVIEW_LABEL_MAX_LENGTH];
    float m_labelFontSize;
    float m_labelFontHeight;

    float m_alpha;

    float m_px;
    float m_py;

    float m_width;
    float m_width2;
    float m_height;
    float m_itemsHeight;

    void SetHeight(float h);

    virtual void DrawItems(float xCenter, float y) = 0;
};


#endif /* HUNGRYWOLF_AUILISTVIEW_H */
