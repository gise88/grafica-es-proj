/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   UI.h
 * Author: ise
 *
 * Created on March 23, 2017, 6:19 PM
 */

#ifndef HUNGRYWOLF_UI_H
#define HUNGRYWOLF_UI_H


#include <SDL2/SDL.h>

#include <vector>

#include "UIButton.h"
#include "UIListView.h"


class UI {
public:
    UI();

    virtual ~UI();

    UIButton* CreateButton();

    UIListView* CreateListView();

    bool ProcessEvent(SDL_Event *event);

    void ClearAll();

    void Draw();

private:

    UIButton *m_latestButtonPressed;
    std::vector<UIButton *> m_buttonVec;

    std::vector<UIListView *> m_listViewVec;
};


#endif /* HUNGRYWOLF_UI_H */
