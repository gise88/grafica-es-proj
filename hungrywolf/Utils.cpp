/*
 * Copyright (C) 2016 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   Utils.cpp
 * Author: ise
 * 
 * Created on October 26, 2016, 4:51 PM
 */



#include <time.h>
#include <stdlib.h>
#include <unistd.h>

#include "Utils.h"



Utils::Utils() {
}

Utils::~Utils() {
}

void Utils::initRandom() {
//    printf("srand: %d\n", (unsigned int)time(NULL) ^ getpid());
    srand((unsigned int)time(NULL) ^ getpid());
}

float Utils::randSign() {
    return rand() % 2 == 0 ? -1.0f : 1.0f;
}

uint32_t Utils::randUInt(uint32_t min, uint32_t max) {
    return (rand() % (1 + max - min)) + min;
}

float Utils::randFloat(float min, float max) {
    float random = ((float)rand()) / (float)RAND_MAX;
    float diff = max - min;
    float r = random * diff;
    return min + r;
}