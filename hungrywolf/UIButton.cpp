/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   UIButton.cpp
 * Author: ise
 *
 * Created on March 23, 2017, 6:12 PM
 */


#include <string.h>

#include "UIButton.h"
#include "GLUtils.h"



UIButton::UIButton() {
    strncpy(m_label, "btn label", UIBUTTON_LABEL_MAX_LENGTH);
    m_fontSize = 0.15;
    m_alpha = 1;
    m_px = 0;
    m_py = 0;
    m_width = 0;
    m_height = 0;
    m_highlighted = false;
    m_pressed = false;


    m_buttonCallback = [=](UIButton *) -> void {
        printf("ButtonCallback has not been set yet\n");
    };
}

UIButton::~UIButton() {

}

void UIButton::SetLabel(const char *label) {
    strncpy(m_label, label, UIBUTTON_LABEL_MAX_LENGTH);
}

const char* UIButton::GetLabel() {
    return m_label;
}

void UIButton::SetFontSize(float fontSize) {
    m_fontSize = fontSize;
}

void UIButton::SetBounds(float x, float y, float w, float h) {
    m_px = x;
    m_py = y;
    m_width = w;
    m_height = h;
}

void UIButton::GetBounds(float *x, float *y, float *w, float *h) {
    *x = m_px;
    *y = m_py;
    *w = m_width;
    *h = m_height;
}

void UIButton::SetTransparencyAlpha(float alpha) {
    m_alpha = alpha;
}

void UIButton::SetHighlighted(bool highlighted) {
    m_highlighted = highlighted;
}

void UIButton::SetPressed(bool pressed) {
    m_pressed = pressed;
}

void UIButton::SetOnClickListener(std::function<void(UIButton *)> buttonCallback) {
    m_buttonCallback = buttonCallback;
}

void UIButton::EmitClickEvent() {
    m_pressed = false;
    m_buttonCallback(this);
}

void UIButton::Draw() {
    float fontX;
    float fontY;

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);

    if (m_highlighted)
        glColor4f(0.7f, 0.7f, 0.8f, m_alpha);
    else
        glColor4f(0.6f, 0.6f, 0.6f, m_alpha);

    glBegin(GL_QUADS);
    glVertex2i(m_px, m_py);
    glVertex2i(m_px, m_py + m_height);
    glVertex2i(m_px + m_width, m_py + m_height);
    glVertex2i(m_px + m_width, m_py);
    glEnd();

    glLineWidth(3);

    if (m_pressed)
        glColor4f(0.4f, 0.4f, 0.4f, m_alpha);
    else
        glColor4f(0.8f, 0.8f, 0.8f, m_alpha);

    glBegin(GL_LINE_STRIP);
    glVertex2i(m_px + m_width, m_py);
    glVertex2i(m_px, m_py);
    glVertex2i(m_px, m_py + m_height);
    glEnd();

    if (m_pressed)
        glColor4f(0.8f, 0.8f, 0.8f, m_alpha);
    else
        glColor4f(0.4f, 0.4f, 0.4f, m_alpha);

    glBegin(GL_LINE_STRIP);
    glVertex2i(m_px, m_py + m_height);
    glVertex2i(m_px + m_width, m_py + m_height);
    glVertex2i(m_px + m_width, m_py);
    glEnd();

    glLineWidth(1);

    fontX = m_px + ((m_px + m_width) - m_px) / 2;
    fontY = m_py + ((m_py + m_height) - m_py) / 2;

    if (m_pressed) {
        fontX += 2;
        fontY += 2;
    }

    if (m_highlighted) {
        GLUtils::DrawTextShadow(
            GLUT_STROKE_ROMAN,
            m_label,
            fontX, fontY,
            3, 1, m_fontSize, true
        );
    } else {
        GLUtils::DrawTextShadow(
            GLUT_STROKE_ROMAN,
            m_label,
            fontX, fontY,
            3, 2, m_fontSize, true
        );
    }

    glDisable(GL_BLEND);
    glDisable(GL_LINE_SMOOTH);
}