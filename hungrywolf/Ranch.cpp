/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Ranch.cpp
 * Author: ise
 *
 * Created on March 08, 2017, 9:34 AM
 */


#include "Ranch.h"
#include "TextureLoader.h"
#include "Settings.h"



Ranch::Ranch(int width, int height) {
    float scale = 0.3;

    m_texId = TextureLoader::loadAsMipmap("res/obj/Fence/Fence tex.jpg");

    m_segmentFence = new Mesh("res/obj/Fence/Segment_fence.obj", scale);
    m_endFenceLog = new Mesh("res/obj/Fence/End_fence_log.obj", scale);

    float segmentLength = (m_segmentFence->bbmax.X() - m_segmentFence->bbmin.X()) -
        ((m_segmentFence->bbmax.Z() - m_segmentFence->bbmin.Z()) / 10);
    float xOffs = (segmentLength * width) / 2;
    float zOffs = 2;

    for (int h=0; h<height; h++) {
        m_fenceVec.push_back(new Fence(m_texId, m_segmentFence, xOffs, 0, -segmentLength * h + zOffs, 90.0f));
        m_fenceVec.push_back(new Fence(m_texId, m_segmentFence, -xOffs, 0, -segmentLength * h - segmentLength + zOffs, 270.0f));
    }
    for (int w=0; w<width; w++)
        m_fenceVec.push_back(new Fence(m_texId, m_segmentFence, xOffs - segmentLength * w, 0, -segmentLength * height + zOffs, 0.0f));

    int entranceWidthMin = (width - 2) / 2;
    int entranceWidthMax = entranceWidthMin + 3;

    for (int w=0; w<entranceWidthMin; w++)
        m_fenceVec.push_back(new Fence(m_texId, m_segmentFence, -xOffs + segmentLength * w, 0, zOffs, 180.0f));

    for (int w=entranceWidthMax; w<width; w++)
        m_fenceVec.push_back(new Fence(m_texId, m_segmentFence, -xOffs + segmentLength * w, 0, zOffs, 180.0f));

    m_fenceVec.push_back(new Fence(m_texId, m_segmentFence, -xOffs + segmentLength * entranceWidthMin, 0, zOffs, 270.0f));
    m_fenceVec.push_back(new Fence(m_texId, m_endFenceLog, -xOffs + segmentLength * entranceWidthMin, 0, zOffs, 270.0f));
    m_fenceVec.push_back(new Fence(m_texId, m_segmentFence, -xOffs + segmentLength * entranceWidthMax, 0, segmentLength + zOffs, 90.0f));

    m_xMin = -xOffs;
    m_xMax = xOffs;
    m_zMin = zOffs;
    m_zMax = -segmentLength * height + zOffs;
}

Ranch::~Ranch() {
    for (Fence *fence : m_fenceVec)
        delete fence;
    glDeleteTextures(1, &m_texId);
    delete m_segmentFence;
    delete m_endFenceLog;
}

void Ranch::Render() {
    for (Fence *fence : m_fenceVec)
        fence->Render();

    if (Settings::Instance().useBoundingBox) {
        glPushMatrix();

        glDisable(GL_TEXTURE_2D);
        glLineWidth(2.0);

        glColor3f(0, 1, 0);

        glBegin(GL_LINE_LOOP);

#define Y 0.5f

        glVertex3f(m_xMin, Y, m_zMin);
        glVertex3f(m_xMax, Y, m_zMin);
        glVertex3f(m_xMax, Y, m_zMax);
        glVertex3f(m_xMin, Y, m_zMax);
        glVertex3f(m_xMin, Y, m_zMin);

        glEnd();

        glPopMatrix();
    }
}


void Ranch::GetBoundingBox(Point3 *bbmin, Point3 *bbmax) {
    bbmin->set(m_xMin, 0, m_zMin);
    bbmax->set(m_xMax, 0, m_zMax);
}

Ranch::Fence::Fence(GLuint texId, Mesh *fence, float px, float py, float pz, float rotY) {
    m_texId = texId;
    m_fence = fence;
    m_px = px;
    m_py = py;
    m_pz = pz;
    m_rotY = rotY;
}

Ranch::Fence::~Fence() {
}

void Ranch::Fence::Render() {
    glPushMatrix();

    glTranslatef(m_px, m_py, m_pz);
    glScalef(1, 1, -1);
    glRotatef(m_rotY, 0, 1, 0);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_texId);
    glColor3f(1, 1, 1);
    glDisable(GL_LIGHTING);

    m_fence->RenderNxVTex();

    glPopMatrix();
}