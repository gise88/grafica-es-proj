/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Wolf.h
 * Author: ise
 *
 * Created on February 06, 2017, 3:24 PM
 */



#include "Wolf.h"
#include "TextureLoader.h"
#include "Settings.h"
#include "CollisionInfo.h"
#include "AnimationsManager.h"

#define WOLF_RUN_ANIMATION "wolf_run_animation"
#define WOLF_SDSU_ANIMATION "wolf_sit_down_stand_up_animation"


Wolf::Wolf(Colliders *colliders) {
    float scale = Settings::Instance().wolfScale;

    m_colliders = colliders;
    m_colliders->SetScale(scale);

    m_idle = new Mesh("res/obj/wolf-yes/Wolf_idle.obj", scale);

    AnimationsManager::Instance().LoadAnimation(WOLF_RUN_ANIMATION, "res/obj/wolf-yes/anim/Wolf_run_", 1, 17, scale);
    AnimationsManager::Instance().LoadAnimation(WOLF_SDSU_ANIMATION, "res/obj/wolf-yes/anim/Wolf_sitdown-standup_", 1, 10, scale);

    m_run = AnimationsManager::Instance().NewAnimationFromTag(WOLF_RUN_ANIMATION);
    m_sitDownStandUp = AnimationsManager::Instance().NewAnimationFromTag(WOLF_SDSU_ANIMATION);
    m_sitDownStandUp->SetCycleAnimation(false);

    m_texId = TextureLoader::loadAsMipmap("res/obj/wolf-yes/textures/wolf col.jpg");

    Reset();
}

Wolf::~Wolf() {
    delete m_idle;
    delete m_run;
    delete m_sitDownStandUp;
    glDeleteTextures(1, &m_texId);
}

void Wolf::Reset() {
    m_currMode = Mode::IDLE;
    m_nextMode = Mode::IDLE;
    m_currDirection = Direction::AHEAD;

    m_vx = m_vy = m_vz = 0; // velocita' attuale

    velSterzo = 4.0; // A
    velRitornoSterzo = 0.93; // B, sterzo massimo = A*B / (1-B)
    sterzo = 0;

    accMax = 0.0015;

    // attriti: percentuale di velocita' che viene mantenuta
    // 1 = no attrito
    // <<1 = attrito grande
    frictionZRun = 0.991; // piccolo attrito sulla Z (nel senso di rotolamento delle ruote)
    frictionZTurn = 0.97; // piccolo attrito sulla Z (nel senso di rotolamento delle ruote)
    frictionZIdle = 0.92; // piccolo attrito sulla Z (nel senso di rotolamento delle ruote)
    frictionX = 0.8; // grande attrito sulla X (per non fare slittare la macchina)
    frictionY = 1.0; // attrito sulla y nullo

    // Nota: vel max = accMax*attritoZ / (1-attritoZ)

    grip = 0.45; // quanto il facing macchina si adegua velocemente allo sterzo

    SetPositionAndFacing(0, 0, 0, 0);

    m_firstDoStepAfterRender = true;
}

void Wolf::SetMode(Mode mode) {
    m_nextMode = mode;

    switch (m_currMode) {
        case Mode::IDLE:
            m_currMode = mode;
            break;

        case Mode::RUN:
            m_currMode = mode;
            break;

        case Mode::__SITTING_DOWN:
        case Mode::SIT_DOWN:
            if (mode != Mode::SIT_DOWN) {
                m_currMode = Mode::__STANDING_UP;
                m_sitDownStandUp->SetRegularDirection(false);
            }
            break;

        case Mode::__STANDING_UP:
            break;
    }
}

void Wolf::SetDirection(Direction direction) {
    m_currDirection = direction;
}

Colliders* Wolf::GetColliders() {
    return m_colliders;
}

void Wolf::SetupNextMesh() {

    switch (m_currMode) {
        case Mode::IDLE:
            m_currentMesh = m_idle;
            break;

        case Mode::RUN:
            m_run->GetNextMesh(&m_currentMesh);
            break;

        case Mode::__SITTING_DOWN:
            if (!m_sitDownStandUp->GetNextMesh(&m_currentMesh))
                m_currMode = Mode::SIT_DOWN;
            break;

        case Mode::SIT_DOWN:
            m_sitDownStandUp->GetNextMesh(&m_currentMesh);
            break;

        case Mode::__STANDING_UP:
            if (!m_sitDownStandUp->GetNextMesh(&m_currentMesh)) {
                m_currMode = m_nextMode;
                m_sitDownStandUp->SetRegularDirection(true);
            }
            break;
    }
}

void Wolf::DoStep() {

    if (m_firstDoStepAfterRender) {
        SetupNextMesh();
        m_firstDoStepAfterRender = false;
    }

    // computiamo l'evolversi della macchina
    float vxm, vym, vzm; // velocita' in spazio macchina

    // da vel frame mondo a vel frame macchina
    float cosf = cos(m_facing * M_PI / 180.0);
    float sinf = sin(m_facing * M_PI / 180.0);
    vxm = +cosf * m_vx - sinf * m_vz;
    vym = m_vy;
    vzm = +sinf * m_vx + cosf * m_vz;

    // gestione dello sterzo
    if (m_currDirection == Direction::LEFT)
        sterzo += velSterzo;
    if (m_currDirection == Direction::RIGHT)
        sterzo -= velSterzo;
    sterzo *= velRitornoSterzo; // ritorno a volante dritto

    if (m_currMode == Mode::RUN)
        vzm -= accMax; // accelerazione in avanti

    // attirti (semplificando)
    vxm *= frictionX;
    vym *= frictionY;
    if (m_currMode == Mode::RUN) {
        if (m_currDirection == Direction::AHEAD)
            vzm *= frictionZRun;
        else
            vzm *= frictionZTurn;
    }
    if (m_currMode != Mode::RUN)
        vzm *= frictionZIdle;

    // l'orientamento della macchina segue quello dello sterzo
    // (a seconda della velocita' sulla z)
    m_facing = fmod(m_facing - (vzm * grip) * sterzo, 360.0f);
    if (m_facing < 0)
        m_facing += 360.0f;

    // ritorno a vel coord mondo
    m_vx = +cosf * vxm + sinf * vzm;
    m_vy = vym;
    m_vz = -sinf * vxm + cosf * vzm;


    m_colliders->Update(m_px + m_vx, m_py + m_vy, m_pz + m_vz, m_facing, &(m_currentMesh->bbmin), &(m_currentMesh->bbmax));

    CollisionInfo collisionInfo = m_colliders->RequestCollisionCheck();
    CollisionInfo::Direction direction = collisionInfo.GetDirection();
    if (direction == CollisionInfo::Direction::NONE) {
        // posizione = posizione + velocita * delta t (ma delta t e' costante)
        m_px += m_vx;
        m_py += m_vy;
        m_pz += m_vz;
    } else {
        if (!(direction & CollisionInfo::Direction::Z_MAX) && !(direction & CollisionInfo::Direction::Z_MIN)) {
            m_pz += m_vz;
            m_px -= m_vx;
        } else if (!(direction & CollisionInfo::Direction::X_MAX) && !(direction & CollisionInfo::Direction::X_MIN)) {
            m_px += m_vx;
            m_pz -= m_vz;
        }

//        if (direction == CollisionInfo::Direction::Z_MAX || direction & CollisionInfo::Direction::Z_MIN) {
//            if (m_facing > 180)
//                m_facing += (360 - m_facing) * 2;
//            else if (m_facing < 180)
//                m_facing -= m_facing * 2;
//            m_facing += 180;
//        } else if (direction == CollisionInfo::Direction::X_MAX || direction & CollisionInfo::Direction::X_MIN) {
//            if (m_facing < 90 || m_facing > 270)
//                m_facing += (360 - m_facing) * 2;
//            else if (m_facing > 90 && m_facing < 270)
//                m_facing -= m_facing * 2;
//        }

        m_py += m_vy;
        m_colliders->Update(m_px, m_py, m_pz, m_facing, &(m_currentMesh->bbmin), &(m_currentMesh->bbmax));
    }
}

void Wolf::Render() {
    // sono nello spazio mondo

    glPushMatrix();

    glTranslatef(m_px, m_py, m_pz);
    glScalef(1, 1, -1);
    glRotatef(-m_facing, 0, 1, 0);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_texId);
    glColor3f(1, 1, 1); // metto il colore neutro (viene moltiplicato col colore texture, componente per componente)
    glDisable(GL_LIGHTING); // disabilito il lighting OpenGL standard (lo faccio con la texture)

    m_currentMesh->RenderNxVTex();

    if (Settings::Instance().useShadow) {
        glDisable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);

        glPushMatrix();
        glColor4f(0.5, 0.5, 0.5, 0.8);
        glTranslatef(0, 0.006f, 0);
        glScalef(1.01, 0, 1.01);
        m_currentMesh->RenderNxV();
        glPopMatrix();

        glPushMatrix();
        glColor4f(0.4, 0.4, 0.4, 0.7);
        glColor4f(0.5, 0.5, 0.5, 0.8);
        glTranslatef(0, 0.009f, 0);
        glScalef(0.8, 0, 1.01);
        m_currentMesh->RenderNxV();
        glPopMatrix();
        glDisable(GL_BLEND);
    }

    glPopMatrix();

    if (Settings::Instance().useBoundingBox)
        m_colliders->Render();

    m_firstDoStepAfterRender = true;
}