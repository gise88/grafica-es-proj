/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Minimap.cpp
 * Author: ise
 *
 * Created on March 18, 2017, 12:34 AM
 */



#include <math.h>
#include "Minimap.h"

#include "TextureLoader.h"
#include "GLUtils.h"



Minimap::Minimap(Point3 bbmin, Point3 bbmax) {

    m_bbmin.set(bbmin.X(), bbmin.Y(), bbmin.Z());
    m_bbmin.set(bbmax.X(), bbmax.Y(), bbmax.Z());

    m_settings = Settings::InstancePtr();

    m_wolfTexId = TextureLoader::load("res/img/wolf-footprint128.png");
    m_sheepTexId = TextureLoader::load("res/img/sheep-icon.png");
    m_compassTexId = TextureLoader::load("res/img/compass2.png");
}

Minimap::~Minimap() {
    glDeleteTextures(1, &m_wolfTexId);
    glDeleteTextures(1, &m_sheepTexId);
    glDeleteTextures(1, &m_compassTexId);
}

void Minimap::Render(Rigidbody *wolf, std::list<Rigidbody *> sheepList) {

    float centerX = m_settings->screenWidth - 120;
    float centerY = m_settings->screenHeight - 120;
    float footprintSize = 15;
    float wolfX = wolf->GetX();
    float wolfY = wolf->GetZ();
    float radiusIn = 100;
    float radiusOut = 110;
    float radiusImg = 120;
    float wolfFacing = wolf->GetFacing();

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);
    glColor4f(0.6, 0.6, 0.6, 0.2);
    GLUtils::DrawFilledCircle2D(centerX, centerY, radiusIn);
    glDisable(GL_BLEND);

    glPushMatrix();
    glTranslatef(centerX, centerY, 0);

    glPushMatrix();
    glBindTexture(GL_TEXTURE_2D, m_compassTexId);
    glEnable(GL_TEXTURE_2D);
//    glLineWidth(3.0);
    glBegin(GL_QUADS);
    glColor3f(1, 1, 1);
    glTexCoord2i(1, 0);
    glVertex2d(-radiusImg, radiusImg);
    glTexCoord2i(1, 1);
    glVertex2d(-radiusImg, -radiusImg);
    glTexCoord2i(0, 1);
    glVertex2d(radiusImg, -radiusImg);
    glTexCoord2i(0, 0);
    glVertex2d(radiusImg, radiusImg);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    for (Rigidbody *sheep : sheepList) {
        float sheepX = sheep->GetX();
        float sheepY = sheep->GetZ();
        float deltaX = sheepX - wolfX;
        float deltaY = sheepY - wolfY;
        float dist = sqrt(deltaX * deltaX + deltaY * deltaY);
        float theta = atan2(deltaY, deltaX);
        float offsX = 0;
        float offsY = 0;
        float distMax = 30;

        if (dist > distMax)
            dist = radiusOut;
        else
            dist = dist / distMax * radiusIn;

        offsX = dist * sin(theta);
        offsY = dist * cos(theta);

        glPushMatrix();
        glRotatef(-90, 0, 0, 1);
        glTranslatef(offsX, offsY, 0);
        glRotatef(90, 0, 0, 1);
        glBindTexture(GL_TEXTURE_2D, m_sheepTexId);
        glEnable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
        glColor3f(1, 1, 1);

        glTexCoord2i(1, 0);
        glVertex2d(-footprintSize, footprintSize);
        glTexCoord2i(1, 1);
        glVertex2d(-footprintSize, -footprintSize);
        glTexCoord2i(0, 1);
        glVertex2d(footprintSize, -footprintSize);
        glTexCoord2i(0, 0);
        glVertex2d(footprintSize, footprintSize);

        glEnd();
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();
    }


    glPushMatrix();
    glRotatef(wolfFacing, 0, 0, 1);
    glBindTexture(GL_TEXTURE_2D, m_wolfTexId);
    glEnable(GL_TEXTURE_2D);
//    glLineWidth(3.0);
    glBegin(GL_QUADS);
    glColor3f(1, 1, 1);
    glTexCoord2i(1, 0);
    glVertex2d(-footprintSize, footprintSize);
    glTexCoord2i(1, 1);
    glVertex2d(-footprintSize, -footprintSize);
    glTexCoord2i(0, 1);
    glVertex2d(footprintSize, -footprintSize);
    glTexCoord2i(0, 0);
    glVertex2d(footprintSize, footprintSize);
    glEnd();
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    glPopMatrix();
}