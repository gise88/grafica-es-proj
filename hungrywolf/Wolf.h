/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Wolf.h
 * Author: ise
 *
 * Created on February 06, 2017, 3:24 PM
 */

#ifndef HUNGRYWOLF_WOLF_H
#define HUNGRYWOLF_WOLF_H


#include <GL/gl.h>
#include <GL/glu.h>

#include <cstdlib>
#include <string>
#include <vector>

#include "Rigidbody.h"
#include "IRenderable.h"

#include "Colliders.h"

#include "mesh.h"
#include "Animation.h"

#include "Ranch.h"
#include "Sheep.h"



class Wolf : public Rigidbody, public IRenderable {
public:

    enum class Mode {
        IDLE /* il lupo è fermo */,
        RUN  /* il lupo corre */,
        SIT_DOWN /* il lupo è seduto */,
        // internal state
        __SITTING_DOWN /* il lupo si sta sedendo */,
        __STANDING_UP  /* il lupo si sta alzando */
    };

    enum class Direction {
        AHEAD /* il lupo punta avanti */,
        LEFT  /* il lupo ruota a sinistra */,
        RIGHT /* il lupo ruota a destra */,
    };

    Wolf(Colliders *colliders);

    virtual ~Wolf();

    void Reset();

    void SetMode(Mode mode);
    void SetDirection(Direction direction);

    void DoStep();
    void Render() override;

    Colliders* GetColliders();

private:

    GLuint m_texId;

    Mesh *m_idle;
    Animation *m_run;
    Animation *m_sitDownStandUp;

    Mesh *m_currentMesh;

    Mode m_currMode;
    Mode m_nextMode;

    Direction m_currDirection;

    Colliders *m_colliders;

    bool m_firstDoStepAfterRender;

    float m_vx;
    float m_vy;
    float m_vz;

    float frictionX;
    float frictionY;
    float frictionZRun;
    float frictionZTurn;
    float frictionZIdle;

    float velSterzo, velRitornoSterzo, accMax,
        grip, sterzo; // attriti

    void SetupNextMesh();
};


#endif /* HUNGRYWOLF_WOLF_H */
