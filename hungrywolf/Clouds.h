/*
 * Copyright (C) 2016 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   Clouds.h
 * Author: ise
 *
 * Created on October 26, 2016, 3:09 AM
 */

#ifndef HUNGRYWOLF_CLOUDS_H
#define HUNGRYWOLF_CLOUDS_H


#include <GL/gl.h>

#include <cstdlib>
#include <string>
#include <vector>

#include "mesh.h"

#include "CloudsObjFinder.h"



class Clouds {
public:
    Clouds(float maxDist);

    virtual ~Clouds();

    void createGroups();

    void draw(float px, float py, float pz);

private:

    class Cloud {
    public:
        Cloud(GLuint texId, const char *objFilename, float maxDist, float phi, float theta, float rotSpeed);

        virtual ~Cloud();

        void move(float px, float py, float pz);

    private:
        Mesh m_cloudMesh;
        float m_dist;
        GLuint m_texId;
        float m_maxDistance;
        float m_phiAngle;
        float m_thetaAngle;
        float m_rotationSpeed;

        float m_alphaColor;
    };

    float m_maxDistance;
    size_t m_nPngClouds;
    GLuint *m_cloudsTexId;
    CloudsObjFinder m_cloudsObjFinder;
    std::vector<std::vector<Cloud *> > m_cloudsGroupsVec;
};

#endif /* HUNGRYWOLF_CLOUDS_H */

