/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Settings.cpp
 * Author: ise
 *
 * Created on February 10, 2017, 4:17 PM
 */



#include <stdio.h>

#include "Settings.h"
#include "Utils.h"


// global singleton instance of Settings class
static Settings gs_instance_settings;


Settings::Settings() {
    printf("Settings::Settings()\n"); fflush(stdout);

    title = "Hungry Wolf";
    screenWidth = 1600;
    screenHeight = 960;
    worldSize = 800;

    showInfoWindow = true;

    cloudsGroupsCount = Utils::randUInt(5, 10);
    cloudsInGroup = Utils::randUInt(1, 4);

    wolfScale = 2;
    sheepScale = 0.4;

    useWireframe = false;
    useShadow = false;
    useBoundingBox = false;
    useEnvmap = false;

    billboardBack = false;

    useOnlyHeadCollider = false;

    debugInfo = false;

    ranchWidth = 35;
    ranchHeight = 35;
}

Settings::~Settings() {
}