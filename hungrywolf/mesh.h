#ifndef HUNGRYWOLF_MESH_H
#define HUNGRYWOLF_MESH_H


// classe Vertex: 
// i vertici della mesh

#include <GL/gl.h>

#include <vector>

#include "Settings.h"
#include "point3.h"



class Vertex {
public:
    Point3 p; // posizione

    // attributi per verice
    Vector3 n; // normale (per vertice)
};

class Point2 {
public:
    Point2(float x, float y) {
        coord[0] = x;
        coord[1] = y;
    }

    // mandare il punto come coordinata texture
    void SendAsTexCoords() const {
        glTexCoord2fv(coord);
    }

private:
    GLfloat coord[2];
};

class Edge {
public:
    Vertex *v[2]; // due puntatori a Vertice (i due estremi dell'edge)
    // attributi per edge:
};

class Face {
public:
    Vertex *v[3]; // tre puntatori a Vertice (i tre vertici del triangolo)

    // costruttore
    Face(Vertex *a, Vertex *b, Vertex *c) {
        v[0] = a;
        v[1] = b;
        v[2] = c;
    }

    // attributi per faccia
    Vector3 n; // normale (per faccia)

    // computa la normale della faccia
    void ComputeNormal() {
        n = -((v[1]->p - v[0]->p) % (v[2]->p - v[0]->p)).Normalize();
    }

    // attributi per wedge
};

class TexFace {
public:
    Point2 *p[3]; // tre puntatori a Vertice (i tre vertici del triangolo)

    // costruttore
    TexFace(Point2 *a, Point2 *b, Point2 *c) {
        p[0] = a;
        p[1] = b;
        p[2] = c;
    }
};

class Mesh {
    std::vector<Vertex> v; // vettore di vertici
    std::vector<Face> f;   // vettore di facce
    std::vector<Edge> e;   // vettore di edge (per ora, non usato)
    std::vector<Point2> vt;
    std::vector<TexFace> ft;  // vettore di facce per texture

    Settings *settings;

public:

    // costruttore con caricamento
    Mesh(const char *filename) {
        LoadFromObj(filename, 1.0);
        ComputeNormalsPerFace();
        ComputeNormalsPerVertex();
        ComputeBoundingBox();
//        printf("Mesh::Mesh(%s)\n", filename); fflush(stdout);
        settings = Settings::InstancePtr();
    }

    // costruttore con caricamento e scala
    Mesh(const char *filename, float scale) {
        LoadFromObj(filename, scale);
        ComputeNormalsPerFace();
        ComputeNormalsPerVertex();
        ComputeBoundingBox();
//        printf("Mesh::Mesh(%s, %f)\n", filename, scale); fflush(stdout);
        settings = Settings::InstancePtr();
    }

    virtual ~Mesh() {

    }

    // metodi
    void RenderNxF(); // manda a schermo la mesh Normali x Faccia
    void RenderNxV(); // manda a schermo la mesh Normali x Vertice
    void RenderNxVTex();
    void RenderWire(); // manda a schermo la mesh in wireframe

    bool LoadFromObj(const char *filename, float scale); //  carica la mesh da un file OFF

    void ComputeNormalsPerFace();

    void ComputeNormalsPerVertex();

    void ComputeBoundingBox();

    // centro del axis aligned bounding box
    Point3 Center() {
        return (bbmin + bbmax) / 2.0;
    };

    Point3 bbmin, bbmax; // bounding box
};


#endif /* HUNGRYWOLF_MESH_H */