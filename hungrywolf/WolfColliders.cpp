/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   WolfColliders.cpp
 * Author: ise
 *
 * Created on March 15, 2017, 2:44 AM
 */


#include <math.h>

#include "WolfColliders.h"



WolfColliders::WolfColliders(BoxCollider *boxCollider)
    : Colliders(boxCollider) {

    m_sphereDataVec.push_back(std::make_tuple(0.4, "REAR_LEGS"));
    m_sphereDataVec.push_back(std::make_tuple(0.75, "CHEST"));
    m_sphereDataVec.push_back(std::make_tuple(0.95, "HEAD"));
}

WolfColliders::~WolfColliders() {
}

void WolfColliders::Update(float px, float py, float pz, float facing, Point3 *bbmin, Point3 *bbmax) {
    float cosf = cos((facing - 90.0f) * M_PI / 180.0);
    float sinf = sin((facing - 90.0f) * M_PI / 180.0);

    float ymax = bbmax->Y();
    float zmin = bbmin->Z();
    float zmax = bbmax->Z();

    m_sphereVec.clear();

    for (std::tuple<float, std::string> &t : m_sphereDataVec) {
        float perc = std::get<0>(t);
        std::string tag = std::get<1>(t);

        float offset = zmin + (zmax - zmin) * perc;
        float circleCX = - cosf * offset + px;
        float circleCY = ymax * 0.65 + py;
        float circleCZ = sinf * offset + pz;

        m_sphereVec.push_back(SphereCollider(circleCX, circleCY, circleCZ, 0.105f * m_scale, tag.c_str()));
    }

    float offset = zmin + (zmax - zmin) * 0.5;
    float circleCX = - cosf * offset + px;
    float circleCY = ymax * 0.65 + py;
    float circleCZ = sinf * offset + pz;

    m_containerSphere.Set(circleCX, circleCY, circleCZ, 0.55f * m_scale);
}