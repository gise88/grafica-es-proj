/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Countdown.cpp
 * Author: ise
 *
 * Created on March 22, 2017, 4:36 PM
 */


#include <iomanip>
#include <sstream>
#include <ctime>

#include "Countdown.h"


Countdown::Countdown() {
    m_start = std::chrono::system_clock::now();
}

Countdown::~Countdown() {
}

void Countdown::AddCountdownEvent(int remainingSec, std::function<void(int)> eventHandler) {
    m_eventsVec.push_back(CountdownEvent(remainingSec, eventHandler));
}

void Countdown::Set(int offsetInSeconds) {
    m_start = std::chrono::system_clock::now();
    m_start += std::chrono::seconds(offsetInSeconds);
}

void Countdown::ResetCountdownEvent() {
    for (CountdownEvent & ce : m_eventsVec)
        ce.m_enabled = true;
}

void Countdown::Pause() {
    m_pause = std::chrono::system_clock::now();
}

void Countdown::Continue() {
    std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();

    std::chrono::duration<float> elapsed_seconds = now - m_pause;
    printf("elapsed_seconds: %0.2f\n", elapsed_seconds.count());
    m_start += std::chrono::seconds((int)elapsed_seconds.count());
}


std::string Countdown::GetCountdownTime() {
    std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
    std::chrono::duration<double> remaining_seconds = (now - m_start) * -1;

    float fsec = remaining_seconds.count();

    for (CountdownEvent & ce : m_eventsVec) {
        if (ce.m_enabled && (float)ce.m_remainingSec + 1 >= fsec) {
            ce.m_enabled = false;
            ce.m_eventHandler(ce.m_remainingSec);
        }
    }

    if (fsec <= 0)
        return "0:0";

    int min = (int)fsec / 60;
    int sec = (int)fsec - min * 60;

    std::stringstream stream;
    stream << min << ":" << std::fixed << std::setprecision(2) << sec;
    return stream.str();
}