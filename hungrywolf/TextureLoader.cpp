/*
 * Copyright (C) 2016 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   TextureLoader.cpp
 * Author: ise
 * 
 * Created on October 26, 2016, 4:06 AM
 */

#include <GL/gl.h>
#include <GL/glu.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


#include "TextureLoader.h"



TextureLoader::TextureLoader() {
}

TextureLoader::~TextureLoader() {
}

GLuint TextureLoader::load(std::string filename) {
    GLuint id;
    glGenTextures(1, &id);

    SDL_Surface *img = IMG_Load(filename.c_str());
    if (!img) {
        fprintf(stderr, "[TextureLoader::load]: IMG_Load for \"%s\": %s\n", filename.c_str(), IMG_GetError());
        fflush(stderr);
        exit(1);
    }

    glBindTexture(GL_TEXTURE_2D, id);

    printf("TextureLoader::load \"%s\": %d\n", filename.c_str(), id);
    fflush(stdout);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img->w, img->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, img->pixels);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    SDL_FreeSurface(img);
    return id;
}

void TextureLoader::loadAll(std::vector<std::string> filenames, GLuint textureIds[]) {
    for (Uint32 i = 0; i < filenames.size(); i++) {
        textureIds[i] = load(filenames[i]);
    }
}

GLuint TextureLoader::loadAsMipmap(std::string filename) {
    GLuint id;
    glGenTextures(1, &id);

    if (SDL_WasInit(SDL_INIT_VIDEO) == 0) {
        fprintf(stderr, "[TextureLoader::loadAsMipmap]: SDL_WasInit for \"%s\": %s\n", filename.c_str(), IMG_GetError());
        fflush(stderr);
        exit(1);
    }

    SDL_Surface *img = IMG_Load(filename.c_str());
    if (!img) {
        fprintf(stderr, "[TextureLoader::loadAsMipmap]: IMG_Load for \"%s\": %s\n", filename.c_str(), IMG_GetError());
        fflush(stderr);
        exit(1);
    }

    printf("TextureLoader::loadAsMipmap \"%s\": %d      %d %d\n", filename.c_str(), id, img->w, img->h);
    fflush(stdout);

    glBindTexture(GL_TEXTURE_2D, id);
    gluBuild2DMipmaps(
        GL_TEXTURE_2D,
        GL_RGB,
        img->w, img->h,
        GL_RGB,
        GL_UNSIGNED_BYTE,
        img->pixels
    );
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    return id;
}