#!/bin/bash

read W H < <( file "$1" | cut -f 5,7 -d " " | sed 's/,$//' )


W4=$(( $W / 4 ))
H3=$(( $H / 3 ))

echo $W4 $H3

convert "$1" -crop "${W4}x${H3}+${W4}+0" +repage -gravity Northwest top.png

namelist=( "left.png" "front.png" "right.png" "back.png" )
i=0
for (( ofsx=0 ; $ofsx < $W ; ofsx=$[ $ofsx + $W4 ] )); do 
	convert "$1" -crop "${W4}x${H3}+${ofsx}+${H3}" +repage -gravity Northwest "${namelist[$i]}"
	i=$(($i + 1))
done

ofsy=$(( $H3 * 2))
convert "$1" -crop "${W4}x${H3}+${W4}+${ofsy}" +repage -gravity Northwest bottom.png


#convert top.png -flip top.png
#convert left.png -flip left.png
#convert front.png -flip front.png
#convert right.png -flip right.png
#convert back.png -flip back.png
#convert bottom.png -flip bottom.png

#convert top.png top.bmp
#convert left.png left.bmp
#convert front.png front.bmp
#convert right.png right.bmp
#convert back.png back.bmp
#convert bottom.png bottom.bmp
