/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Timer.cpp
 * Author: ise
 *
 * Created on March 22, 2017, 11:54 AM
 */


#include <iomanip>
#include <sstream>
#include <ctime>

#include "Timer.h"


Timer::Timer() {
    m_start = std::chrono::system_clock::now();
}

Timer::~Timer() {
}


void Timer::Reset() {
    m_start = std::chrono::system_clock::now();
}


float Timer::GetElapsedTimeInSeconds() {
    std::chrono::time_point<std::chrono::system_clock> now;
    now = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = now - m_start;
    return elapsed_seconds.count();
}

std::string Timer::GetElapsedTime() {
    float msec = GetElapsedTimeInSeconds();
    int min = (int)msec / 60;
    int sec = (int)msec - min * 60;

    std::stringstream stream;
    stream << min << ":" << std::fixed << std::setprecision(2) << sec;
    return stream.str();
}





