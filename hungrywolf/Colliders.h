/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Colliders.h
 * Author: ise
 *
 * Created on March 17, 2017, 4:24 AM
 */

#ifndef HUNGRYWOLF_COLLIDERS_H
#define HUNGRYWOLF_COLLIDERS_H


#include <vector>

#include "Settings.h"
#include "IRenderable.h"

#include "SphereCollider.h"
#include "BoxCollider.h"
#include "CollisionInfo.h"


class Colliders : public IRenderable {
public:

    Colliders(BoxCollider *boxCollider);

    virtual ~Colliders();

    void SetScale(float scale);

    void Render() override;

    SphereCollider *GetSphereColliders(std::vector<SphereCollider> **sphereColliderVec);

    virtual void Update(float px, float py, float pz, float facing, Point3 *bbmin, Point3 *bbmax) = 0;

    CollisionInfo RequestCollisionCheck();

protected:

    float m_scale;

    Settings *m_settings;

    SphereCollider m_containerSphere;
    std::vector<SphereCollider> m_sphereVec;

    BoxCollider *m_boxCollider;

};


#endif /* HUNGRYWOLF_COLLIDERS_H */
