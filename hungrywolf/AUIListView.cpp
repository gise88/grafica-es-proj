/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   AUIListView.cpp
 * Author: ise
 *
 * Created on March 31, 2017, 9:45 PM
 */



#include <string.h>

#include "AUIListView.h"
#include "GLUtils.h"


AUIListView::AUIListView() {
    strncpy(m_label, "lv label", UILISTVIEW_LABEL_MAX_LENGTH);
    m_alpha = 1;
    m_px = 0;
    m_py = 0;
    SetWidth(0);
    SetHeight(0);
    SetLabelFontSize(0.15);
}

AUIListView::~AUIListView() {
}

void AUIListView::SetLabel(const char *label) {
    strncpy(m_label, label, UILISTVIEW_LABEL_MAX_LENGTH);
}

const char* AUIListView::GetLabel() {
    return m_label;
}

void AUIListView::SetLabelFontSize(float fontSize) {
    m_labelFontSize = fontSize;
    m_labelFontHeight = glutStrokeHeight(GLUT_STROKE_ROMAN) * fontSize;
    SetHeight(m_itemsHeight);
}

void AUIListView::SetPosition(float x, float y) {
    m_px = x;
    m_py = y;
}

void AUIListView::GetPosition(float *x, float *y) {
    *x = m_px;
    *y = m_py;
}

void AUIListView::SetWidth(float w) {
    m_width = w;
    m_width2 = m_width / 2;
}

#define PADDING_Y 20
void AUIListView::SetHeight(float h) {
    m_itemsHeight = h;
    m_height = m_itemsHeight + PADDING_Y * 3 + m_labelFontHeight;
}

void AUIListView::SetTransparencyAlpha(float alpha) {
    m_alpha = alpha;
}

void AUIListView::Draw() {

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_LINE_SMOOTH);

    glColor4f(0.6f, 0.6f, 0.6f, m_alpha);

    glBegin(GL_QUADS);
    glVertex2i(m_px - m_width2, m_py);
    glVertex2i(m_px - m_width2, m_py + m_height);
    glVertex2i(m_px + m_width2, m_py + m_height);
    glVertex2i(m_px + m_width2, m_py);
    glEnd();

    glLineWidth(3);

    glColor4f(0.8f, 0.8f, 0.8f, m_alpha);

    glBegin(GL_LINE_STRIP);
    glVertex2i(m_px + m_width2, m_py);
    glVertex2i(m_px - m_width2, m_py);
    glVertex2i(m_px - m_width2, m_py + m_height);
    glEnd();

    glColor4f(0.4f, 0.4f, 0.4f, m_alpha);

    glBegin(GL_LINE_STRIP);
    glVertex2i(m_px - m_width2, m_py + m_height);
    glVertex2i(m_px + m_width2, m_py + m_height);
    glVertex2i(m_px + m_width2, m_py);
    glEnd();


    GLUtils::DrawTextShadow(
        GLUT_STROKE_ROMAN,
        m_label,
        m_px, m_py + PADDING_Y + m_labelFontHeight / 2,
        3, 2, m_labelFontSize, true
    );


    glLineWidth(1);
    glColor4f(0.5f, 0.5f, 0.5f, m_alpha);

    glBegin(GL_LINES);
    glVertex2i(m_px - m_width2 + 10, m_py + PADDING_Y * 1 + m_labelFontHeight);
    glVertex2i(m_px + m_width2 - 10, m_py + PADDING_Y * 1 + m_labelFontHeight);
    glEnd();


    DrawItems(m_px, m_py + PADDING_Y * 2 + m_labelFontHeight);


    glDisable(GL_BLEND);
    glDisable(GL_LINE_SMOOTH);
}