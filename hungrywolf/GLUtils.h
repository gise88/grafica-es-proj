/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   GLUtils.h
 * Author: ise
 *
 * Created on March 11, 2017, 3:38 PM
 */

#ifndef HUNGRYWOLF_GLUTILS_H
#define HUNGRYWOLF_GLUTILS_H


#include <GL/gl.h>
#include <GL/glu.h>


class GLUtils {
public:

    static void DrawCircleY(float cx, float cy, float cz, float r, int num_segments);
    static void DrawFilledCircle2D(float x, float y, float r);
    static void DrawCircle2D(float x, float y, float r, float lineWidth);
    static void DrawText(void *font, const char *str, float x, float y, float lineWidth, float scale, bool center);
    static void DrawTextShadow(void *font, const char *str, float x, float y, float lineWidth, float shadowSize, float scale, bool center);
    static void DrawTextWithBG(void *font, const char *str, float x, float y, float lineWidth, float scale, bool center);

private:
    GLUtils();

    virtual ~GLUtils();
};


#endif /* HUNGRYWOLF_GLUTILS_H */
