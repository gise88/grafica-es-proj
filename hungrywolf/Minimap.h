/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Minimap.h
 * Author: ise
 *
 * Created on March 18, 2017, 12:34 AM
 */

#ifndef HUNGRYWOLF_MINIMAP_H
#define HUNGRYWOLF_MINIMAP_H


#include <GL/gl.h>

#include <list>

#include "point3.h"
#include "Settings.h"
#include "Rigidbody.h"


class Minimap {
public:
    Minimap(Point3 bbmin, Point3 bbmax);

    virtual ~Minimap();

    void Render(Rigidbody *wolf, std::list<Rigidbody *> sheepList);

private:

    GLuint m_wolfTexId;
    GLuint m_sheepTexId;
    GLuint m_compassTexId;

    Point3 m_bbmin;
    Point3 m_bbmax;

    Settings *m_settings;
};


#endif /* HUNGRYWOLF_MINIMAP_H */
