/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Game.cpp
 * Author: ise
 *
 * Created on March 18, 2017, 1:40 AM
 */


#include <GL/gl.h>

#include "Game.h"
#include "Settings.h"



Game::Game(SDL_Window *win) {
    m_window = win;
    m_ui = new UI();
    m_settings = Settings::InstancePtr();
    m_camera = new Camera(Camera::CameraType::MOUSE);
    m_inputManager = new InputManager(
        std::bind(&Game::QuitHandler, this),
        std::bind(&Game::WindowEventHandler, this, std::placeholders::_1)
    );
    m_inputManager->SetCamera(m_camera);
    m_inputManager->SetUI(m_ui);
    m_quitProgram = false;
    m_fpsNow = 0;
    m_fps = 0;
}

Game::~Game() {
    delete m_ui;
    delete m_camera;
    delete m_inputManager;
}

void Game::Start() {

    SDL_Event e;

    Uint32 nStep = 0; // numero di passi di FISICA fatti fin'ora
    const Uint32 PHYS_SAMPLING_STEP = 10; // numero di millisec che un passo di fisica simula

    // Frames Per Seconds
    const int fpsSampling = 3000; // lunghezza intervallo di calcolo fps
    Uint32 timeLastInterval = 0; // quando e' cominciato l'ultimo intervallo

    while (!m_quitProgram) {
        // guardo se c'e' un evento:
        if (SDL_PollEvent(&e)) {
            m_inputManager->ProcessEvent(&e);
        } else {
            // nessun evento: siamo IDLE

            Uint32 timeNow = SDL_GetTicks(); // che ore sono?

            if (timeLastInterval + fpsSampling < timeNow) {
                m_fps = 1000.0f * ((float)m_fpsNow) / (timeNow - timeLastInterval);
                m_fpsNow = 0;
                timeLastInterval = timeNow;
            }

            bool somethingDone = false;
            int guardia = 0; // sicurezza da loop infinito

            // finche' il tempo simulato e' rimasto indietro rispetto
            // al tempo reale...
            while (nStep * PHYS_SAMPLING_STEP < timeNow) {
                DoStep();
                nStep++;
                somethingDone = true;
                timeNow = SDL_GetTicks();
                if (guardia++ > 1000) {
                    m_quitProgram = true;
                    break;
                } // siamo troppo lenti!
            }

            if (somethingDone)
                PreRender();
            else {
                // tempo libero!!!
            }
        }
    }
}

void Game::QuitHandler() {
    m_quitProgram = true;
}

void Game::WindowEventHandler(SDL_Event *event) {
    if (event->window.event == SDL_WINDOWEVENT_EXPOSED)
        PreRender();
    else {
        m_windowId = SDL_GetWindowID(m_window);
        if (event->window.windowID == m_windowId) {
            if (event->window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
                m_settings->screenWidth = event->window.data1;
                m_settings->screenHeight = event->window.data2;
                glViewport(0, 0, m_settings->screenWidth, m_settings->screenHeight);
                PreRender();
            }
        }
    }
}


void Game::PreRender() {
    // un frame in piu'!!!
    m_fpsNow++;



    Render();
}


