/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   InputManager.h
 * Author: ise
 *
 * Created on February 09, 2017, 6:48 PM
 */

#ifndef HUNGRYWOLF_INPUTMANAGER_H
#define HUNGRYWOLF_INPUTMANAGER_H


#include <functional>
#include <SDL2/SDL.h>


#include "UI.h"
#include "Settings.h"
#include "Camera.h"



class InputManager {
public:

    InputManager(
        std::function<void()> quitHandler,
        std::function<void(SDL_Event *)> windowEventHandler);

    virtual ~InputManager();

    void ResetUnhandledEventHandler();

    void SetCamera(Camera *camera);

    void SetUI(UI *ui);

    void SetUnhandledEventHandler(std::function<void(SDL_Event *)> unhandledEventHandler);

    void ProcessEvent(SDL_Event *event);

protected:

    Settings *m_settings;
    Camera *m_camera;
    UI *m_ui;

    std::function<void()> m_quitHandler;
    std::function<void(SDL_Event *)> m_windowEventHandler;
    std::function<void(SDL_Event *)> m_unhandledEventHandler;
};


#endif /* HUNGRYWOLF_INPUTMANAGER_H */
