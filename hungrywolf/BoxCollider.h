/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   BoxCollider.h
 * Author: ise
 *
 * Created on March 10, 2017, 10:13 PM
 */

#ifndef HUNGRYWOLF_BOXCOLLIDER_H
#define HUNGRYWOLF_BOXCOLLIDER_H


#include "point3.h"


class BoxCollider {
public:

    virtual void GetBoundingBox(Point3 *bbmin, Point3 *bbmax) = 0;

};


#endif /* HUNGRYWOLF_BOXCOLLIDER_H */
