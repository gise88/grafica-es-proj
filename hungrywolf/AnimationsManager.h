/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   AnimationsManager.h
 * Author: ise
 *
 * Created on March 21, 2017, 11:46 PM
 */

#ifndef HUNGRYWOLF_ANIMATIONSMANAGER_H
#define HUNGRYWOLF_ANIMATIONSMANAGER_H


#include <vector>
#include <unordered_map>

#include "Singleton.h"
#include "Animation.h"
#include "mesh.h"


class AnimationsManager : public Singleton<AnimationsManager> {
public:
    AnimationsManager();

    virtual ~AnimationsManager();

    void LoadAnimation(std::string tag, const char* pattern, int start, int end, float scale);

    Animation* NewAnimationFromTag(std::string tag);

private:

    std::unordered_map<std::string, std::vector<Mesh *> > m_meshesMap;
};


#endif /* HUNGRYWOLF_ANIMATIONSMANAGER_H */
