/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   UIListView.h
 * Author: ise
 *
 * Created on March 31, 2017, 10:28 AM
 */

#ifndef HUNGRYWOLF_UILISTVIEW_H
#define HUNGRYWOLF_UILISTVIEW_H


#include "AUIListView.h"


class UIListView : public AUIListView {
public:

    UIListView();

    ~UIListView();

    void SetListItems(const std::list<int> list);

    void SetListItems(const std::list<std::string> list);

    void SetFontSize(float fontSize);

protected:

    float m_fontSize;
    float m_fontHeight;

    virtual void DrawItems(float xCenter, float y) override;

    std::list<std::string> m_items;
};





#endif /* HUNGRYWOLF_UILISTVIEW_H */
