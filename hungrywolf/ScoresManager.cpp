/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   ScoresManager.cpp
 * Author: ise
 *
 * Created on April 04, 2017, 2:25 PM
 */


#include <fstream>

#include "ScoresManager.h"


#define FILE_BEST_SCORES "best-scores.txt"

ScoresManager::ScoresManager(int maxCountScores) {
    m_maxCountScores = maxCountScores;
    m_scoreList = ReadScoresFromFile();
    m_scoreList.sort(std::greater<int>());
}

ScoresManager::~ScoresManager() {
    WriteScoresToFile(m_scoreList);
}

std::list<int> ScoresManager::GetScores() {
    return m_scoreList;
}

void ScoresManager::AddScore(int score) {
    m_scoreList.push_front(score);
    m_scoreList.sort(std::greater<int>());
    while (m_scoreList.size() > (size_t)m_maxCountScores)
        m_scoreList.pop_back();
}

std::list<int> ScoresManager::ReadScoresFromFile() {
    std::list<int> scores;
    int a;

    std::ifstream inFile(FILE_BEST_SCORES);

    if(!inFile.is_open()) {
        printf(FILE_BEST_SCORES " does not exist\n");
        return scores;
    }

    while (inFile >> a)
        scores.push_front(a);

    inFile.close();

    return scores;
}

void ScoresManager::WriteScoresToFile(std::list<int> scores) {
    std::ofstream outFile(FILE_BEST_SCORES);
    if(!outFile.is_open()) {
        printf("!outFile.is_open()\n");
        return;
    }

    for (int score : scores)
        outFile << score << std::endl;

    outFile.close();
}