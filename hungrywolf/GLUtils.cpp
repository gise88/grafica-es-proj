/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   GLUtils.cpp
 * Author: ise
 *
 * Created on March 11, 2017, 3:38 PM
 */

#include <stdio.h>
#include <GL/glu.h>
#include <GL/freeglut.h>

#include <math.h>

#include "GLUtils.h"



GLUtils::GLUtils() {
}

GLUtils::~GLUtils() {
}

void GLUtils::DrawCircleY(float cx, float cy, float cz, float r, int num_segments) {
    glBegin(GL_LINE_LOOP);
    for (int ii = 0; ii < num_segments; ii++) {
        float theta = 2.0f * 3.1415926f * float(ii) / float(num_segments);//get the current angle
        float x = r * cosf(theta); //calculate the x component
        float z = r * sinf(theta); //calculate the y component
        glVertex3f(x + cx, cy, z + cz); //output vertex
    }
    glEnd();
}

void GLUtils::DrawFilledCircle2D(float x, float y, float r) {
    glBegin(GL_TRIANGLE_FAN);
    glVertex2f(x, y);
    for (float i = 0.0f; i <= 360; i++)
        glVertex2f(r * cos(M_PI * i / 180.0) + x, r * sin(M_PI * i / 180.0) + y);
    glEnd();
}

void GLUtils::DrawCircle2D(float x, float y, float r, float lineWidth) {
    glLineWidth(lineWidth);
    glBegin(GL_LINE_LOOP);
    for (float i = 0.0f; i <= 360; i++)
        glVertex2f(r * cos(M_PI * i / 180.0) + x, r * sin(M_PI * i / 180.0) + y);
    glEnd();
}

void GLUtils::DrawText(void *font, const char *str, float x, float y, float lineWidth, float scale, bool center) {
    glPushMatrix();
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glLineWidth(lineWidth);
    glTranslatef(x, y, 0);
    glScalef(scale, -scale, 1);
    if (center) {
        float len = 0;
        for (const char *c = str; *c != '\0'; c++)
            len += glutStrokeWidthf(font, (int)*c);
        glTranslatef(-len / 2, 0, 0);
    }
    glutStrokeString(font, (unsigned char *)str);
    glDisable(GL_BLEND);
    glDisable(GL_LINE_SMOOTH);
    glPopMatrix();
}


void GLUtils::DrawTextShadow(void *font, const char *str, float x, float y, float lineWidth, float shadowSize, float scale, bool center) {
    glColor3f(0, 0, 0);
    DrawText(font, str, x - shadowSize, y - shadowSize, lineWidth, scale, center);
    DrawText(font, str, x - shadowSize, y + shadowSize, lineWidth, scale, center);
    DrawText(font, str, x + shadowSize, y - shadowSize, lineWidth, scale, center);
    DrawText(font, str, x + shadowSize, y + shadowSize, lineWidth, scale, center);
    glColor3f(1, 1, 1);
    DrawText(font, str, x, y, lineWidth, scale, center);
}

void GLUtils::DrawTextWithBG(void *font, const char *str, float x, float y, float lineWidth, float scale, bool center) {
    glPushMatrix();
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glLineWidth(2.0);

    glPushMatrix();
    glTranslatef(x, y, 0);
    float width = 0;
    float height = glutStrokeHeight(font);
    for (const char *c = str; *c != '\0'; c++)
        width += glutStrokeWidthf(font, (int)*c);
    glPopMatrix();

    glPushMatrix();
    glTranslatef(x, y, 0);
    glScalef(scale, -scale, 1);
    if (center)
        glTranslatef(-width / 2, -height / 2, 0);
    glTranslatef(0, -height / 4, 0);
    glColor4f(0.4, 0.4, 0.4, 0.9);
    glBegin(GL_QUADS);
#define PADDING_X 10
#define PADDING_Y 10
    glVertex2f(-PADDING_X, height + PADDING_Y);
    glVertex2f(-PADDING_X, -PADDING_Y);
    glVertex2f(width + PADDING_X, -PADDING_Y);
    glVertex2f(width + PADDING_X, height + PADDING_Y);
    glEnd();
    glPopMatrix();

    glColor3f(1, 1, 1);
    glTranslatef(x, y, 0);
    glScalef(scale, -scale, 1);
    if (center)
        glTranslatef(-width / 2, -height / 2, 0);
    glutStrokeString(font, (unsigned char *)str);

    glDisable(GL_BLEND);
    glDisable(GL_LINE_SMOOTH);
    glPopMatrix();
}