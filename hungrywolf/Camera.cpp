/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Camera.cpp
 * Author: ise
 *
 * Created on February 10, 2017, 1:19 PM
 */



#include <stdexcept>
#include <string>

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>

#include "Camera.h"



Camera::Camera(CameraType initCameraType) {
    bool found = false;
    for (uint32_t i = 0; i < m_cameraTypes.size(); i++) {
        if (m_cameraTypes[i] == initCameraType) {
            m_indexCameraType = i;
            found = true;
        }
    }
    if (! found)
        throw std::invalid_argument("Some missing value int the m_cameraTypes array (file: Camera.h)");

    m_eyeDist = 5.0f;
    m_viewAlpha = 0;
    m_viewBeta = 40;
}

Camera::~Camera() {
}


void Camera::ResetCamera() {
    m_eyeDist = 5.0f;
    m_viewAlpha = 0;
    m_viewBeta = 40;
}


Camera::CameraType Camera::GetCameraType() {
    return m_cameraTypes[m_indexCameraType];
}

void Camera::NextCameraType() {
    m_indexCameraType = (m_indexCameraType + 1) % m_cameraTypes.size();
    switch (m_cameraTypes[m_indexCameraType]) {
        case CameraType::BACK_FIXED:
            printf("CameraType::BACK_FIXED\n");
            break;
        case CameraType::RIGHT_FIXED:
            printf("CameraType::RIGHT_FIXED\n");
            break;

        case CameraType::TOP_FIXED:
            printf("CameraType::TOP_FIXED\n");
            break;

        case CameraType::HEAD:
            printf("CameraType::HEAD\n");
            break;

        case CameraType::MOUSE:
            printf("CameraType::MOUSE\n");
            break;
    }
    fflush(stdout);
}


void Camera::ZoomIn() {
    m_eyeDist = m_eyeDist * 0.9;
    if (m_eyeDist < 1)
        m_eyeDist = 1;
}

void Camera::ZoomOut() {
    m_eyeDist = m_eyeDist / 0.9f;
    if (m_eyeDist > 40)
        m_eyeDist = 40;
}


void Camera::RotateLeft() {
    m_viewAlpha = fmod(m_viewAlpha + 3.0, 360);
}

void Camera::RotateRight() {
    m_viewAlpha = fmod(m_viewAlpha - 3.0, 360);
}


void Camera::MoveUp() {
    m_viewBeta += 3.0;
    if (m_viewBeta > +90)
        m_viewBeta = +90;
}

void Camera::MoveDown() {
    m_viewBeta -= 3.0;
    if (m_viewBeta < -45)
        m_viewBeta = -45;
}


void Camera::SetViewAngles(int xrel, int yrel) {
    m_viewAlpha += xrel * 0.4f;
    m_viewBeta += yrel * 0.4f;
    if (m_viewBeta < -45)
        m_viewBeta = -45;
    if (m_viewBeta > +90)
        m_viewBeta = +90;
}


void Camera::Update(float px, float py, float pz, float angle) {
    double cosf = cos(angle * M_PI / 180.0);
    double sinf = sin(angle * M_PI / 180.0);
    double camd, camh, ex, ey, ez, cx, cy, cz;
    double cosff, sinff;

    // controllo la posizione della camera a seconda dell'opzione selezionata
    switch (m_cameraTypes[m_indexCameraType]) {

    case CameraType::BACK_FIXED:
        camd = 2.5;
        camh = 1.0;
        ex = px + camd * sinf;
        ey = py + camh;
        ez = pz + camd * cosf;
        cx = px - camd * sinf;
        cy = py + camh;
        cz = pz - camd * cosf;
        gluLookAt(ex, ey, ez, cx, cy, cz, 0.0, 1.0, 0.0);
        break;

    case CameraType::RIGHT_FIXED:
        camd = 0.5;
        camh = 0.65;
        angle = angle + 65.0;
        cosff = cos(angle * M_PI / 180.0);
        sinff = sin(angle * M_PI / 180.0);
        ex = px + camd * sinff;
        ey = py + camh;
        ez = pz + camd * cosff;
        cx = px - camd * sinf;
        cy = py + camh;
        cz = pz - camd * cosf;
        gluLookAt(ex, ey, ez, cx, cy, cz, 0.0, 1.0, 0.0);
        break;

    case CameraType::TOP_FIXED:
        camd = 3.5;
        camh = 1.0;
        ex = px + camd * sinf;
        ey = py + camh;
        ez = pz + camd * cosf;
        cx = px - camd * sinf;
        cy = py + camh;
        cz = pz - camd * cosf;
        gluLookAt(ex, ey + 5, ez, cx, cy, cz, 0.0, 1.0, 0.0);
        break;

    case CameraType::HEAD:
        camd = 0.1;
        camh = 1.15;
        ex = px + camd * sinf;
        ey = py + camh;
        ez = pz + camd * cosf;
        cx = px - camd * sinf;
        cy = py + camh;
        cz = pz - camd * cosf;
        gluLookAt(ex, ey, ez, cx, cy, cz, 0.0, 1.0, 0.0);
        break;

    case CameraType::MOUSE:
        camd = 0.2;
        camh = 0.55;
        ex = px + camd * sinf;
        ey = py + camh;
        ez = pz + camd * cosf;
        cx = px - camd * sinf;
        cy = py + camh;
        cz = pz - camd * cosf;

        glTranslatef(0, 0, -m_eyeDist);
        glRotatef(m_viewBeta, 1, 0, 0);
        glRotatef(m_viewAlpha, 0, 1, 0);

        gluLookAt(ex, ey, ez, cx, cy, cz, 0.0, 1.0, 0.0);
        break;
    }
}