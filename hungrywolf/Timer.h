/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Timer.h
 * Author: ise
 *
 * Created on March 22, 2017, 11:54 AM
 */

#ifndef HUNGRYWOLF_TIMER_H
#define HUNGRYWOLF_TIMER_H


#include <chrono>
#include <string>


class Timer {
public:
    Timer();

    virtual ~Timer();

    void Reset();
    float GetElapsedTimeInSeconds();
    std::string GetElapsedTime();

private:

    std::chrono::time_point<std::chrono::system_clock> m_start;

};


#endif /* HUNGRYWOLF_TIMER_H */
