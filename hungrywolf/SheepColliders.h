/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   SheepColliders.h
 * Author: ise
 *
 * Created on March 17, 2017, 2:59 AM
 */

#ifndef HUNGRYWOLF_SHEEPCOLLIDERS_H
#define HUNGRYWOLF_SHEEPCOLLIDERS_H


#include <vector>

#include "Colliders.h"

#include "SphereCollider.h"
#include "BoxCollider.h"


class SheepColliders : public Colliders {
public:
    SheepColliders(BoxCollider *boxCollider);

    virtual ~SheepColliders();

    virtual void Update(float px, float py, float pz, float facing, Point3 *bbmin, Point3 *bbmax) override;
};


#endif /* HUNGRYWOLF_SHEEPCOLLIDERS_H */
