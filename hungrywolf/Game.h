/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Game.h
 * Author: ise
 *
 * Created on March 18, 2017, 1:40 AM
 */

#ifndef HUNGRYWOLF_GAME_H
#define HUNGRYWOLF_GAME_H


#include "IRenderable.h"

#include "UI.h"
#include "Settings.h"
#include "Camera.h"
#include "InputManager.h"



class Game : public IRenderable {
public:
    Game(SDL_Window *win);

    virtual ~Game();

    void Start();

    virtual void DoStep() = 0;

private:

    void PreRender();

protected:

    // valore di fps dell'intervallo precedente
    float m_fps;

    SDL_Window *m_window;

    UI *m_ui;

    Camera *m_camera;

    InputManager *m_inputManager;

    Settings *m_settings;

    virtual void QuitHandler();

    void WindowEventHandler(SDL_Event *event);

private:

    // quanti fotogrammi ho disegnato fin'ora nell'intervallo attuale
    Uint32 m_fpsNow;

    bool m_quitProgram;

    Uint32 m_windowId;


};


#endif /* HUNGRYWOLF_GAME_H */
