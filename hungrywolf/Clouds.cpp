/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Clouds.cpp
 * Author: ise
 *
 * Created on October 26, 2016, 3:09 AM
 */


#include <algorithm>
#include <random>

#include "Clouds.h"
#include "Settings.h"
#include "Utils.h"
#include "CloudsPngFinder.h"
#include "TextureLoader.h"



Clouds::Clouds(float maxDist)
    : m_cloudsObjFinder() {
    m_maxDistance = maxDist;

    CloudsPngFinder cloudsPngFinder;
    std::vector<std::string> &pngImages = cloudsPngFinder.getAll();
    m_nPngClouds = pngImages.size();
    m_cloudsTexId = new GLuint[m_nPngClouds];
    TextureLoader::loadAll(pngImages, m_cloudsTexId);
}

Clouds::~Clouds() {
    glDeleteTextures(m_nPngClouds, m_cloudsTexId);
    delete[] m_cloudsTexId;
    for (std::vector<Cloud *> cloudsGroup : m_cloudsGroupsVec)
        for (Cloud *cloud : cloudsGroup)
            delete cloud;
}

void Clouds::createGroups() {
    uint32_t groupsCount = Settings::Instance().cloudsGroupsCount;
    std::vector<std::string> &objs = m_cloudsObjFinder.getAll();
    m_cloudsGroupsVec.reserve(groupsCount);
    auto engine = std::default_random_engine{ };
    uint32_t cloudsInGroup = Settings::Instance().cloudsInGroup;
    for (uint32_t i = 0; i < groupsCount; i++) {
        float maxDist = m_maxDistance - Utils::randFloat(0, 50);
        float phi = 360.0f / groupsCount * i;
        float theta = Utils::randFloat(20, 45);
        float rotSpeed = Utils::randSign() * Utils::randFloat(0.01, 0.02);

        uint32_t texStartIndex = Utils::randUInt(0, m_nPngClouds - 1);
        uint32_t objStartIndex = Utils::randUInt(0, objs.size() - 1);

        std::shuffle(std::begin(objs), std::end(objs), engine);

        std::vector<Cloud *> cloudsGroupVec;
        for (uint32_t j = 0; j < cloudsInGroup; j++) {
            GLuint texId = m_cloudsTexId[texStartIndex++ % m_nPngClouds];
            const char *objFilename = objs[objStartIndex++ % objs.size()].c_str();
            cloudsGroupVec.push_back(new Cloud(texId, objFilename, maxDist, phi, theta, rotSpeed));
        }
        m_cloudsGroupsVec.push_back(std::move(cloudsGroupVec));
    }
}

void Clouds::draw(float px, float py, float pz) {

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_TEXTURE_GEN_S);
    glEnable(GL_TEXTURE_GEN_T);
    glEnable(GL_BLEND);

    glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);

    glPushMatrix();
    for (std::vector<Cloud *> cloudsGroup : m_cloudsGroupsVec)
        for (Cloud *cloud : cloudsGroup)
            cloud->move(px, py, pz);
    glPopMatrix();

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_TEXTURE_GEN_S);
    glDisable(GL_TEXTURE_GEN_T);
    glDisable(GL_BLEND);
}


Clouds::Cloud::Cloud(GLuint texId, const char *objFilename, float maxDist, float phi, float theta, float rotSpeed)
    : m_cloudMesh(Mesh(objFilename, Utils::randFloat(5, 10))) {
    m_texId = texId;
    m_maxDistance = maxDist;
    m_dist = maxDist - Utils::randFloat(0, 50);
    m_phiAngle = phi + Utils::randFloat(-5, 5);
    m_thetaAngle = theta + Utils::randFloat(-5, 5);
    m_rotationSpeed = rotSpeed * Utils::randFloat(1.01, 1.02);
    m_alphaColor = Utils::randFloat(0, 1);
}

Clouds::Cloud::~Cloud() {
}

void Clouds::Cloud::move(float px, float py, float pz) {


    glPushMatrix();
    glTranslatef(px, py, pz);

    m_phiAngle = m_phiAngle + m_rotationSpeed;
    m_alphaColor = m_alphaColor + Utils::randFloat(0.003f, 0.007f);

    glRotatef(m_phiAngle, 0, 1, 0);
    glRotatef(m_thetaAngle, 0, 0, 1);

    glTranslatef(m_dist, 0, 0);
    glBindTexture(GL_TEXTURE_2D, m_texId);

    glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_OBJECT_LINEAR);
    float sz = 1.0f / (m_cloudMesh.bbmin.Z() - m_cloudMesh.bbmax.Z());
    float ty = 1.0f / (m_cloudMesh.bbmin.Y() - m_cloudMesh.bbmax.Y());
    float s[4] = {0, 0, sz, -m_cloudMesh.bbmax.Z() * sz};
    float t[4] = {0, ty, 0, -m_cloudMesh.bbmax.Y() * ty};
    glTexGenfv(GL_S, GL_OBJECT_PLANE, s);
    glTexGenfv(GL_T, GL_OBJECT_PLANE, t);

    glColor4f(0.9, 0.9, 0.9, fmax(0.5, fabs(cos(m_alphaColor))));
    m_cloudMesh.RenderNxV();

    glPopMatrix();
}











