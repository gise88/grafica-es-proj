/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Environment.cpp
 * Author: ise
 *
 * Created on February 09, 2017, 8:28 PM
 */


#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>

#include "Environment.h"
#include "TextureLoader.h"




Environment::Environment( float size, int ranchWidth, int ranchHeight)
    : m_cloudySky(size) {
    m_size = size;
    m_grassTexId = TextureLoader::loadAsMipmap("res/img/seamless_grass_texture.jpg");

    m_ranch = new Ranch(ranchWidth, ranchHeight);
    Point3 bbmin, bbmax;
    m_ranch->GetBoundingBox(&bbmin, &bbmax);
    m_billboard = new Billboard(bbmax.X(), bbmax.Z());
    m_settings = Settings::InstancePtr();
}

Environment::~Environment() {
    delete m_ranch;
    delete m_billboard;
    glDeleteTextures(1, &m_grassTexId);
}

BoxCollider* Environment::GetBoxCollider() {
    return m_ranch;
}

void Environment::Render(float px, float py, float pz) {
    m_cloudySky.draw(px, py, pz);
    DrawFloor(px, py, pz);
    m_ranch->Render();
    m_billboard->Render();
}

void Environment::DrawFloor(float px, float py, float pz) {
    const float S = 400; // size
    const float H = 0; // altezza
    const int K = 150; //disegna K x K quads

    if (!m_settings->useWireframe) {
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D, m_grassTexId);
        glEnable(GL_TEXTURE_GEN_S);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
//    glTexGeni(GL_S, GL_TEXTURE_GEN_MODE , GL_OBJECT_LINEAR);

        glEnable(GL_TEXTURE_GEN_T);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
//    glTexGeni(GL_T, GL_TEXTURE_GEN_MODE , GL_OBJECT_LINEAR);

        float s[3]={0, 0, 1};
        float t[3]={1, 0, 0};
        glTexGenfv(GL_S, GL_OBJECT_PLANE, s);
        glTexGenfv(GL_T, GL_OBJECT_PLANE, t);

        glFrontFace(GL_CW); // consideriamo Front Facing le facce ClockWise

        glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE,  GL_MODULATE);

        glColor3f(1, 1, 1); // colore uguale x tutti i quads
        glBegin(GL_QUADS);
    } else {
        glBegin(GL_LINE_LOOP);
    }
    // glColor3f(0.749, 0.686, 0.541); // colore uguale x tutti i quads

    // disegna KxK quads
    glNormal3f(0, 1, 0); // normale verticale uguale x tutti
    for (int x = 0; x < K; x++)
        for (int z = 0; z < K; z++) {
            float x0 = -S + 2 * (x + 0) * S / K;
            float x1 = -S + 2 * (x + 1) * S / K;
            float z0 = -S + 2 * (z + 0) * S / K;
            float z1 = -S + 2 * (z + 1) * S / K;
            glVertex3d(x0 + px, H + py, z0 + pz);
            glVertex3d(x1 + px, H + py, z0 + pz);
            glVertex3d(x1 + px, H + py, z1 + pz);
            glVertex3d(x0 + px, H + py, z1 + pz);
        }
    glEnd();

    glDisable(GL_TEXTURE_2D);
    glDisable(GL_TEXTURE_GEN_S);
    glDisable(GL_TEXTURE_GEN_T);
    glFrontFace(GL_CCW);
}