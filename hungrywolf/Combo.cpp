/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Combo.cpp
 * Author: ise
 *
 * Created on March 25, 2017, 11:12 AM
 */


#include <stdio.h>


#include "Combo.h"
#include "TextureLoader.h"



Combo::Combo() {
    m_comboTexId = TextureLoader::load("res/img/combo/combo.png");
    m_multiplierTexId = TextureLoader::load("res/img/combo/x.png");
    char name[31];
    for (int i = 0; i <= 9; i++) {
        snprintf(name, 30, "res/img/combo/%d.png", i);
        m_texIdNumsVec.push_back(TextureLoader::load(name));
    }

    m_settings = Settings::InstancePtr();
    m_comboValue = 0;

    m_alphaColor = 1;

    m_comboWidth = 250;
    m_comboHeight = 75;

    m_xWidth = 50;
    m_xHeight = 35;

    m_digitWidth = 70;
    m_digitHeight = 50;
}

Combo::~Combo() {
    glDeleteTextures(1, &m_comboTexId);
    glDeleteTextures(1, &m_multiplierTexId);
    for (GLuint texId : m_texIdNumsVec)
        glDeleteTextures(1, &texId);
}

void Combo::IncrementComboValue() {
    m_comboValue++;
    m_alphaColor = 1;
    m_timer.Reset();
}

void Combo::Reset() {
    m_comboValue = 0;
}

std::list<int> Combo::IntToDigits(int x) {
    std::list<int> returnValue;
    while (x >= 10) {
        returnValue.push_front(x % 10);
        x = x / 10;
    }
    returnValue.push_front(x);
    return returnValue;
}

void Combo::Render() {

    if (m_comboValue > 0 && m_alphaColor > 0.01) {

        if (m_timer.GetElapsedTimeInSeconds() > 1.5)
            m_alphaColor -= 0.02;

        float screenWidth2 = m_settings->screenWidth / 2.0f;
        float comboCenterY = m_settings->screenHeight - 150;

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glColor4f(1, 1, 1, m_alphaColor);

        glPushMatrix();
        glBindTexture(GL_TEXTURE_2D, m_comboTexId);
        glEnable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
//        glColor3f(1, 1, 1);
        glTexCoord2i(0, 0);
        glVertex2d(screenWidth2 - m_comboWidth, comboCenterY + m_comboHeight);
        glTexCoord2i(0, 1);
        glVertex2d(screenWidth2 - m_comboWidth, comboCenterY - m_comboHeight);
        glTexCoord2i(1, 1);
        glVertex2d(screenWidth2 + m_comboWidth, comboCenterY - m_comboHeight);
        glTexCoord2i(1, 0);
        glVertex2d(screenWidth2 + m_comboWidth, comboCenterY + m_comboHeight);
        glEnd();
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();

        if (m_comboValue > 1) {

            std::list<int> digits = IntToDigits(m_comboValue);

            float multiplierWidth = m_xWidth + m_digitWidth * digits.size();
            float startX = screenWidth2 - multiplierWidth/2;
            float multiplierY = comboCenterY - 60;

            glPushMatrix();
            glBindTexture(GL_TEXTURE_2D, m_multiplierTexId);
            glEnable(GL_TEXTURE_2D);
            glBegin(GL_QUADS);
//            glColor3f(1, 1, 1);
            glTexCoord2i(0, 0);
            glVertex2d(startX, multiplierY + m_xHeight);
            glTexCoord2i(0, 1);
            glVertex2d(startX, multiplierY - m_xHeight);
            glTexCoord2i(1, 1);
            glVertex2d(startX + m_xWidth, multiplierY - m_xHeight);
            glTexCoord2i(1, 0);
            glVertex2d(startX + m_xWidth, multiplierY + m_xHeight);
            glEnd();
            glDisable(GL_TEXTURE_2D);
            glPopMatrix();

            startX += m_xWidth;

            for (int x : digits) {

                glPushMatrix();
                glBindTexture(GL_TEXTURE_2D, m_texIdNumsVec[x]);
                glEnable(GL_TEXTURE_2D);
                glBegin(GL_QUADS);
//                glColor3f(1, 1, 1);
                glTexCoord2i(0, 0);
                glVertex2d(startX, multiplierY + m_digitHeight);
                glTexCoord2i(0, 1);
                glVertex2d(startX, multiplierY - m_digitHeight);
                glTexCoord2i(1, 1);
                glVertex2d(startX + m_digitWidth, multiplierY - m_digitHeight);
                glTexCoord2i(1, 0);
                glVertex2d(startX + m_digitWidth, multiplierY + m_digitHeight);
                glEnd();
                glDisable(GL_TEXTURE_2D);
                glPopMatrix();

                startX += m_digitWidth;
            }
        }

        glDisable(GL_BLEND);
    }
}