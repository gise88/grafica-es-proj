/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Settings.h
 * Author: ise
 *
 * Created on February 10, 2017, 4:17 PM
 */

#ifndef HUNGRYWOLF_SETTINGS_H
#define HUNGRYWOLF_SETTINGS_H



#include <stdint.h>

#include "Singleton.h"



class Settings : public Singleton<Settings> {
public:
    Settings();

    virtual ~Settings();


    const char *title;

    int screenWidth;
    int screenHeight;

    float worldSize;

    bool showInfoWindow;

    uint32_t cloudsGroupsCount;
    uint32_t cloudsInGroup;

    float wolfScale;
    float sheepScale;

    bool useWireframe;
    bool useShadow;
    bool useBoundingBox;
    bool useEnvmap;

    bool billboardBack;

    bool useOnlyHeadCollider;

    bool debugInfo;

    int ranchWidth;
    int ranchHeight;

private:
};


#endif /* HUNGRYWOLF_SETTINGS_H */
