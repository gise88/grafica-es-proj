/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   UI.cpp
 * Author: ise
 *
 * Created on March 23, 2017, 6:19 PM
 */



#include "UI.h"



UI::UI() {
    m_latestButtonPressed = nullptr;
}

UI::~UI() {
    for (UIButton *uiButton : m_buttonVec)
        delete uiButton;

    for (UIListView *uiListView : m_listViewVec)
        delete uiListView;
}

UIButton* UI::CreateButton() {
    UIButton *b = new UIButton();
    m_buttonVec.push_back(b);
    return b;
}

UIListView* UI::CreateListView() {
    UIListView* lv = new UIListView();
    m_listViewVec.push_back(lv);
    return lv;
}

void UI::ClearAll() {
    for (UIButton *uiButton : m_buttonVec)
        delete uiButton;
    m_buttonVec.clear();
    m_latestButtonPressed = nullptr;

    for (UIListView *uiListView : m_listViewVec)
        delete uiListView;
    m_listViewVec.clear();
}

bool IsMouseInside(float mx, float my, float px, float py, float w, float h) {
    if (mx >= px && mx <= px + w && my >= py && my <= py + h)
        return true;
    return false;
}

bool UI::ProcessEvent(SDL_Event *event) {
    float x, y, w, h;

    if (event->type == SDL_MOUSEMOTION) {
        if (m_latestButtonPressed != nullptr) {
            m_latestButtonPressed->GetBounds(&x, &y, &w, &h);
            if (IsMouseInside(event->motion.x, event->motion.y, x, y, w, h))
                m_latestButtonPressed->SetPressed(true);
            else
                m_latestButtonPressed->SetPressed(false);
        }
        for (UIButton *b : m_buttonVec) {
            b->GetBounds(&x, &y, &w, &h);
            if (IsMouseInside(event->motion.x, event->motion.y, x, y, w, h)) {
                b->SetHighlighted(true);
                return true;
            } else {
                b->SetHighlighted(false);
            }
        }
    } else if (event->type == SDL_MOUSEBUTTONDOWN && event->button.button == SDL_BUTTON_LEFT) {
        for (UIButton *b : m_buttonVec) {
            b->GetBounds(&x, &y, &w, &h);
            if (IsMouseInside(event->motion.x, event->motion.y, x, y, w, h)) {
                b->SetPressed(true);
                m_latestButtonPressed = b;
                return true;
            }
        }
    } else if (event->type == SDL_MOUSEBUTTONUP && event->button.button == SDL_BUTTON_LEFT) {
        if (m_latestButtonPressed != nullptr) {
            m_latestButtonPressed->GetBounds(&x, &y, &w, &h);
            if (IsMouseInside(event->motion.x, event->motion.y, x, y, w, h)) {
                m_latestButtonPressed->EmitClickEvent();
                return true;
            } else {
                m_latestButtonPressed->SetPressed(false);
                m_latestButtonPressed = nullptr;
            }
        }
    }
    return false;
}

void UI::Draw() {
    for (UIButton *uiButton : m_buttonVec)
        uiButton->Draw();

    for (UIListView *uiListView : m_listViewVec)
        uiListView->Draw();
}