/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   HungryWolfInputManager.cpp
 * Author: ise
 *
 * Created on March 23, 2017, 10:58 PM
 */



#include "HungryWolfInputManager.h"



HungryWolfInputManager::HungryWolfInputManager(Wolf *wolf) {
    m_wolf = wolf;
}

HungryWolfInputManager::~HungryWolfInputManager() {
}


void HungryWolfInputManager::ProcessEvent(SDL_Event *event) {

    switch (event->type) {
        case SDL_KEYDOWN:

            switch (event->key.keysym.sym) {
                case SDLK_w:
                    m_wolf->SetMode(Wolf::Mode::RUN);
                    break;
                case SDLK_a:
                    m_wolf->SetDirection(Wolf::Direction::LEFT);
                    break;
                case SDLK_d:
                    m_wolf->SetDirection(Wolf::Direction::RIGHT);
                    break;
                case SDLK_s:
                    m_wolf->SetMode(Wolf::Mode::SIT_DOWN);
                    break;
            }
            break;

        case SDL_KEYUP:
            switch (event->key.keysym.sym) {
                case SDLK_w:
                    m_wolf->SetMode(Wolf::Mode::IDLE);
                    break;
                case SDLK_a:
                case SDLK_d:
                    m_wolf->SetDirection(Wolf::Direction::AHEAD);
                    break;
            }
            break;
    }
}