/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Billboard.cpp
 * Author: ise
 *
 * Created on March 19, 2017, 2:37 AM
 */



#include "Billboard.h"

#include "TextureLoader.h"
#include "Settings.h"



Billboard::Billboard(float px, float pz) {

    float scale = 5;

    m_px = px + 10;
    m_pz = pz + 10;

    m_offsY = 3.42021f * scale;
    m_rot = 0;
    m_state = State::FRONT_SIDE;

    m_billboard = new Mesh("res/obj/Billboard/billboard.obj", scale);
    m_base = new Mesh("res/obj/Billboard/base.obj", scale);

    m_texId = TextureLoader::loadAsMipmap("res/obj/Billboard/Billboard tex.jpg");
}

Billboard::~Billboard() {
    delete m_base;
    delete m_billboard;
    glDeleteTextures(1, &m_texId);
}

void Billboard::Render() {
    glPushMatrix();

    glTranslatef(m_px, 0, m_pz);
    glScalef(1, 1, -1);
    glRotatef(135, 0, 1, 0);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, m_texId);
    glColor3f(1, 1, 1);
    glDisable(GL_LIGHTING);

    m_base->RenderNxVTex();

    glPushMatrix();

    glTranslatef(0, m_offsY, 0);

    glRotatef(-20, 0, 0, 1);

    switch (m_state) {
        case State::FRONT_SIDE:
            if (Settings::Instance().billboardBack) {
                m_rot = -1;
                m_state = State::GO_TO_BACK;
                glRotatef(m_rot, 0, 0, 1);
            }
            break;
        case State::BACK_SIDE:
            glRotatef(180, 0, 0, 1);
            if (!Settings::Instance().billboardBack) {
                m_rot = -179;
                m_state = State::GO_TO_FRONT;
            }
            break;
        case State::GO_TO_BACK:
            m_rot -= 1;
            glRotatef(m_rot, 0, 0, 1);
            if (m_rot <= -180)
                m_state = State::BACK_SIDE;
            break;
        case State::GO_TO_FRONT:
            m_rot += 1;
            glRotatef(m_rot, 0, 0, 1);
            if (m_rot >= 0)
                m_state = State::FRONT_SIDE;
            break;

    }
    m_billboard->RenderNxVTex();

    glPopMatrix();
    glPopMatrix();
}