/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Animation.cpp
 * Author: ise
 *
 * Created on March 04, 2017, 11:12 AM
 */


#include <sstream>
#include <iomanip>

#include "Animation.h"
#include "Utils.h"



std::string Animation::formatAnimationFilename(long num) {
    std::ostringstream oss;
    oss << std::setfill('0') << std::setw(6) << num;
    return oss.str();
}

std::vector<Mesh *> Animation::LoadAnimationMeshes(const char* pattern, int start, int end, float scale) {
    std::vector<Mesh *> animVec;
    for (int i = start; i <= end; i++) {
        std::string filename(pattern);
        filename.append(formatAnimationFilename(i)).append(".obj");
        printf("Loading anim: %s\n", filename.c_str());
        animVec.push_back(new Mesh(filename.c_str(), scale));
    }
    return animVec;
}

Animation::Animation(std::vector<Mesh *> animVec) {
    for (Mesh *mesh : animVec)
        m_anim.push_back(mesh);

    m_size = m_anim.size();
    m_index = 0;

    m_regularDirection = true;
    m_cycleAnimation = true;
}

Animation::~Animation() {
}

void Animation::SetRegularDirection(bool regular) {
    m_regularDirection = regular;
}

void Animation::SetCycleAnimation(bool cycle) {
    m_cycleAnimation = cycle;
}


bool Animation::GetNextMesh(Mesh **current) {
    *current = m_anim[m_index];

    if (m_cycleAnimation) {
        m_index = (m_index + 1) % m_size;
    } else if (m_regularDirection) {
        if (m_index >= m_size - 1)
            return false;
        m_index++;
    } else {
        if (m_index <= 0)
            return false;
        m_index--;
    }
    return true;
}