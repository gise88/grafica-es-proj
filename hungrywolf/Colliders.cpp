/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Colliders.cpp
 * Author: ise
 *
 * Created on March 17, 2017, 4:24 AM
 */


#include <GL/gl.h>
#include <GL/glu.h>
#include <string.h>

#include "Colliders.h"

#include "CollisionsManager.h"
#include "GLUtils.h"


Colliders::Colliders(BoxCollider *boxCollider)
    : m_containerSphere(0, 0, 0, 0) {
    m_boxCollider = boxCollider;
    m_scale = 1;
    m_settings = Settings::InstancePtr();
}

Colliders::~Colliders() {
}

void Colliders::SetScale(float scale) {
    m_scale = scale;
}

SphereCollider* Colliders::GetSphereColliders(std::vector<SphereCollider> **sphereColliderVec) {
    *sphereColliderVec = &m_sphereVec;
    return &m_containerSphere;
}

void Colliders::Render() {
    glPushMatrix();

    glDisable(GL_TEXTURE_2D);
    glLineWidth(2.0);

    glColor3f(0, 1, 0);

    GLUtils::DrawCircleY(m_containerSphere.x,
                         m_containerSphere.y,
                         m_containerSphere.z,
                         m_containerSphere.radius,
                         20);

    glColor3f(1, 1, 0);
    for (SphereCollider& sc : m_sphereVec) {
        if (m_settings->useOnlyHeadCollider && strcmp(sc.tag, "HEAD") == 0)
            glColor3f(1, 0, 0);
        else
            glColor3f(1, 1, 0);
        GLUtils::DrawCircleY(sc.x, sc.y, sc.z, sc.radius, 10);
    }

    glPopMatrix();
}

CollisionInfo Colliders::RequestCollisionCheck() {
    return CollisionsManager::CheckBetweenColliders(this, m_boxCollider);
}