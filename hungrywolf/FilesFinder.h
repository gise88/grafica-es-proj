/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   FilesFinder.h
 * Author: ise
 *
 * Created on January 31, 2017, 12:19 PM
 */

#ifndef HUNGRYWOLF_FILESFINDER_H
#define HUNGRYWOLF_FILESFINDER_H

#include <cstdlib>
#include <string>
#include <vector>

class FilesFinder {
public:
	FilesFinder(const char *pattern);
	virtual ~FilesFinder();

	virtual std::vector<std::string>& getAll();

private:	
	std::vector<std::string> m_filenames;

};

#endif /* HUNGRYWOLF_FILESFINDER_H */

