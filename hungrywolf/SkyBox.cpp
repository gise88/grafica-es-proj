/*
 * Copyright (C) 2016 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   SkyBox.cpp
 * Author: ise
 * 
 * Created on October 24, 2016, 12:00 PM
 */

#include <sstream>
#include <GL/glew.h>

#include "Settings.h"
#include "SkyBox.h"
#include "TextureLoader.h"



SkyBox::SkyBox(float size) {

    m_size = size;
    m_yoffs = 0.0;

    std::vector<std::string> filenames = {
        "res/img/front.png",
        "res/img/right.png",
        "res/img/back.png",
        "res/img/left.png",
        "res/img/top.png",
        "res/img/bottom.png"
    };

    TextureLoader::loadAll(filenames, m_skybox);

    initCubeAndTexture();
}

SkyBox::~SkyBox() {
    glDeleteTextures(6, &(m_skybox[0]));
}


void SkyBox::draw(float px, float py, float pz) {

    bool isTextureEnabled = glIsEnabled(GL_TEXTURE_2D);
    bool isCullFaceEnabled = glIsEnabled(GL_CULL_FACE);

    glDisable(GL_LIGHTING);
    glDisable(GL_CULL_FACE);
    glEnable(GL_TEXTURE_2D);

    glColor3f(1, 1, 1);

    glPushMatrix();

    if (Settings::Instance().useWireframe) {
        glDisable(GL_TEXTURE_2D);
        glColor3f(.5, .5, .5);
        glLineWidth(100.0);

        for (int side = 0; side < 6; side++) {
            int v = side * 4;
            for (int i = 0; i < 4; i++) {
                int i1 = v + i;
                int i2 = v + (i + 1) % 4;
                glBegin(GL_LINES);
                glVertex3f(m_cubeVertex[i1].x + px, m_cubeVertex[i1].y + py, m_cubeVertex[i1].z + pz);
                glVertex3f(m_cubeVertex[i2].x + px, m_cubeVertex[i2].y + py, m_cubeVertex[i2].z + pz);
                glEnd();
            }
        }

        glColor3f(1, 1, 1);
    } else {
        for (int side = 0; side < 6; side++) {
            glBindTexture(GL_TEXTURE_2D, m_skybox[side]);
            glBegin(GL_QUADS);
            int vstart = side * 4;
            for (int v = vstart; v < vstart + 4; v++) {
                glTexCoord2f(m_textureVertex[v].x, m_textureVertex[v].y);
                glVertex3f(m_cubeVertex[v].x + px, m_cubeVertex[v].y + py, m_cubeVertex[v].z + pz);
            }
            glEnd();
        }
    }

    glPopMatrix();

    if (!isTextureEnabled)
        glDisable(GL_TEXTURE_2D);

    if (isCullFaceEnabled)
        glEnable(GL_CULL_FACE);

    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
}

void SkyBox::initCubeAndTexture() {
    //front face
    m_textureVertex.push_back(Vec2(1, 0));
    m_cubeVertex.push_back(Vec3(m_size / 2, m_size / 2 + m_yoffs, -m_size / 2));
    m_textureVertex.push_back(Vec2(0, 0));
    m_cubeVertex.push_back(Vec3(-m_size / 2, m_size / 2 + m_yoffs, -m_size / 2));
    m_textureVertex.push_back(Vec2(0, 1));
    m_cubeVertex.push_back(Vec3(-m_size / 2, -m_size / 2 + m_yoffs, -m_size / 2));
    m_textureVertex.push_back(Vec2(1, 1));
    m_cubeVertex.push_back(Vec3(m_size / 2, -m_size / 2 + m_yoffs, -m_size / 2));
    //right face
    m_textureVertex.push_back(Vec2(0, 0));
    m_cubeVertex.push_back(Vec3(m_size / 2, m_size / 2 + m_yoffs, -m_size / 2));
    m_textureVertex.push_back(Vec2(1, 0));
    m_cubeVertex.push_back(Vec3(m_size / 2, m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(1, 1));
    m_cubeVertex.push_back(Vec3(m_size / 2, -m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(0, 1));
    m_cubeVertex.push_back(Vec3(m_size / 2, -m_size / 2 + m_yoffs, -m_size / 2));
    //back face
    m_textureVertex.push_back(Vec2(0, 0));
    m_cubeVertex.push_back(Vec3(m_size / 2, m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(1, 0));
    m_cubeVertex.push_back(Vec3(-m_size / 2, m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(1, 1));
    m_cubeVertex.push_back(Vec3(-m_size / 2, -m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(0, 1));
    m_cubeVertex.push_back(Vec3(m_size / 2, -m_size / 2 + m_yoffs, m_size / 2));
    //left face
    m_textureVertex.push_back(Vec2(0, 0));
    m_cubeVertex.push_back(Vec3(-m_size / 2, m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(1, 0));
    m_cubeVertex.push_back(Vec3(-m_size / 2, m_size / 2 + m_yoffs, -m_size / 2));
    m_textureVertex.push_back(Vec2(1, 1));
    m_cubeVertex.push_back(Vec3(-m_size / 2, -m_size / 2 + m_yoffs, -m_size / 2));
    m_textureVertex.push_back(Vec2(0, 1));
    m_cubeVertex.push_back(Vec3(-m_size / 2, -m_size / 2 + m_yoffs, m_size / 2));
    //top face
    m_textureVertex.push_back(Vec2(1, 0));
    m_cubeVertex.push_back(Vec3(m_size / 2, m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(0, 0));
    m_cubeVertex.push_back(Vec3(-m_size / 2, m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(0, 1));
    m_cubeVertex.push_back(Vec3(-m_size / 2, m_size / 2 + m_yoffs, -m_size / 2));
    m_textureVertex.push_back(Vec2(1, 1));
    m_cubeVertex.push_back(Vec3(m_size / 2, m_size / 2 + m_yoffs, -m_size / 2));
    //bottom face
    m_textureVertex.push_back(Vec2(1, 1));
    m_cubeVertex.push_back(Vec3(m_size / 2, -m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(0, 1));
    m_cubeVertex.push_back(Vec3(-m_size / 2, -m_size / 2 + m_yoffs, m_size / 2));
    m_textureVertex.push_back(Vec2(0, 0));
    m_cubeVertex.push_back(Vec3(-m_size / 2, -m_size / 2 + m_yoffs, -m_size / 2));
    m_textureVertex.push_back(Vec2(1, 0));
    m_cubeVertex.push_back(Vec3(m_size / 2, -m_size / 2 + m_yoffs, -m_size / 2));
}