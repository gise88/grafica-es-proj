/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   HungryWolfInputManager.h
 * Author: ise
 *
 * Created on March 23, 2017, 10:58 PM
 */

#ifndef HUNGRYWOLF_HUNGRYWOLFINPUTMANAGER_H
#define HUNGRYWOLF_HUNGRYWOLFINPUTMANAGER_H


#include <functional>
#include <SDL2/SDL.h>


#include "Wolf.h"

class HungryWolfInputManager {
public:
    HungryWolfInputManager(Wolf *wolf);

    virtual ~HungryWolfInputManager();


    void ProcessEvent(SDL_Event *event);

private:

    Wolf *m_wolf;

};


#endif /* HUNGRYWOLF_HUNGRYWOLFINPUTMANAGER_H */
