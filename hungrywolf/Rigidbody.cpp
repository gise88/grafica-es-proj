/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Rigidbody.cpp
 * Author: ise
 *
 * Created on March 15, 2017, 12:34 AM
 */



#include "Rigidbody.h"



Rigidbody::Rigidbody() {
    m_px = 0;
    m_py = 0;
    m_pz = 0;
    m_facing = 0;
}

Rigidbody::~Rigidbody() {
}

void Rigidbody::SetPositionAndFacing(float x, float y, float z, float facing) {
    m_px = x;
    m_py = y;
    m_pz = z;
    m_facing = facing;
}

float Rigidbody::GetX() {
    return m_px;
}

float Rigidbody::GetY() {
    return m_py;
}

float Rigidbody::GetZ() {
    return m_pz;
}

float Rigidbody::GetFacing() {
    return m_facing;
}


