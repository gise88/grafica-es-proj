/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   SphereCollider.cpp
 * Author: ise
 *
 * Created on March 15, 2017, 2:39 AM
 */



#include <string.h>


#include "SphereCollider.h"


SphereCollider::SphereCollider(float _x, float _y, float _z, float _radius)
    : x(_x), y(_y), z(_z), radius(_radius) {
    strncpy(tag, SPHERE_COLLIDER_CONTAINER_TAG, MAX_TAG_LENGTH);
}

SphereCollider::SphereCollider(float _x, float _y, float _z, float _radius, const char *_tag)
    : x(_x), y(_y), z(_z), radius(_radius) {
    strncpy(tag, _tag, MAX_TAG_LENGTH);
}

SphereCollider::SphereCollider(SphereCollider *sphereCollider)
    : x(sphereCollider->x), y(sphereCollider->y), z(sphereCollider->z), radius(sphereCollider->radius) {
strncpy(tag, sphereCollider->tag, MAX_TAG_LENGTH);
}

void SphereCollider::Set(float _x, float _y, float _z, float _radius) {
    x = _x;
    y = _y;
    z = _z;
    radius = _radius;
}

void SphereCollider::Set(float _x, float _y, float _z, float _radius, const char *_tag) {
    Set(_x, _y, _z, _radius);
    strncpy(tag, _tag, MAX_TAG_LENGTH);
}