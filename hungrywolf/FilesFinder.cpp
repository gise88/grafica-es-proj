/*
 * Copyright (C) 2016 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   FilesFinder.cpp
 * Author: ise
 * 
 * Created on January 31, 2017, 12:19 PM
 */

#include <glob.h>

#include "FilesFinder.h"



FilesFinder::FilesFinder(const char *pattern) {
    if (pattern == nullptr)
        printf("FilesFinder::FilesFinder -> pattern is NULL\n");

    else {
        glob_t glob_result;
        glob(pattern, GLOB_TILDE, NULL, &glob_result);
        size_t count = glob_result.gl_pathc;
        printf("Found %lu file%scount with the following pattern: '%s'.\n", count, count == 1 ? " " : "s ", pattern);
        for (uint32_t i = 0; i < glob_result.gl_pathc; i++)
            m_filenames.push_back(std::string(glob_result.gl_pathv[i]));
        globfree(&glob_result);
    }
}

FilesFinder::~FilesFinder() {
    m_filenames.clear();
}

std::vector<std::string> &FilesFinder::getAll() {
    return m_filenames;
}
