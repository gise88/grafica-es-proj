/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   WolfColliders.h
 * Author: ise
 *
 * Created on March 15, 2017, 2:44 AM
 */

#ifndef HUNGRYWOLF_WOLFCOLLIDERS_H
#define HUNGRYWOLF_WOLFCOLLIDERS_H


#include <vector>
#include <tuple>
#include <string>

#include "Colliders.h"

#include "SphereCollider.h"
#include "BoxCollider.h"


class WolfColliders : public Colliders {
public:

    WolfColliders(BoxCollider *boxCollider);

    virtual ~WolfColliders();

    virtual void Update(float px, float py, float pz, float facing, Point3 *bbmin, Point3 *bbmax) override;

private:

    std::vector< std::tuple<float, std::string> > m_sphereDataVec;

};


#endif /* HUNGRYWOLF_WOLFCOLLIDERS_H */
