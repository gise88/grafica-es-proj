/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Ranch.h
 * Author: ise
 *
 * Created on March 08, 2017, 9:34 AM
 */

#ifndef HUNGRYWOLF_RANCH_H
#define HUNGRYWOLF_RANCH_H


#include <GL/gl.h>

#include <cstdlib>
#include <string>
#include <vector>

#include "BoxCollider.h"
#include "IRenderable.h"

#include "mesh.h"



class Ranch : public BoxCollider , public IRenderable {
public:
    Ranch(int width, int height);

    virtual ~Ranch();

    void Render() override;

    void GetBoundingBox(Point3 *bbmin, Point3 *bbmax) override;

private:

    class Fence {
    public:
        Fence(GLuint texId, Mesh *fence, float px, float py, float pz, float rotY);

        virtual ~Fence();

        void Render();

    private:
        GLuint m_texId;
        Mesh *m_fence;

        float m_px;
        float m_py;
        float m_pz;

        float m_rotY;
    };

    GLuint m_texId;
    Mesh *m_segmentFence;
    Mesh *m_endFenceLog;

    float m_xMin;
    float m_xMax;
    float m_zMin;
    float m_zMax;

    std::vector<Fence *> m_fenceVec;
};


#endif /* HUNGRYWOLF_RANCH_H */
