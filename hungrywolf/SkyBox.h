/*
 * Copyright (C) 2016 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   SkyBox.h
 * Author: ise
 *
 * Created on October 24, 2016, 12:00 PM
 */

#ifndef HUNGRYWOLF_SKYBOX_H
#define HUNGRYWOLF_SKYBOX_H


#include <cstdlib>
#include <string>
#include <vector>

#include <GL/gl.h>
#include <GL/glu.h>

#include "Clouds.h" 


struct Vec2 {
	GLfloat x;
	GLfloat y;
	
	Vec2(GLfloat x, GLfloat y)
    : x(x), y(y) {}
};

struct Vec3 {
	GLfloat x;
	GLfloat y;
	GLfloat z;
	
	Vec3(GLfloat x, GLfloat y, GLfloat z)
    : x(x), y(y), z(z) {}
};


class SkyBox {
public:
	SkyBox(float size);
	virtual ~SkyBox();
	
	void draw(float px, float py, float pz);

private:
	
	float m_size;	
	float m_yoffs;
	
	GLuint m_skybox[6];
	std::vector<Vec3> m_cubeVertex;
	std::vector<Vec2> m_textureVertex;
	
	void initCubeAndTexture();
};

#endif /* HUNGRYWOLF_SKYBOX_H */

