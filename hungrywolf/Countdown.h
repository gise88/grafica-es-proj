/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Countdown.h
 * Author: ise
 *
 * Created on March 22, 2017, 4:36 PM
 */

#ifndef HUNGRYWOLF_COUNTDOWN_H
#define HUNGRYWOLF_COUNTDOWN_H


#include <chrono>
#include <string>
#include <vector>
#include <functional>



class Countdown {
public:
    Countdown();

    virtual ~Countdown();

    void AddCountdownEvent(int remainingSec, std::function<void(int)> eventHandler);

    void ResetCountdownEvent();

    void Pause();

    void Continue();

    void Set(int offsetInSeconds);

    std::string GetCountdownTime();

private:

    class CountdownEvent {
    public:

        CountdownEvent(int remainingSec, std::function<void(int)> eventHandler) {
            m_remainingSec = remainingSec;
            m_eventHandler = eventHandler;
            m_enabled = true;
        }

        virtual ~CountdownEvent() {}

        int m_remainingSec;

        bool m_enabled;

        std::function<void(float)> m_eventHandler;
    };

    std::vector<CountdownEvent> m_eventsVec;

    std::chrono::time_point<std::chrono::system_clock> m_start;

    std::chrono::time_point<std::chrono::system_clock> m_pause;
};


#endif /* HUNGRYWOLF_COUNTDOWN_H */
