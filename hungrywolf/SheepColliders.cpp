/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   SheepColliders.cpp
 * Author: ise
 *
 * Created on March 17, 2017, 2:59 AM
 */


#include <math.h>

#include "SheepColliders.h"



SheepColliders::SheepColliders(BoxCollider *boxCollider)
    : Colliders(boxCollider) {
}

SheepColliders::~SheepColliders() {
}

void SheepColliders::Update(float px, float py, float pz, float facing, Point3 *bbmin, Point3 *bbmax) {
    float cosf = cos((facing - 90.0f) * M_PI / 180.0);
    float sinf = sin((facing - 90.0f) * M_PI / 180.0);

    float ymax = bbmax->Y();
    float zmin = bbmin->Z();
    float zmax = bbmax->Z();

    m_sphereVec.clear();

    std::vector<float> percentages = {0.15, 0.5, 0.85};
    for (float p : percentages) {
        float offset = zmin + (zmax - zmin) * p;
        float circleCX = - cosf * offset + px;
        float circleCY = ymax * 0.65 + py;
        float circleCZ = sinf * offset + pz;

        m_sphereVec.push_back(SphereCollider(circleCX, circleCY, circleCZ, 0.525f * m_scale, "TAG"));
    }

    float offset = zmin + (zmax - zmin) * 0.5;
    float circleCX = - cosf * offset + px;
    float circleCY = ymax * 0.65 + py;
    float circleCZ = sinf * offset + pz;

    m_containerSphere.Set(circleCX, circleCY, circleCZ, 1.75f * m_scale);
}