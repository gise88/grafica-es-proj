/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Environment.h
 * Author: ise
 *
 * Created on February 09, 2017, 8:28 PM
 */

#ifndef HUNGRYWOLF_ENVIRONMENT_H
#define HUNGRYWOLF_ENVIRONMENT_H


#include "mesh.h"

#include "Ranch.h"
#include "Settings.h"
#include "CloudySky.h"
#include "Billboard.h"



class Environment {
public:
    Environment(float size, int ranchWidth, int ranchHeight);

    virtual ~Environment();

    BoxCollider* GetBoxCollider();

    void Render(float px, float py, float pz);

private:

    void DrawFloor(float px, float py, float pz);

    float m_size;

    Ranch *m_ranch;

    Settings *m_settings;

    Billboard *m_billboard;

    CloudySky m_cloudySky;

    GLuint m_grassTexId;
};


#endif /* HUNGRYWOLF_ENVIRONMENT_H */
