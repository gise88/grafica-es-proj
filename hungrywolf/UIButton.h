/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   UIButton.h
 * Author: ise
 *
 * Created on March 23, 2017, 6:12 PM
 */

#ifndef HUNGRYWOLF_UIBUTTON_H
#define HUNGRYWOLF_UIBUTTON_H

#define UIBUTTON_LABEL_MAX_LENGTH 50


#include <functional>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>

#include <SDL2/SDL.h>



class UIButton {
public:
    UIButton();

    virtual ~UIButton();


    void SetLabel(const char *label);

    const char* GetLabel();

    void SetFontSize(float fontSize);

    void SetBounds(float x, float y, float w, float h);

    void GetBounds(float *x, float *y, float *w, float *h);

    void SetTransparencyAlpha(float alpha);

    void SetHighlighted(bool highlighted);

    void SetPressed(bool pressed);

    void SetOnClickListener(std::function<void(UIButton *)> buttonCallback);

    void EmitClickEvent();

    virtual void Draw();

private:

    char m_label[UIBUTTON_LABEL_MAX_LENGTH];
    float m_fontSize;

    float m_alpha;

    float m_px;
    float m_py;

    float m_width;
    float m_height;

    bool m_pressed;
    bool m_highlighted;

    std::function<void(UIButton *)> m_buttonCallback;
};


#endif /* HUNGRYWOLF_UIBUTTON_H */
