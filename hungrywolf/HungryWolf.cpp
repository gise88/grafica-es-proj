/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   HungryWolf.cpp
 * Author: ise
 *
 * Created on March 18, 2017, 2:09 AM
 */

#include <math.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>

#include "HungryWolf.h"

#include "Utils.h"
#include "GLUtils.h"
#include "TextureLoader.h"



#define POINTS_PER_SHEEP 10
#define TIMEOUT_PER_COMBO 5
#define MULTIPLIER_PER_COMBO 2
#define GAME_COUNTDOWN_TIME 180
#define GAME_MAX_LEVEL 5
#define GAME_BONUS_LEVEL 50
#define POINTS_TO_GO_TO_BONUS_LEVEL 450
#define TIME_PRE_WIN_OR_PRE_LOSE 10
#define MAX_SCORE_LIST_LENGHT 10


HungryWolf::HungryWolf(SDL_Window *window)
    : Game(window), m_scoresManager(MAX_SCORE_LIST_LENGHT) {
    m_introTexId = TextureLoader::loadAsMipmap("res/img/intro.jpg");

    m_winTexId = TextureLoader::load("res/img/win-lose banner/win.png");
    m_loseTexId = TextureLoader::load("res/img/win-lose banner/lose.png");

    m_wolf = nullptr;
    m_wolfColliders = nullptr;
    m_environment = nullptr;
    m_collisionsManager = nullptr;
    m_combo = nullptr;
    m_hungryWolfInputManager = nullptr;
    m_minimap = nullptr;
    m_countdown = nullptr;

    m_gameState = GameState::INTRO;
    CreateUI();

//    InitGame();
}

HungryWolf::~HungryWolf() {
    if (m_gameState != GameState::INTRO) {
        delete m_wolf;
        delete m_wolfColliders;
        delete m_environment;
        delete m_collisionsManager;
        delete m_combo;
        delete m_hungryWolfInputManager;
        delete m_minimap;
        delete m_countdown;
        delete m_exitGameCountdown;
        delete m_listViewInfo;

        Sheep::UnloadMeshAndTexture();
    }

    for (Sheep *s : m_allSheepList) {
        SheepColliders *c = (SheepColliders *)s->GetColliders();
        delete c;
        delete s;
    }

    glDeleteTextures(1, &m_introTexId);
    glDeleteTextures(1, &m_winTexId);
    glDeleteTextures(1, &m_loseTexId);
}

void HungryWolf::QuitHandler() {

    switch (m_gameState) {
        case GameState::PLAY:
            SetGameState(GameState::PAUSE);
            break;
        case GameState::PRE_LOSE:
            SetGameState(GameState::LOSE);
            break;
        case GameState::PRE_WIN:
            SetGameState(GameState::WIN);
            break;
        case GameState::INTRO:
        case GameState::PAUSE:
        case GameState::LOSE:
        case GameState::WIN:
            Game::QuitHandler();
    }
}

void HungryWolf::InitGame() {
    Point3 bbmin, bbmax;
    BoxCollider *boxCollider;

    m_environment = new Environment(m_settings->worldSize, m_settings->ranchWidth, m_settings->ranchHeight);
    boxCollider = m_environment->GetBoxCollider();
    boxCollider->GetBoundingBox(&bbmin, &bbmax);
    m_minimap = new Minimap(bbmin, bbmax);
    m_wolfColliders = new WolfColliders(boxCollider);
    m_wolf = new Wolf(m_wolfColliders);
    m_collisionsManager = new CollisionsManager();
    m_hungryWolfInputManager = new HungryWolfInputManager(m_wolf);
    m_combo = new Combo();

    Sheep::LoadMeshAndTexture();

    for (int i = 0; i < GAME_BONUS_LEVEL; i++) {
        SheepColliders *sheepColliders = new SheepColliders(m_environment->GetBoxCollider());
        Sheep *sheep = new Sheep(sheepColliders);
        m_allSheepList.push_back(sheep);
    }

    m_inputManager->SetUnhandledEventHandler(
        std::bind(&HungryWolfInputManager::ProcessEvent, m_hungryWolfInputManager, std::placeholders::_1));

    m_countdown = new Countdown();
    m_countdown->AddCountdownEvent(15, std::bind(&HungryWolf::CountdownEventHandler, this, std::placeholders::_1));
    m_countdown->AddCountdownEvent(5, std::bind(&HungryWolf::CountdownEventHandler, this, std::placeholders::_1));
    m_countdown->AddCountdownEvent(0, std::bind(&HungryWolf::CountdownEventHandler, this, std::placeholders::_1));

    m_exitGameCountdown = new Countdown();

    std::list<std::string> infoList = {
        "W : move forward",
        "S : seat",
        "W + A/D : move left and right",
        "",
        "Q : camera rotate right",
        "E : camera rotate left",
        "R : camera zoom in",
        "F : camera zoom out",
        "T : camera move up",
        "G : camera move down",
        "",
        "F1 : change camera type",
        "F2 : use wire-frame",
        "F3 : view/hide bounding boxes",
        "F4 : switch on/off shadow",
        "F5 : look at the billboard and press F5! ;)",
        "F6 : enable/disable fog",
        "F7 : use only the wolf's head as collider to eat sheeps",
        "F8 : print some debug info",
        "",
        "I : open/close this menu",
        "ESC : exit game"
    };

    m_listViewInfo = new UIListView();
    m_listViewInfo->SetLabel("Keywords Information");
    m_listViewInfo->SetPosition(m_settings->screenWidth / 2, 100);
    m_listViewInfo->SetWidth(fmax(600, m_settings->screenWidth * 0.65));
    m_listViewInfo->SetListItems(infoList);
    m_listViewInfo->SetTransparencyAlpha(0.6);
    m_listViewInfo->SetLabelFontSize(0.30);
    m_listViewInfo->SetFontSize(0.18);

    SetGameState(GameState::PLAY);
}

void HungryWolf::CountdownEventHandler(int remainingTime) {
    printf("CountdownEventHandler: %d\n", remainingTime);
    m_remainingTime = remainingTime;
    if (remainingTime == 0 && m_gameState == GameState::PLAY)
        SetGameState(GameState::PRE_LOSE);
    else if (remainingTime == -TIME_PRE_WIN_OR_PRE_LOSE) {
        if (m_gameState == GameState::PRE_LOSE)
            SetGameState(GameState::LOSE);
        else if (m_gameState == GameState::PRE_WIN)
            SetGameState(GameState::WIN);
    }
}

void HungryWolf::SetGameState(GameState newGameState) {

    GameState currGameState = m_gameState;
    m_gameState = newGameState;

    if (currGameState == GameState::INTRO && newGameState == GameState::PLAY) {
        InitGame();
        StartNewGame();
        EnterGame();
    } else if (currGameState == GameState::PLAY && newGameState == GameState::PAUSE) {
        m_countdown->Pause();
        ExitGame();
    } else if (currGameState == GameState::PAUSE && newGameState == GameState::PLAY) {
        m_countdown->Continue();
        EnterGame();
    } else if (currGameState == GameState::PLAY &&
               (newGameState == GameState::PRE_WIN || newGameState == GameState::PRE_LOSE)) {
        m_scoresManager.AddScore(m_score);
        m_exitGameCountdown->Set(0);
        m_exitGameCountdown->AddCountdownEvent(
            -TIME_PRE_WIN_OR_PRE_LOSE,
            std::bind(&HungryWolf::CountdownEventHandler, this, std::placeholders::_1));
    } else if ((currGameState == GameState::PRE_LOSE || currGameState == GameState::PRE_WIN) &&
               (newGameState == GameState::WIN || newGameState == GameState::LOSE)) {
        ExitGame();
    } else if ((currGameState == GameState::WIN || currGameState == GameState::LOSE) &&
               newGameState == GameState::PLAY) {
        StartNewGame();
        EnterGame();
    }
}

void HungryWolf::EnterGame() {
    SDL_SetRelativeMouseMode(SDL_TRUE);
    m_resetCameraOnFirstRender = true;
    m_ui->ClearAll();
}

void HungryWolf::ExitGame() {
    SDL_SetRelativeMouseMode(SDL_FALSE);
    CreateUI();
}

void HungryWolf::StartNewGame() {
    m_score = 0;
    m_comboCount = 0;
    m_level = 0;
    m_bonusLevelEnabled = false;
    m_remainingTime = GAME_COUNTDOWN_TIME;

    StartNextLevel();

    m_wolf->Reset();

    m_countdown->Set(GAME_COUNTDOWN_TIME);
    m_countdown->ResetCountdownEvent();
}

void HungryWolf::SpawnNSheep(int count) {
    Point3 bbmin, bbmax;
    m_environment->GetBoxCollider()->GetBoundingBox(&bbmin, &bbmax);

    for (Sheep *sheep : m_allSheepList)
        m_spawnedSheepList.remove(sheep);

    int i = 0;
    for (Sheep *sheep : m_allSheepList) {
        if (i >= count)
            break;

        float x = Utils::randFloat(bbmin.X(), bbmax.X());
        float z = Utils::randFloat(bbmin.Z(), bbmax.Z());

        float facing = Utils::randFloat(0, 360);
        sheep->SetPositionAndFacing(x, 0, z, facing);
        m_spawnedSheepList.push_back(sheep);
        i++;
    }
}

void HungryWolf::StartNextLevel() {
    if (m_level == GAME_MAX_LEVEL && m_bonusLevelEnabled)
        m_level = GAME_BONUS_LEVEL;
    else
        m_level++;

    m_currentLevelSheepCount = m_level;
    SpawnNSheep(m_level);
    m_comboTimer.Reset();
}

void HungryWolf::DoStep() {
    if (m_gameState == GameState::PLAY) {
        m_wolf->DoStep();

        Sheep *eatenSheep = m_collisionsManager->WolfCollisions(m_wolf, m_spawnedSheepList);
        if (eatenSheep != nullptr) {
            if (m_spawnedSheepList.size() != (size_t)m_currentLevelSheepCount &&
                m_comboTimer.GetElapsedTimeInSeconds() <= TIMEOUT_PER_COMBO) {
                m_comboCount++;
                m_combo->IncrementComboValue();
            } else {
                m_comboCount = 0;
                m_combo->Reset();
            }

            m_spawnedSheepList.remove(eatenSheep);

            m_score += POINTS_PER_SHEEP + POINTS_PER_SHEEP * MULTIPLIER_PER_COMBO * m_comboCount;
            m_comboTimer.Reset();

            if (m_spawnedSheepList.size() == 0) {
                if (m_score >= POINTS_TO_GO_TO_BONUS_LEVEL)
                    m_bonusLevelEnabled = true;

                if ((!m_bonusLevelEnabled && m_level >= GAME_MAX_LEVEL) || m_level >= GAME_BONUS_LEVEL) {
                    SetGameState(GameState::PRE_WIN);
                    // TODO: WIIIIIIIN
                } else
                    StartNextLevel();
            }
        }

        for (Sheep *s : m_spawnedSheepList)
            s->DoStep();
    }
}

void HungryWolf::SetCoordToPixel() {
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(-1, -1, 0);
    glScalef(2.0f / m_settings->screenWidth, 2.0f / m_settings->screenHeight, 1);
}


void HungryWolf::CreateUI() {

    float width2 = m_settings->screenWidth / 2;
    float height = m_settings->screenHeight - 80;
    float offsX = 270;
    float bWidth = 250;
    float bHeight = 100;
    float alpha = 0.8f;

    UIButton *bContinue = nullptr;

    UIButton *bExit = m_ui->CreateButton();
    bExit->SetLabel("Exit");
    bExit->SetBounds(width2 - offsX - bWidth / 2, height - bHeight / 2, bWidth, bHeight);
    bExit->SetTransparencyAlpha(alpha);
    bExit->SetOnClickListener(
        [=](UIButton *sender) -> void {
            printf("Bye bye!\n");
            QuitHandler();
        }
    );

    UIButton *bStart = m_ui->CreateButton();
    bStart->SetBounds(width2 + offsX - bWidth / 2, height - bHeight / 2, bWidth, bHeight);
    bStart->SetTransparencyAlpha(alpha);

    if (m_scoresManager.GetScores().size() > 0) {
        UIListView *lvScores = m_ui->CreateListView();
        lvScores->SetLabel("Latest Scores:");
        lvScores->SetPosition(width2, m_settings->screenHeight / 2 - 50);
        lvScores->SetWidth(230);
        lvScores->SetListItems(m_scoresManager.GetScores());
        lvScores->SetTransparencyAlpha(alpha);
        lvScores->SetLabelFontSize(0.20);
    }

    switch (m_gameState) {
        case GameState::INTRO:
            bStart->SetLabel("Start Game!");
            bStart->SetOnClickListener(
                [=](UIButton *sender) -> void {
                    printf("Start Game!\n");
                    SetGameState(GameState::PLAY);
                }
            );
            break;
        case GameState::PLAY:
        case GameState::PRE_LOSE:
        case GameState::PRE_WIN:
            break;
        case GameState::PAUSE:
            bStart->SetLabel("Start New Game!");
            bStart->SetOnClickListener(
                [=](UIButton *sender) -> void {
                    printf("Start New Game!\n");
                    SetGameState(GameState::LOSE);
                    SetGameState(GameState::PLAY);
                }
            );

            bContinue = m_ui->CreateButton();
            bContinue->SetBounds(width2 - bWidth / 2, height - bHeight / 2, bWidth, bHeight);
            bContinue->SetTransparencyAlpha(alpha);
            bContinue->SetLabel("Continue?");
            bContinue->SetOnClickListener(
                [=](UIButton *sender) -> void {
                    printf("Continue Game!\n");
                    SetGameState(GameState::PLAY);
                }
            );
            break;
        case GameState::LOSE:
        case GameState::WIN:
            bStart->SetLabel("Start New Game!");
            bStart->SetOnClickListener(
                [=](UIButton *sender) -> void {
                    printf("Start New Game!\n");
                    SetGameState(GameState::PLAY);
                }
            );
            break;
    }
    SDL_SetRelativeMouseMode(SDL_FALSE);
}

void HungryWolf::RenderMain() {
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, m_settings->screenWidth, m_settings->screenHeight, 0, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glPushMatrix();
    glBindTexture(GL_TEXTURE_2D, m_introTexId);
    glEnable(GL_TEXTURE_2D);
    glBegin(GL_QUADS);
    glColor3f(1, 1, 1);

    glTexCoord2i(0, 0);
    glVertex2d(0, 0);
    glTexCoord2i(0, 1);
    glVertex2d(0, m_settings->screenHeight);
    glTexCoord2i(1, 1);
    glVertex2d(m_settings->screenWidth, m_settings->screenHeight);
    glTexCoord2i(1, 0);
    glVertex2d(m_settings->screenWidth, 0);

    glEnd();
    glDisable(GL_TEXTURE_2D);
    glPopMatrix();

    GLUtils::DrawTextShadow(
        GLUT_STROKE_MONO_ROMAN,
        "Gianluca Iselli: gianluca.iselli@gmail.com",
        10, m_settings->screenHeight - 10,
        2, 1, 0.10, false);

    m_ui->Draw();
}

void HungryWolf::Render2D() {

    glDisable(GL_DEPTH_TEST);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D);


    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, m_settings->screenWidth, m_settings->screenHeight, 0, -1, 1);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    char c[100];
    float x = 10;

    GLUtils::DrawTextWithBG(GLUT_STROKE_MONO_ROMAN,
                            "Gianluca Iselli: gianluca.iselli@gmail.com",
                            x, m_settings->screenHeight - 10,
                            2, 0.10, false);

    glColor3f(1, 1, 1);
    snprintf(c, 50, "fps: %0.2f", m_fps);
    GLUtils::DrawText(GLUT_STROKE_ROMAN, c, x, m_settings->screenHeight - 30, 3, 0.15, false);

    if (m_settings->debugInfo) {
        float y = 100;
        snprintf(c, 50, "Wolf pos: %0.2f %0.2f", m_wolf->GetX(), m_wolf->GetZ());
        GLUtils::DrawText(GLUT_STROKE_ROMAN, c, x, y += 30, 2, 0.15, false);
        y += 25;
        for (Sheep *sheep : m_spawnedSheepList) {
            snprintf(c, 50, "Sheep pos: %0.2f %0.2f", sheep->GetX(), sheep->GetZ());
            GLUtils::DrawText(GLUT_STROKE_ROMAN, c, x, y, 2, 0.15, false);
            y += 25;
        }
    }

    if (m_remainingTime == 15)
        glColor3f(1, 1, 0);
    else if (m_remainingTime == 5 || m_remainingTime == 0)
        glColor3f(1, 0, 0);
    else
        glColor3f(1, 1, 1);

    snprintf(c, 50, "%s", m_countdown->GetCountdownTime().c_str());
    GLUtils::DrawText(GLUT_STROKE_ROMAN, c, m_settings->screenWidth / 2, 50, 4, 0.4, true);

    glColor3f(1, 0.522, 0.106);
    if (m_level == GAME_BONUS_LEVEL)
        snprintf(c, 50, "BONUS LEVEEEEL %d!!", GAME_BONUS_LEVEL);
    else
        snprintf(c, 50, "Level: %d/%d", m_level, GAME_MAX_LEVEL);
    GLUtils::DrawText(GLUT_STROKE_ROMAN, c, 20, 50, 5, 0.35, false);

    glColor3f(1, 0, 0);
    snprintf(c, 50, "Score: %d", m_score);
    GLUtils::DrawText(GLUT_STROKE_ROMAN, c, 20, 100, 5, 0.35, false);

    if (m_settings->showInfoWindow) {
        m_listViewInfo->SetPosition(m_settings->screenWidth / 2, 100);
        m_listViewInfo->SetWidth(fmax(600, m_settings->screenWidth * 0.65));
        m_listViewInfo->Draw();
    }

    SetCoordToPixel();

    std::list<Rigidbody *> rigidbodyList;
    for (Sheep *sheep : m_spawnedSheepList)
        rigidbodyList.push_back(sheep);
    m_minimap->Render(m_wolf, rigidbodyList);

    m_combo->Render();

    if (m_gameState == GameState::PRE_WIN || m_gameState == GameState::PRE_LOSE) {

        m_exitGameCountdown->GetCountdownTime();

        float screenWidth2 = m_settings->screenWidth / 2.0f;
        float comboCenterY = m_settings->screenHeight - 170;
        float m_winLoseWidth = 350;
        float m_winLoseHeight = 150;
        GLuint winLoseTexId = m_gameState == GameState::PRE_WIN ? m_winTexId : m_loseTexId;

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glColor4f(1, 1, 1, 0.8f);

        glPushMatrix();
        glBindTexture(GL_TEXTURE_2D, winLoseTexId);
        glEnable(GL_TEXTURE_2D);
        glBegin(GL_QUADS);
        glTexCoord2i(0, 0);
        glVertex2d(screenWidth2 - m_winLoseWidth, comboCenterY + m_winLoseHeight);
        glTexCoord2i(0, 1);
        glVertex2d(screenWidth2 - m_winLoseWidth, comboCenterY - m_winLoseHeight);
        glTexCoord2i(1, 1);
        glVertex2d(screenWidth2 + m_winLoseWidth, comboCenterY - m_winLoseHeight);
        glTexCoord2i(1, 0);
        glVertex2d(screenWidth2 + m_winLoseWidth, comboCenterY + m_winLoseHeight);
        glEnd();
        glDisable(GL_TEXTURE_2D);
        glPopMatrix();

        glDisable(GL_BLEND);
    }

    glPopMatrix();
}

void HungryWolf::Render() {
    glLineWidth(3); // linee larghe

    // settiamo il viewport
    glViewport(0, 0, m_settings->screenWidth, m_settings->screenHeight);

    // colore sfondo = bianco

    if (m_settings->useShadow && !m_settings->useWireframe)
        glClearColor(0.0f, 0.0f, 0.0f, 0.1f);
    else
        glClearColor(0.72902, 0.95, 0.98, 0.1);


    // settiamo la matrice di proiezione
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(70, //fovy,
                   ((float)m_settings->screenWidth) / m_settings->screenHeight, //aspect Y/X,
                   0.2, //distanza del NEAR CLIPPING PLANE in coordinate vista
                   1000 //distanza del FAR CLIPPING PLANE in coordinate vista
    );

    //gluPerspective(45,((float) scrW) / scrH, 1.0, 500.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // riempe tutto lo screen buffer di pixel color sfondo
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // setto la posizione luce
    static float tmpv[4] = {0, 1, 2, 0}; // ultima comp=0 => luce direzionale
    static float tmpcol[4] = {1, 1, 1, 1};

    switch (m_gameState) {
        case GameState::PLAY:
        case GameState::PRE_WIN:
        case GameState::PRE_LOSE:

            if (m_resetCameraOnFirstRender) {
                m_camera->ResetCamera();
                m_resetCameraOnFirstRender = false;
            }

            glLightfv(GL_LIGHT0, GL_POSITION, tmpv);

            m_camera->Update(m_wolf->GetX(), m_wolf->GetY(), m_wolf->GetZ(), m_wolf->GetFacing());

            glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, tmpcol);
            glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, 127);

            glEnable(GL_LIGHTING);

            m_environment->Render(m_wolf->GetX(), m_wolf->GetY(), m_wolf->GetZ());

            m_wolf->Render();
            for (Sheep *s : m_spawnedSheepList)
                s->Render();

            Render2D();

            break;
        case GameState::INTRO:
        case GameState::PAUSE:
        case GameState::LOSE:
        case GameState::WIN:
            RenderMain();
            break;
    }

    glFinish();
    // ho finito: buffer di lavoro diventa visibile
    SDL_GL_SwapWindow(m_window);
}