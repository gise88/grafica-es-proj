/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Sheep.cpp
 * Author: ise
 *
 * Created on March 02, 2017, 9:44 PM
 */



#include "Sheep.h"
#include "TextureLoader.h"
#include "Settings.h"
#include "AnimationsManager.h"



#define SHEEP_WALK_ANIMATION "sheep_walk_animation"

GLuint Sheep::m_texId = 0;
Mesh *Sheep::m_idle = nullptr;


void Sheep::LoadMeshAndTexture() {

    float scale = Settings::Instance().sheepScale;

    Sheep::m_idle = new Mesh((char *)"res/obj/Sheep/sheep.obj", scale);
    AnimationsManager::Instance().LoadAnimation(SHEEP_WALK_ANIMATION, "res/obj/Sheep/anim/Sheep_walk_", 0, 39, scale);

    Sheep::m_texId = TextureLoader::loadAsMipmap("res/obj/Sheep/sheep_UVs.jpg");
}

void Sheep::UnloadMeshAndTexture() {
    delete Sheep::m_idle;
    glDeleteTextures(1, &Sheep::m_texId);
}


Sheep::Sheep(Colliders *colliders) {
    m_colliders = colliders;
    m_colliders->SetScale(Settings::Instance().sheepScale);

    m_walk = AnimationsManager::Instance().NewAnimationFromTag(SHEEP_WALK_ANIMATION);

    m_currMode = Mode::WALK;
    m_currDirection = Direction::AHEAD;

    m_vx = m_vy = m_vz = 0; // velocita' attuale

    velSterzo = 4.0; // A
    velRitornoSterzo = 0.93; // B, sterzo massimo = A*B / (1-B)
    sterzo = 0;

    accMax = 0.0006;

    // attriti: percentuale di velocita' che viene mantenuta
    // 1 = no attrito
    // <<1 = attrito grande
    frictionZWalk = 0.98; // piccolo attrito sulla Z (nel senso di rotolamento delle ruote)
    frictionZTurn = 0.97; // piccolo attrito sulla Z (nel senso di rotolamento delle ruote)
    frictionZIdle = 0.92; // piccolo attrito sulla Z (nel senso di rotolamento delle ruote)
    frictionX = 0.8; // grande attrito sulla X (per non fare slittare la macchina)
    frictionY = 1.0; // attrito sulla y nullo

    // Nota: vel max = accMax*attritoZ / (1-attritoZ)

    grip = 0.45; // quanto il facing macchina si adegua velocemente allo sterzo

    m_firstDoStepAfterRender = true;
}

Sheep::~Sheep() {
    delete m_walk;
}

void Sheep::SetMode(Mode mode) {
    m_currMode = mode;
}

void Sheep::SetDirection(Direction direction) {
    m_currDirection = direction;
}

Colliders* Sheep::GetColliders() {
    return m_colliders;
}

void Sheep::SetupNextMesh() {

    switch (m_currMode) {
        case Mode::IDLE:
            m_currentMesh = Sheep::m_idle;
            break;

        case Mode::WALK:
            m_walk->GetNextMesh(&m_currentMesh);
            break;
    }
}

void Sheep::DoStep() {

    if (m_firstDoStepAfterRender) {
        SetupNextMesh();
        m_firstDoStepAfterRender = false;
    }

    // computiamo l'evolversi della macchina
    float vxm, vym, vzm; // velocita' in spazio macchina

    // da vel frame mondo a vel frame macchina
    float cosf = cos(m_facing * M_PI / 180.0);
    float sinf = sin(m_facing * M_PI / 180.0);
    vxm = +cosf * m_vx - sinf * m_vz;
    vym = m_vy;
    vzm = +sinf * m_vx + cosf * m_vz;

    // gestione dello sterzo
    if (m_currDirection == Direction::LEFT)
        sterzo += velSterzo;
    if (m_currDirection == Direction::RIGHT)
        sterzo -= velSterzo;
    sterzo *= velRitornoSterzo; // ritorno a volante dritto

    if (isnan(sterzo))
        printf("merda isnan(sterzo) 1\n");

    if (m_currMode == Mode::WALK)
        vzm -= accMax; // accelerazione in avanti

    if (isnan(vzm))
        printf("merda isnan(vzm) 1\n");

    // attirti (semplificando)
    vxm *= frictionX;
    vym *= frictionY;

    if (isnan(vxm))
        printf("merda isnan(vxm) 1\n");

    if (m_currMode == Mode::WALK) {
        if (m_currDirection == Direction::AHEAD)
            vzm *= frictionZWalk;
        else
            vzm *= frictionZTurn;
    }
    if (m_currMode != Mode::WALK)
        vzm *= frictionZIdle;

    if (isnan(vzm))
        printf("merda isnan(vzm) 2\n");

    // l'orientamento della macchina segue quello dello sterzo
    // (a seconda della velocita' sulla z)
    m_facing = fmod(m_facing - (vzm * grip) * sterzo, 360.0f);
    if (m_facing < 0)
        m_facing += 360.0f;

    if (isnan(m_vx) || isnan(m_vz))
        printf("merda isnan(m_vx) || isnan(m_vz) 1\n");

    // ritorno a vel coord mondo
    m_vx = +cosf * vxm + sinf * vzm;
    m_vy = vym;
    m_vz = -sinf * vxm + cosf * vzm;

    if (isnan(m_vx) || isnan(m_vz))
        printf("merda isnan(m_vx) || isnan(m_vz) 2\n");


    m_colliders->Update(m_px + m_vx, m_py + m_vy, m_pz + m_vz, m_facing, &(m_currentMesh->bbmin), &(m_currentMesh->bbmax));

    CollisionInfo collisionInfo = m_colliders->RequestCollisionCheck();
    CollisionInfo::Direction direction = collisionInfo.GetDirection();
    if (direction == CollisionInfo::Direction::NONE) {
        // posizione = posizione + velocita * delta t (ma delta t e' costante)
        m_px += m_vx;
        m_py += m_vy;
        m_pz += m_vz;
    } else {
        // rimbalzo della pecora quando incontra il recinto
        if (direction == CollisionInfo::Direction::Z_MAX || direction & CollisionInfo::Direction::Z_MIN) {
            if (m_facing > 180)
                m_facing += (360 - m_facing) * 2;
            else if (m_facing < 180)
                m_facing -= m_facing * 2;
            m_facing += 180;
            m_vz *= -1;
        } else if (direction == CollisionInfo::Direction::X_MAX || direction & CollisionInfo::Direction::X_MIN) {
            if (m_facing < 90 || m_facing > 270)
                m_facing += (360 - m_facing) * 2;
            else if (m_facing > 90 && m_facing < 270)
                m_facing -= m_facing * 2;
            m_vx *= -1;
        }

        if (!(direction & CollisionInfo::Direction::Z_MAX) && !(direction & CollisionInfo::Direction::Z_MIN)) {
            m_pz += m_vz;
            m_px -= m_vx;
        } else if (!(direction & CollisionInfo::Direction::X_MAX) && !(direction & CollisionInfo::Direction::X_MIN)) {
            m_px += m_vx;
            m_pz -= m_vz;
        }

        m_py += m_vy;
        m_colliders->Update(m_px, m_py, m_pz, m_facing, &(m_currentMesh->bbmin), &(m_currentMesh->bbmax));
    }
}

// https://sinepost.wordpress.com/2012/08/19/bouncing-off-the-walls/

void Sheep::Render() {
    // sono nello spazio mondo

    glPushMatrix();

    glTranslatef(m_px, m_py, m_pz);
    glScalef(1, 1, -1);
    glRotatef(-m_facing, 0, 1, 0);

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, Sheep::m_texId);
    glColor3f(1, 1, 1); // metto il colore neutro (viene moltiplicato col colore texture, componente per componente)
    glDisable(GL_LIGHTING); // disabilito il lighting OpenGL standard (lo faccio con la texture)

    m_currentMesh->RenderNxVTex();

    if (Settings::Instance().useShadow) {
        glDisable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);

        glPushMatrix();
        glColor4f(0.5, 0.5, 0.5, 0.8);
        glTranslatef(0, 0.006f, 0);
        glScalef(1.01, 0, 1.01);
        m_currentMesh->RenderNxV();
        glPopMatrix();

        glPushMatrix();
        glColor4f(0.4, 0.4, 0.4, 0.7);
        glColor4f(0.5, 0.5, 0.5, 0.8);
        glTranslatef(0, 0.009f, 0);
        glScalef(0.8, 0, 1.01);
        m_currentMesh->RenderNxV();
        glPopMatrix();
        glDisable(GL_BLEND);
    }

    glPopMatrix();


    if (Settings::Instance().useBoundingBox)
        m_colliders->Render();

    m_firstDoStepAfterRender = true;
}