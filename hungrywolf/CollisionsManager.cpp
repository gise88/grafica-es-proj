/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   CollisionsManager.cpp
 * Author: ise
 *
 * Created on March 13, 2017, 11:14 AM
 */

#include <string.h>
#include <vector>


#include "Settings.h"
#include "CollisionsManager.h"



CollisionsManager::CollisionsManager() {
}

CollisionsManager::~CollisionsManager() {
}

Sheep *CollisionsManager::WolfCollisions(Wolf *wolf, std::list<Sheep *> sheepList) {

    Colliders *wolfColliders = wolf->GetColliders();
    for (Sheep *sheep : sheepList) {
        CollisionInfo info = CheckBetweenColliders(wolfColliders, sheep->GetColliders());
        if (info.GetDirection() != CollisionInfo::Direction::NONE)
            return sheep;
    }
    return nullptr;
}

CollisionInfo CollisionsManager::CheckBetweenColliders(Colliders *c1, Colliders *c2) {

    std::vector<SphereCollider> *c1Spheres;
    std::vector<SphereCollider> *c2Spheres;
    SphereCollider *c1Container = c1->GetSphereColliders(&c1Spheres);
    SphereCollider *c2Container = c2->GetSphereColliders(&c2Spheres);
    CollisionInfo collisionInfo(CollisionInfo::Direction::NONE);

    if (CheckCollision(c1Container, c2Container, nullptr)) {
        for (SphereCollider &sc1 : *c1Spheres) {

            if (Settings::Instance().useOnlyHeadCollider && strcmp(sc1.tag, "HEAD") != 0)
                continue;

            for (SphereCollider &sc2 : *c2Spheres) {
                if (CheckCollision(&sc1, &sc2, &collisionInfo)) {
                    collisionInfo.SetColliderSphere(&sc1);
                    return collisionInfo;
                }
            }
        }
    }

    return collisionInfo;
}

CollisionInfo CollisionsManager::CheckBetweenColliders(Colliders *c, BoxCollider *bc) {
    std::vector<SphereCollider> *spheres;
    SphereCollider *cContainer = c->GetSphereColliders(&spheres);
    Point3 bbmin, bbmax;
    bc->GetBoundingBox(&bbmin, &bbmax);
    CollisionInfo collisionInfo(CollisionInfo::Direction::NONE);

    if (CheckCollision(cContainer, &bbmin, &bbmax, nullptr)) {
        for (SphereCollider &sc : *spheres) {
            if (CheckCollision(&sc, &bbmin, &bbmax, &collisionInfo)) {
                collisionInfo.SetColliderSphere(&sc);
            }
        }
    }

    return collisionInfo;
}


float CollisionsManager::CalculateDistance(SphereCollider *s1, SphereCollider *s2) {
    float diffX = s1->x - s2->x;
    float diffY = s1->y - s2->y;
    float diffZ = s1->z - s2->z;
    return sqrt(diffX * diffX + diffY * diffY + diffZ * diffZ);
}

bool CollisionsManager::CheckCollision(SphereCollider *s1, SphereCollider *s2, CollisionInfo *i) {
    if (i == nullptr)
        return CalculateDistance(s1, s2) < s1->radius + s2->radius;

    if (CalculateDistance(s1, s2) < s1->radius + s2->radius) {

        if (s1->z <= s2->x && s1->z + s1->radius >= s2->x - s2->radius)
            i->SetDirection(CollisionInfo::Direction::Z_MAX);
        else if (s1->z >= s2->x && s1->z - s1->radius <= s2->x + s2->radius)
            i->SetDirection(CollisionInfo::Direction::Z_MIN);
        else if (s1->x <= s2->x && s1->x + s1->radius >= s2->x - s2->radius)
            i->SetDirection(CollisionInfo::Direction::X_MAX);
        else if (s1->x >= s2->x && s1->x - s1->radius <= s2->x + s2->radius)
            i->SetDirection(CollisionInfo::Direction::X_MIN);

        return true;
    }
    i->SetDirection(CollisionInfo::Direction::NONE);
    return false;
}

bool CollisionsManager::CheckCollision(SphereCollider *s, Point3 *bbmin, Point3 *bbmax, CollisionInfo *i) {

    bool colliding = false;
    CollisionInfo::Direction directionMask = CollisionInfo::Direction::NONE;

    if (s->z - s->radius <= bbmax->Z()) {
        directionMask |= CollisionInfo::Direction::Z_MAX;
        colliding = true;
    } else if (s->z + s->radius >= bbmin->Z()) {
        directionMask |= CollisionInfo::Direction::Z_MIN;
        colliding = true;
    }
    if (s->x + s->radius >= bbmax->X()) {
        directionMask |= CollisionInfo::Direction::X_MAX;
        colliding = true;
    } else if (s->x - s->radius <= bbmin->X()) {
        directionMask |= CollisionInfo::Direction::X_MIN;
        colliding = true;
    }
    if (i != nullptr)
        i->SetDirection(directionMask);
    return colliding;
}
