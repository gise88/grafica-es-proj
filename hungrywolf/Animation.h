/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Animation.h
 * Author: ise
 *
 * Created on March 04, 2017, 11:12 AM
 */

#ifndef HUNGRYWOLF_ANIMATION_H
#define HUNGRYWOLF_ANIMATION_H


#include <GL/gl.h>
#include <GL/glu.h>

#include <cstdlib>
#include <string>
#include <vector>

#include "mesh.h"


class Animation {
public:

    Animation(std::vector<Mesh *> animVec);

    virtual ~Animation();

    void SetRegularDirection(bool regular);
    void SetCycleAnimation(bool cycle);

    bool GetNextMesh(Mesh **current);


    static std::vector<Mesh *> LoadAnimationMeshes(const char* pattern, int start, int end, float scale);

    static std::string formatAnimationFilename(long num);

private:

    std::vector<Mesh *> m_anim;
    int m_index;
    int m_size;

    bool m_regularDirection;
    bool m_cycleAnimation;
};


#endif /* HUNGRYWOLF_ANIMATION_H */
