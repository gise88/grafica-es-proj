/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Rigidbody.h
 * Author: ise
 *
 * Created on March 15, 2017, 12:34 AM
 */

#ifndef HUNGRYWOLF_RIGIDBODY_H
#define HUNGRYWOLF_RIGIDBODY_H


class Rigidbody {
public:
    Rigidbody();

    virtual ~Rigidbody();

    void SetPositionAndFacing(float x, float y, float z, float facing);

    float GetX();
    float GetY();
    float GetZ();

    float GetFacing();

protected:

    float m_px;
    float m_py;
    float m_pz;

    float m_facing;

};


#endif /* HUNGRYWOLF_RIGIDBODY_H */
