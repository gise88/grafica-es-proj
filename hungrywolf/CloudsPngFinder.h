/*
 * Copyright (C) 2016 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* 
 * File:   CloudsPngFinder.h
 * Author: ise
 *
 * Created on October 26, 2016, 1:48 PM
 */

#ifndef HUNGRYWOLF_CLOUDSPNGFINDER_H
#define HUNGRYWOLF_CLOUDSPNGFINDER_H


#include "FilesFinder.h"



class CloudsPngFinder : public FilesFinder {
public:
    CloudsPngFinder();

    virtual ~CloudsPngFinder();
};

#endif /* HUNGRYWOLF_CLOUDSPNGFINDER_H */

