/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   Combo.h
 * Author: ise
 *
 * Created on March 25, 2017, 11:12 AM
 */

#ifndef HUNGRYWOLF_COMBO_H
#define HUNGRYWOLF_COMBO_H

#include <GL/gl.h>
#include <vector>
#include <list>

#include "Timer.h"
#include "Settings.h"


class Combo {
public:
    Combo();

    virtual ~Combo();

    void IncrementComboValue();

    void Reset();

    void Render();

private:

    GLuint m_comboTexId;
    GLuint m_multiplierTexId;
    std::vector<GLuint> m_texIdNumsVec;

    float m_comboWidth;
    float m_comboHeight;

    float m_xWidth;
    float m_xHeight;

    float m_digitWidth;
    float m_digitHeight;

    float m_alphaColor;

    int m_comboValue;

    Settings *m_settings;

    Timer m_timer;

    std::list<int> IntToDigits(int x);
};


#endif /* HUNGRYWOLF_COMBO_H */
