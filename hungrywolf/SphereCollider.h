/*
 * Copyright (C) 2017 Gianluca Iselli <gianluca.iselli@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * File:   SphereCollider.h
 * Author: ise
 *
 * Created on March 15, 2017, 2:39 AM
 */

#ifndef HUNGRYWOLF_SPHERECOLLIDER_H
#define HUNGRYWOLF_SPHERECOLLIDER_H


#define SPHERE_COLLIDER_CONTAINER_TAG   "container"
#define MAX_TAG_LENGTH 20


class SphereCollider {
public:

    float x;
    float y;
    float z;
    float radius;
    char tag[MAX_TAG_LENGTH];

    SphereCollider(float _x, float _y, float _z, float _radius);

    SphereCollider(float _x, float _y, float _z, float _radius, const char *_tag);

    SphereCollider(SphereCollider *sphereCollider);

    void Set(float _x, float _y, float _z, float _radius);

    void Set(float _x, float _y, float _z, float _radius, const char *_tag);
};



#endif /* HUNGRYWOLF_SPHERECOLLIDER_H */
