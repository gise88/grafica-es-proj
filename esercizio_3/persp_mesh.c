#include <stdio.h>
#include <math.h>
#include "../GCGraLib2/GCGraLib2.h"

#define MAXVERT (100)
#define MAXEDGE (200)
#define DEFAULT_PTSIZE  12



#define EDGE_INSIDE_VIEW 0
#define EDGE_OUT_OF_VIEW 1

typedef unsigned char BOOL;

#define TRUE 1
#define FALSE 0

#define OBS_S 15
#define OBS_D 50
#define OBS_PHI 20
#define OBS_THETA 20

enum {
	WIN_QUIT,
	WIN_TETA,
	WIN_PHI,
	WIN_ZOOMIN,
	WIN_ZOOMOUT,
	WIN_DZOOMIN,
	WIN_DZOOMOUT,
	WIN_RIGHT,
	WIN_LEFT,
	WIN_UP,
	WIN_DOWN,
	WIN_VIEWUP,

	WIN_COUNT
};

typedef struct {
	float x;
	float y;
	float z;
	int type;
} VERTEX;

typedef struct {
	float x;
	float y;
} POINT;

typedef struct {
	int v1;
	int v2;
} EDGE;

typedef struct {
	POINT p1;
	POINT p2;
} SCREEN_EDGE_OBS;

typedef struct {
	SDL_Rect rect;
	char text[10];
} RECT;



/* Dati per visualizzare un oggetto definito solo da vertici e lati*/
/* coordinate vertici */
int nvert=0, nedge=0; /* numero vertici e lati */
VERTEX vertices[MAXVERT];
EDGE edges[MAXEDGE];
SCREEN_EDGE_OBS obs_edges[MAXEDGE];
int nobs_edges;

VERTEX view_up;
VERTEX obj_center; /* centro oggetto */

VERTEX viewpoint[4];


float D = 25, theta = 20, phi = 20;
int xvmin, xvmax, yvmin, yvmax; /* coordinate viewport */
int bxmin, bxmax, bymin, bymax; /* bounds precisi della box di disegno */


/* passo di incremento D */
#define DSTEP 2.5 

/* zoomin e zoomout */
#define SALPHA 0.05

/* step di quanto viene mosso il centro oggetto */
#define STEP_SIZE 0.25

float alpha; /* apertura angolare piramide di vista */

RECT menu[WIN_COUNT];

void init_menu_entry(int index, int x, int y, int w, int h, char* text) {
	menu[index].rect.x = x;
	menu[index].rect.y = y;
	menu[index].rect.w = w;
	menu[index].rect.h = h;
	strcpy(menu[index].text, text);
}

/* inizializzazione menu */
void init_menu(SDL_Renderer *ren, RECT menu[], float theta, float phi) {

	init_menu_entry(WIN_TETA, 605, 5, 110, 110, "THETA");
	init_menu_entry(WIN_PHI, 605, 120, 110, 110, "PHI");
	init_menu_entry(WIN_VIEWUP, 605, 235, 110, 110, "VIEW UP");

	init_menu_entry(WIN_ZOOMIN, 602, 355, 50, 30, "ZoomI");
	init_menu_entry(WIN_ZOOMOUT, 662, 355, 50, 30, "ZoomO");
	init_menu_entry(WIN_DZOOMIN, 602, 395, 50, 30, "DZoomI");
	init_menu_entry(WIN_DZOOMOUT, 662, 395, 50, 30, "DZoomO");

	init_menu_entry(WIN_UP, 631, 435, 50, 30, "UP");
	init_menu_entry(WIN_RIGHT, 662, 470, 50, 30, "RIGHT");
	init_menu_entry(WIN_LEFT, 602, 470, 50, 30, "LEFT");
	init_menu_entry(WIN_DOWN, 631, 505, 50, 30, "DOWN");
	init_menu_entry(WIN_QUIT, 605, 555, 110, 40, "QUIT");

	SDL_RenderDrawLine(ren, menu[WIN_TETA].rect.x + menu[WIN_TETA].rect.w / 2, menu[WIN_TETA].rect.y + menu[WIN_TETA].rect.h / 2,
			(int) (menu[WIN_TETA].rect.w / 2) * cos(theta) + menu[WIN_TETA].rect.x + menu[WIN_TETA].rect.w / 2,
			(int) (menu[WIN_TETA].rect.h / 2) * sin(theta) + menu[WIN_TETA].rect.y + menu[WIN_TETA].rect.h / 2);

	SDL_RenderDrawLine(ren, menu[WIN_PHI].rect.x + menu[WIN_PHI].rect.w / 2, menu[WIN_PHI].rect.y + menu[WIN_PHI].rect.h / 2,
			(int) (menu[WIN_PHI].rect.w / 2) * cos(phi) + menu[WIN_PHI].rect.x + menu[WIN_PHI].rect.w / 2,
			(int) (menu[WIN_PHI].rect.h / 2) * sin(phi) + menu[WIN_PHI].rect.y + menu[WIN_PHI].rect.h / 2);

	SDL_RenderDrawLine(ren, menu[WIN_VIEWUP].rect.x + menu[WIN_VIEWUP].rect.w / 2, menu[WIN_VIEWUP].rect.y + menu[WIN_VIEWUP].rect.h / 2,
			(int) (menu[WIN_VIEWUP].rect.w / 2) * cos(-2) + menu[WIN_VIEWUP].rect.x + menu[WIN_VIEWUP].rect.w / 2,
			(int) (menu[WIN_VIEWUP].rect.h / 2) * sin(-2) + menu[WIN_VIEWUP].rect.y + menu[WIN_VIEWUP].rect.h / 2);
}

void draw_menu(RECT menu[], SDL_Renderer *ren, TTF_Font *font) {
	int i;

	for (i = 0; i <= WIN_COUNT; i++) {
		GC_DrawText(ren, font, 0, 0, 0, 0, 255, 255, 255, 0, menu[i].text, menu[i].rect.x, menu[i].rect.y, shaded);
		//GC_DrawRect(ren,menu[i].rect.x,menu[i].rect.y,menu[i].rect.w,menu[i].rect.h);
		SDL_RenderDrawRect(ren, &(menu[i].rect));
	}
}

int is_in_viewport(SDL_Rect *vp, int x, int y) {
	return x > vp->x && x < vp->x + vp->w && y > vp->y && y < vp->y + vp->h;
}

/* funzione di determinazione opzione del menu' scelta */
void opt_menu(RECT menu[], int n, int ix, int iy, int *choice) {
	int i;

	*choice = -1;
	for (i = 0; i <= n; i++) {
		if (ix >= menu[i].rect.x && iy >= menu[i].rect.y &&
				ix - menu[i].rect.x <= menu[i].rect.w && iy - menu[i].rect.y <= menu[i].rect.h)
			*choice = i;
	}
}

void add_vertex(float x, float y, float z) {
	vertices[nvert].x = x;
	vertices[nvert].y = y;
	vertices[nvert].z = z;
	nvert++;
}

void add_edge(int v1, int v2) {
	edges[nedge].v1 = v1;
	edges[nedge].v2 = v2;
	nedge++;
}

void copy_trasl(int nv, int ne, float dx, float dy, float dz) {
	int i, tmpnvert = nvert;

	for (i = 0; i < nv; i++) {
		add_vertex(vertices[i].x + dx, vertices[i].y + dy, vertices[i].z + dz);
	}

	for (i = 0; i < ne; i++) {
		add_edge(edges[i].v1 + tmpnvert, edges[i].v2 + tmpnvert);
	}
}

/* funzione che definisce l'oggetto mesh */
void define_cube() {

	/* Determino le coordinate dei vertici dell'oggetto */
	add_vertex(-1.0, -1.0, -1.0);
	add_vertex(1.0, -1.0, -1.0);
	add_vertex(1.0, 1.0, -1.0);
	add_vertex(-1.0, 1.0, -1.0);
	add_vertex(-1.0, -1.0, 1.0);
	add_vertex(1.0, -1.0, 1.0);
	add_vertex(1.0, 1.0, 1.0);
	add_vertex(-1.0, 1.0, 1.0);

	/* Determino la lista dei lati dell'oggetto */
	add_edge(0, 1);
	add_edge(1, 2);
	add_edge(2, 3);
	add_edge(3, 0);
	add_edge(4, 5);
	add_edge(5, 6);
	add_edge(6, 7);
	add_edge(7, 4);
	add_edge(0, 4);
	add_edge(1, 5);
	add_edge(2, 6);
	add_edge(3, 7);

	/*diagonale faccia*/
	add_edge(1, 6);

	copy_trasl(8, 13, 2.0, 0.0, 0.0);
	copy_trasl(8, 13, 0.0, 2.0, 0.0);
	copy_trasl(8, 13, 0.0, 0.0, 2.0);
}

// https://en.wikipedia.org/wiki/List_of_common_coordinate_transformations#To_Cartesian_coordinates

void from_spherical_to_cartesian_coords(VERTEX *vp, float D, float phi, float theta) {
	vp->x = D * sin(phi) * cos(theta);
	vp->y = D * sin(phi) * sin(theta);
	vp->z = D * cos(phi);
}

#define normalize_vertex3(v) {  \
	double norm = sqrt(pow((v).x, 2) + pow((v).y, 2) + pow((v).z, 2));  \
	(v).x = (v).x / norm;   \
	(v).y = (v).y / norm;   \
	(v).z = (v).z / norm;   \
}

#define cross_product_vertex3(a, b, r, sign) { \
	(r).x = sign((a).y * (b).z - (a).z * (b).y); \
	(r).y = sign((a).z * (b).x - (a).x * (b).z); \
	(r).z = sign((a).x * (b).y - (a).y * (b).x); \
	normalize_vertex3(r); \
}

void evaluate_depth(VERTEX *vp, VERTEX *object, VERTEX *r) {
	r->x = -(vp->x - object->x);
	r->y = -(vp->y - object->y);
	r->z = -(vp->z - object->z);
	normalize_vertex3(*r);
}

void trasf_prosp_gen_view_up_vect(float x, float y, float z, float *xe, float *ye, float *ze, VERTEX *view_up, float D, float phi, float theta) {
	VERTEX vp;
	VERTEX vx, vy, vz;

	from_spherical_to_cartesian_coords(&vp, D, phi, theta);
	evaluate_depth(&vp, &obj_center, &vz);
	cross_product_vertex3(vz, *view_up, vx, +);
	cross_product_vertex3(vz, vx, vy, -);

	*xe = vx.x * x + vx.y * y + vx.z * z + 0;
	*ye = vy.x * x + vy.y * y + vy.z * z + 0;
	*ze = vz.x * x + vz.y * y + vz.z * z + D;
}

/* funzione che determina i fattori di scala */
void define_view() {
	int k;
	float r;
	float xmin, xmax, ymin, ymax, zmin, zmax;
	float s;

	xmin = vertices[0].x;
	ymin = vertices[0].y;
	zmin = vertices[0].z;
	xmax = xmin;
	ymax = ymin;
	zmax = zmin;
	for (k = 1; k < nvert; k++) {
		if (vertices[k].x > xmax) xmax = vertices[k].x;
		else if (vertices[k].x < xmin) xmin = vertices[k].x;
		if (vertices[k].y > ymax) ymax = vertices[k].y;
		else if (vertices[k].y < ymin) ymin = vertices[k].y;
		if (vertices[k].z > zmax) zmax = vertices[k].z;
		else if (vertices[k].z < zmin) zmin = vertices[k].z;
	}
	/* centro oggetto iniziale */
	obj_center.x = (xmin + xmax) / 2;
	obj_center.y = (ymin + ymax) / 2;
	obj_center.z = (zmin + zmax) / 2;
	/* raggio sfera contenente oggetto */
	r = sqrt(powf(xmax - obj_center.x, 2) + powf(ymax - obj_center.y, 2) + powf(zmax - obj_center.z, 2));
	/* calcolo semilato window iniziale; il piano di proiezione viene assunto
	   ad una distanza D dall'osservatore */
	s = r * D / sqrt(powf(D, 2) - powf(r, 2));
	/* in base al semilato window cosi' calcolato si determina l'apertura
	   angolare iniziale */
	alpha = atan(s / D);
	printf("\nApertura angolare: %f \n", alpha);
}

void define_viewpoint(VERTEX *view_up) {
	VERTEX vx, vy, vz;

	from_spherical_to_cartesian_coords(&viewpoint[0], D, phi, theta);
	evaluate_depth(&viewpoint[0], &obj_center, &vz);
	viewpoint[1].x = viewpoint[0].x + vz.x;
	viewpoint[1].y = viewpoint[0].y + vz.y;
	viewpoint[1].z = viewpoint[0].z + vz.z;

	cross_product_vertex3(vz, *view_up, vx, +);
	viewpoint[2].x = viewpoint[0].x + vx.x;
	viewpoint[2].y = viewpoint[0].y + vx.y;
	viewpoint[2].z = viewpoint[0].z + vx.z;

	cross_product_vertex3(vz, vx, vy, -);
	viewpoint[3].x = viewpoint[0].x + vy.x;
	viewpoint[3].y = viewpoint[0].y + vy.y;
	viewpoint[3].z = viewpoint[0].z + vy.z;
}

int clip_edge(POINT *sCoords1, POINT *sCoords2, POINT *p1, POINT *p2) {

	if (sCoords1->y >= sCoords2->y) {
		memcpy(p1, sCoords1, sizeof(POINT));
		memcpy(p2, sCoords2, sizeof(POINT));
	} else {
		memcpy(p1, sCoords2, sizeof(POINT));
		memcpy(p2, sCoords1, sizeof(POINT));
	}

	if ((p1->x >= bxmax && p2->x >= bxmax) ||
			(p1->x <= bxmin && p2->x <= bxmin) ||
			(p1->y >= bymax && p2->y >= bymax) ||
			(p1->y <= bymin && p2->y <= bymin))
		return EDGE_OUT_OF_VIEW;
	
	float dist = sqrt(pow(p1->x - p2->x, 2) + pow(p1->y - p2->y, 2));
	float angle = asin((p1->y - p2->y) / dist);

	if (p1->x >= bxmax) {
		float deltax = p1->x - bxmax;
		float alpha = fabs(angle);
		p1->x = bxmax;
		p1->y -= (deltax / cos(alpha)) * sin(alpha);
	} else if (p1->x <= bxmin) {
		float deltax = bxmin - p1->x;
		float alpha = fabs(angle);
		p1->x = bxmin;
		p1->y -= (deltax / cos(alpha)) * sin(alpha);
	}

	if (p2->x >= bxmax) {
		float deltax = p2->x - bxmax;
		float alpha = -fabs(angle);
		p2->x = bxmax;
		p2->y -= (deltax / cos(alpha)) * sin(alpha);
	} else if (p2->x <= bxmin) {
		float deltax = bxmin - p2->x;
		float alpha = -fabs(angle);
		p2->x = bxmin;
		p2->y -= (deltax / cos(alpha)) * sin(alpha);
	}

	angle = asin((p1->x - p2->x) / dist);
	if (p1->y > bymax) {
		float deltay = p1->y - bymax;
		p1->y -= deltay;
		p1->x -= (deltay / cos(angle)) * sin(angle);
	}
	if (p2->y < bymin) {
		float deltay = bymin - p2->y;
		p2->y += deltay;
		p2->x += (deltay / cos(angle)) * sin(angle);
	}

	return EDGE_INSIDE_VIEW;
}

void init_observer_view() {
	int k;
	float xe, ye, ze; /* coord. dell'osservatore */

	POINT screenCoords[MAXVERT]; /* coordinate schermo */

	float Sx, Sy; /* fattori di scala trasf. window-viewport */
	float xwmin, xwmax, ywmin, ywmax; /* coordinate window */
	
	xwmin = -OBS_S;
	xwmax = OBS_S;
	ywmin = -OBS_S;
	ywmax = OBS_S;

	/* fattori di scala per trasf. Window-Viewport */
	Sx = ((float) (xvmax) - (float) (xvmin)) / (xwmax - xwmin);
	Sy = ((float) (yvmax) - (float) (yvmin)) / (ywmax - ywmin);

	/* il piano di proiezione viene definito a passare per l'origine */
	for (k = 0; k < nvert; k++) {
		trasf_prosp_gen_view_up_vect(vertices[k].x - obj_center.x, vertices[k].y - obj_center.y, vertices[k].z - obj_center.z, &xe, &ye, &ze, &view_up, OBS_D, OBS_PHI, OBS_THETA);
		/* proiezione e trasformazione in coordinate schermo */
		screenCoords[k].x = (int) (Sx * ((OBS_D * xe) / ze - xwmin) + xvmin + 0.5);
		screenCoords[k].y = (int) (Sy * (ywmin - (OBS_D * ye) / ze) + yvmax + 0.5);
	}

	/* disegno mesh con clipping */
	for (k = 0, nobs_edges = 0; k < nedge; k++, nobs_edges++) {
		POINT p1, p2;
		if (clip_edge(&(screenCoords[edges[k].v1]), &(screenCoords[edges[k].v2]), &p1, &p2) == EDGE_OUT_OF_VIEW)
			continue;
		
		obs_edges[nobs_edges].p1.x = p1.x;
		obs_edges[nobs_edges].p1.y = p1.y;
		obs_edges[nobs_edges].p2.x = p2.x;
		obs_edges[nobs_edges].p2.y = p2.y;
	}
}

void draw_viewpoint(SDL_Renderer *ren, VERTEX *view_obs) {
	int x1, y1, x2, y2;
	float xe, ye, ze;
	float Sx, Sy; /* fattori di scala trasf. window-viewport */
	float xwmin, xwmax, ywmin, ywmax; /* coordinate window */
	
	/* si ricalcola il semilato della window in base a D e ad alpha */
	xwmin = -OBS_S;
	xwmax = OBS_S;
	ywmin = -OBS_S;
	ywmax = OBS_S;
	
	/* fattori di scala per trasf. Window-Viewport */
	Sx = ((float) (xvmax) - (float) (xvmin)) / (xwmax - xwmin);
	Sy = ((float) (yvmax) - (float) (yvmin)) / (ywmax - ywmin);

	trasf_prosp_gen_view_up_vect(viewpoint[0].x - obj_center.x, viewpoint[0].y - obj_center.y, viewpoint[0].z - obj_center.z, &xe, &ye, &ze, view_obs, OBS_D, OBS_PHI, OBS_THETA);
	x1 = (int) (Sx * ((D * xe) / ze - xwmin) + xvmin + 0.5);
	y1 = (int) (Sy * (ywmin - (D * ye) / ze) + yvmax + 0.5);

	trasf_prosp_gen_view_up_vect(viewpoint[1].x - obj_center.x, viewpoint[1].y - obj_center.y, viewpoint[1].z - obj_center.z, &xe, &ye, &ze, view_obs, OBS_D, OBS_PHI, OBS_THETA);
	x2 = (int) (Sx * ((D * xe) / ze - xwmin) + xvmin + 0.5);
	y2 = (int) (Sy * (ywmin - (D * ye) / ze) + yvmax + 0.5);
	SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
	SDL_RenderDrawLine(ren, x1, y1, x2, y2);

	trasf_prosp_gen_view_up_vect(viewpoint[2].x - obj_center.x, viewpoint[2].y - obj_center.y, viewpoint[2].z - obj_center.z, &xe, &ye, &ze, view_obs, OBS_D, OBS_PHI, OBS_THETA);
	x2 = (int) (Sx * ((D * xe) / ze - xwmin) + xvmin + 0.5);
	y2 = (int) (Sy * (ywmin - (D * ye) / ze) + yvmax + 0.5);
	SDL_SetRenderDrawColor(ren, 0, 255, 0, 255);
	SDL_RenderDrawLine(ren, x1, y1, x2, y2);

	trasf_prosp_gen_view_up_vect(viewpoint[3].x - obj_center.x, viewpoint[3].y - obj_center.y, viewpoint[3].z - obj_center.z, &xe, &ye, &ze, view_obs, OBS_D, OBS_PHI, OBS_THETA);
	x2 = (int) (Sx * ((D * xe) / ze - xwmin) + xvmin + 0.5);
	y2 = (int) (Sy * (ywmin - (D * ye) / ze) + yvmax + 0.5);
	SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
	SDL_RenderDrawLine(ren, x1, y1, x2, y2);

}

void draw_observer_mesh(SDL_Renderer *ren) {
	static VERTEX view_obs = {0, 0, 1};
	
	int k;
	
	for (k = 0; k < nobs_edges; k++) {
		SDL_RenderDrawLine(ren, obs_edges[k].p1.x, obs_edges[k].p1.y, obs_edges[k].p2.x, obs_edges[k].p2.y);
	}
	
	define_viewpoint(&view_up);
	draw_viewpoint(ren, &view_obs);
}

void draw_mesh(SDL_Renderer *ren) {
	int k;
	float xe, ye, ze; /* coord. dell'osservatore */
	float s;

	POINT screenCoords[MAXVERT]; /* coordinate schermo */

	float Sx, Sy; /* fattori di scala trasf. window-viewport */
	float xwmin, xwmax, ywmin, ywmax; /* coordinate window */

	/* si ricalcola il semilato della window in base a D e ad alpha */
	s = D * tan(alpha);

	xwmin = -s;
	xwmax = s;
	ywmin = -s;
	ywmax = s;

	/* fattori di scala per trasf. Window-Viewport */
	Sx = ((float) (xvmax) - (float) (xvmin)) / (xwmax - xwmin);
	Sy = ((float) (yvmax) - (float) (yvmin)) / (ywmax - ywmin);

	/* il piano di proiezione viene definito a passare per l'origine */
	for (k = 0; k < nvert; k++) {
		trasf_prosp_gen_view_up_vect(vertices[k].x - obj_center.x, vertices[k].y - obj_center.y, vertices[k].z - obj_center.z, &xe, &ye, &ze, &view_up, D, phi, theta);
		/* proiezione e trasformazione in coordinate schermo */
		screenCoords[k].x = (int) (Sx * ((D * xe) / ze - xwmin) + xvmin + 0.5);
		screenCoords[k].y = (int) (Sy * (ywmin - (D * ye) / ze) + yvmax + 0.5);
	}

	/* disegno mesh con clipping */
	for (k = 0; k < nedge; k++) {
		POINT p1, p2;
		if (clip_edge(&(screenCoords[edges[k].v1]), &(screenCoords[edges[k].v2]), &p1, &p2) == EDGE_OUT_OF_VIEW)
			continue;
		
		SDL_RenderDrawLine(ren, p1.x, p1.y, p2.x, p2.y);
	}
}

void redraw_scenes(SDL_Renderer *ren, SDL_Rect *viewport, SDL_Renderer *ren_obs, SDL_Rect *viewport_obs) {
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	SDL_RenderFillRect(ren, viewport);
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);

	draw_mesh(ren);
	SDL_RenderPresent(ren);
	
	
	SDL_SetRenderDrawColor(ren_obs, 255, 255, 255, 255);
	SDL_RenderFillRect(ren_obs, viewport_obs);
	SDL_SetRenderDrawColor(ren_obs, 0, 0, 0, 255);

	draw_observer_mesh(ren_obs);	
	SDL_RenderPresent(ren_obs);
}

void draw_theta_phi_box(SDL_Renderer *ren, TTF_Font *font, SDL_Rect *sub_v, int win_type, Sint32 mouse_x, Sint32 mouse_y) {
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	SDL_RenderFillRect(ren, &(menu[win_type].rect));
	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	GC_DrawText(ren, font, 0, 0, 0, 0, 255, 255, 255, 0, menu[win_type].text, menu[win_type].rect.x, menu[win_type].rect.y, shaded);
	GC_DrawRect(ren, menu[win_type].rect.x, menu[win_type].rect.y, menu[win_type].rect.w, menu[win_type].rect.h);
	SDL_RenderDrawRect(ren, &(menu[win_type].rect));
	SDL_RenderDrawLine(ren, mouse_x, mouse_y, menu[win_type].rect.x + menu[win_type].rect.w / 2, menu[win_type].rect.y + menu[win_type].rect.h / 2);
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	SDL_RenderFillRect(ren, sub_v);
}

void init_rect(SDL_Rect *rect, int x, int y, int w, int h) {
	rect->x = x;
	rect->y = y;
	rect->w = w;
	rect->h = h;
}

int main() {
	SDL_Window *win, *win_obs;
	SDL_Renderer *ren, *ren_obs;
	SDL_Rect main_view, viewport, viewport_borders, viewport_obs;
	TTF_Font *font;
	SDL_Event myevent;
	int choice, quit_program = 0;
	float ssx, ssy, ssz;
	float tmpx, tmpy;
	BOOL redraw_needed = FALSE;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "Couldn't init video: %s\n", SDL_GetError());
		return (1);
	}

	/* Initialize the TTF library */
	if (TTF_Init() < 0) {
		fprintf(stderr, "Couldn't initialize TTF: %s\n", SDL_GetError());
		SDL_Quit();
		return (2);
	}

	font = TTF_OpenFont("FreeSans.ttf", DEFAULT_PTSIZE);
	if (font == NULL) {
		fprintf(stderr, "Couldn't load font\n");
	}

	init_rect(&main_view, 0, 0, 720, 600);
	init_rect(&viewport, main_view.x + 5, main_view.y + 5, main_view.w - 130, main_view.h - 10);
	init_rect(&viewport_borders, viewport.x - 1, viewport.y - 1, viewport.w + 2, viewport.h + 2);

	viewport_obs.x = 0;
	viewport_obs.y = 0;
	viewport_obs.w = viewport.w;
	viewport_obs.h = viewport.h;
	
	xvmin = viewport.x;
	yvmin = viewport.y;
	xvmax = viewport.w;
	yvmax = viewport.h;

	bxmin = xvmin;
	bxmax = xvmax + xvmin;
	bymin = yvmin;
	bymax = yvmax + yvmin;

	view_up.x = 0;
	view_up.y = 0;
	view_up.z = 1;

	win = SDL_CreateWindow("View Cube Model", 0, 0, main_view.w, main_view.h, SDL_WINDOW_SHOWN);
	if (win == NULL) {
		fprintf(stderr, "SDL_CreateWindow Error: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		fprintf(stderr, "SDL_CreateRenderer Error: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	// new window for observer
	win_obs = SDL_CreateWindow("Observer", 740, 0, viewport_obs.w, viewport_obs.h, SDL_WINDOW_SHOWN);
	if (win_obs == NULL) {
		fprintf(stderr, "SDL_CreateWindow Error: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}
	ren_obs = SDL_CreateRenderer(win_obs, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren_obs == NULL) {
		SDL_DestroyWindow(win_obs);
		fprintf(stderr, "SDL_CreateRenderer Error: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	SDL_Delay(100);

	//Set color
	SDL_SetRenderDrawColor(ren, 255, 255, 255, 255);
	//Clear back buffer
	SDL_RenderClear(ren);
	//Swap back and front buffer
	SDL_RenderPresent(ren);

	//Set color
	SDL_SetRenderDrawColor(ren_obs, 255, 255, 255, 255);
	//Clear back buffer
	SDL_RenderClear(ren_obs);
	//Swap back and front buffer
	SDL_RenderPresent(ren_obs);

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderDrawRect(ren, &viewport);

	init_menu(ren, menu, theta, phi);
	draw_menu(menu, ren, font);
	define_cube(); /* determinazione mesh oggetto */
	define_view(); /*calcolo dei parametri di vista */

	init_observer_view(ren_obs);
	redraw_scenes(ren, &viewport, ren_obs, &viewport_obs);

	//Disegnare tutte le volte, anche per eventi intermedi,
	//rallenta tutto senza dare un contributo all'immagine finale.
	//Gli eventi si possono scremare in questo modo:
	//viene usata la costante 0xff perchè nel codice sorgente delle SDL è usato
	//come costante all'interno delle funzioni che accettano interi 8 bit come input
	//per rappresentare SDL_ALLEVENTS (che invece e' a 32 bit)
	while (quit_program == 0) {
		redraw_needed = FALSE;
		if (SDL_WaitEvent(&myevent)) {
			SDL_EventState(0xff, SDL_IGNORE); //0xff is all events
			switch (myevent.type) {
				case SDL_QUIT:
					quit_program = 1;
					break;
				case SDL_MOUSEMOTION:
					if (myevent.motion.state == SDL_PRESSED) {

						if (is_in_viewport(&viewport, myevent.motion.x, myevent.motion.y)) {
							theta -= 0.005 * myevent.motion.xrel;
							phi -= 0.005 * myevent.motion.yrel;
							redraw_needed = TRUE;
							break;
						}

						opt_menu(menu, WIN_COUNT, myevent.motion.x, myevent.motion.y, &choice);
						switch (choice) {
							case WIN_TETA:
								draw_theta_phi_box(ren, font, &viewport, WIN_TETA, myevent.motion.x, myevent.motion.y);

								tmpx = myevent.motion.x - (menu[WIN_TETA].rect.x + menu[WIN_TETA].rect.w / 2);
								tmpy = myevent.motion.y - (menu[WIN_TETA].rect.y + menu[WIN_TETA].rect.h / 2);
								theta = atan2(tmpy, tmpx);

								redraw_needed = TRUE;
								break;
							case WIN_PHI:
								draw_theta_phi_box(ren, font, &viewport, WIN_PHI, myevent.motion.x, myevent.motion.y);

								tmpx = myevent.motion.x - (menu[WIN_PHI].rect.x + menu[WIN_PHI].rect.w / 2);
								tmpy = myevent.motion.y - (menu[WIN_PHI].rect.y + menu[WIN_PHI].rect.h / 2);
								phi = atan2(tmpy, tmpx);

								redraw_needed = TRUE;
								break;
							case WIN_VIEWUP:
								draw_theta_phi_box(ren, font, &viewport, WIN_VIEWUP, myevent.motion.x, myevent.motion.y);

								view_up.x = myevent.motion.x - (menu[WIN_VIEWUP].rect.x + menu[WIN_VIEWUP].rect.w / 2);
								view_up.y = myevent.motion.y - (menu[WIN_VIEWUP].rect.y + menu[WIN_VIEWUP].rect.h / 2);
								
								redraw_needed = TRUE;
								break;
						}
					}
					break;

				case SDL_MOUSEBUTTONDOWN:
					if (myevent.button.button == SDL_BUTTON_LEFT) {
						opt_menu(menu, WIN_COUNT, myevent.button.x, myevent.button.y, &choice);
						switch (choice) {
							case WIN_QUIT:
								quit_program = 1;
								break;
							case WIN_RIGHT:
								ssx = -sin(theta);
								ssy = cos(theta);
								obj_center.x -= STEP_SIZE * ssx;
								obj_center.y -= STEP_SIZE * ssy;
								redraw_needed = TRUE;
								break;
							case WIN_LEFT:
								ssx = -sin(theta);
								ssy = cos(theta);
								obj_center.x += STEP_SIZE * ssx;
								obj_center.y += STEP_SIZE * ssy;
								redraw_needed = TRUE;
								break;
							case WIN_UP:
								ssx = -cos(phi) * cos(theta);
								ssy = -cos(phi) * sin(theta);
								ssz = sin(phi);
								obj_center.x += STEP_SIZE * ssx;
								obj_center.y += STEP_SIZE * ssy;
								obj_center.z += STEP_SIZE * ssz;
								redraw_needed = TRUE;
								break;
							case WIN_DOWN:
								ssx = -cos(phi) * cos(theta);
								ssy = -cos(phi) * sin(theta);
								ssz = sin(phi);
								obj_center.x -= STEP_SIZE * ssx;
								obj_center.y -= STEP_SIZE * ssy;
								obj_center.z -= STEP_SIZE * ssz;
								redraw_needed = TRUE;
								break;
							case WIN_ZOOMIN:
								if (alpha - SALPHA > 0)
									alpha -= SALPHA;
								redraw_needed = TRUE;
								break;
							case WIN_ZOOMOUT:
								if (alpha + SALPHA < 1.57)
									alpha += SALPHA;
								redraw_needed = TRUE;
								break;
							case WIN_DZOOMIN:
								if (D - DSTEP > 0)
									D -= DSTEP;
								redraw_needed = TRUE;
								break;
							case WIN_DZOOMOUT:
								D += DSTEP;
								redraw_needed = TRUE;
								break;
						}
					}
					break;
				case SDL_KEYDOWN:
					if (myevent.key.keysym.sym == SDLK_ESCAPE)
						quit_program = 1;
					break;
			}/* fine switch */
			SDL_EventState(0xff, SDL_ENABLE);
		}
		if (redraw_needed)
			redraw_scenes(ren, &viewport, ren_obs, &viewport_obs);
	} /* fine while */

	TTF_CloseFont(font);
	TTF_Quit();
	SDL_DestroyRenderer(ren);
	SDL_DestroyRenderer(ren_obs);
	SDL_DestroyWindow(win);
	SDL_DestroyWindow(win_obs);
	SDL_Quit();
	return (0);

} /* fine main */

