#include <stdio.h>
#include <string.h>
#include <math.h>
#include "../GCGraLib2/GCGraLib2.h"

#define DEFAULT_PTSIZE  18

#define BG_R 255
#define BG_G 255
#define BG_B 255

typedef struct RECT RECT;

struct RECT {
	float xmin, xmax, ymin, ymax;
};

typedef RECT VIEWPORT;
typedef RECT WINDOW;

void get_scale(RECT r1, RECT r2, float *scx, float *scy) {
	*scx = (r1.xmax - r1.xmin) / (r2.xmax - r2.xmin);
	*scy = (r1.ymax - r1.ymin) / (r2.ymax - r2.ymin);
}

void wind_view(float px, float py, int *ix, int *iy, VIEWPORT view, WINDOW win) {
	float scx, scy;
	get_scale(view, win, &scx, &scy);
	*ix = 0.5 + scx * (px - win.xmin) + view.xmin;
	*iy = 0.5 + scy * (win.ymin - py) + view.ymax;
}

void assi(SDL_Renderer* ren, VIEWPORT view, WINDOW win) {
	int ipx, ipy;
	wind_view(0, 0, &ipx, &ipy, view, win);
	SDL_RenderDrawLine(ren, ipx, view.ymin, ipx, view.ymax);
	SDL_RenderDrawLine(ren, view.xmin, ipy, view.xmax, ipy);
}

void draw_data(SDL_Renderer* ren, int n, VIEWPORT view, WINDOW win, float x[], float y[]) {
	int i;
	int x1, x2, y1, y2;
	float px, py;

	px = x[1];
	py = y[1];
	wind_view(px, py, &x1, &y1, view, win);

	for (i = 2; i <= n; i++) {
		px = x[i];
		py = y[i];

		wind_view(px, py, &x2, &y2, view, win);
		SDL_RenderDrawLine(ren, x1, y1, x2, y2);

		x1 = x2;
		y1 = y2;
	}

	//assi (ren, view, win);
}

int read_curve_param(WINDOW* rect, float **x, float **y) {
	int n;
	int i;
	float t;
	float z;
	float a, b, c, d, eb, ed; 
	
	printf("Param a :");
	scanf("%f", &a);
	
	printf("Param b :");
	scanf("%f", &b);
	
	printf("Param c :");
	scanf("%f", &c);
	
	printf("Param d :");
	scanf("%f", &d);
	
	printf("Exp b :");
	scanf("%f", &eb);
	
	printf("Exp d :");
	scanf("%f", &ed);
	
	printf("N :");
	scanf("%d", &n);

		
	*x = (float *)malloc(n * sizeof(float));
	*y = (float *)malloc(n * sizeof(float));
	
	z = 10.0 / (float)n;
	
	for (i=0; i<n; i++) {
		
		t = z * i;
		
		// https://en.wikipedia.org/wiki/Parametric_equation#/media/File:Param33_1.jpg
		(*x)[i] = cos(a * t) - pow(cos(b * t), eb);
		(*y)[i] = sin(c * t) - pow(sin(d * t), ed);
	}
	
#define PADDING 0.3
	
	rect->xmin = (*x)[0] - PADDING;
	rect->xmax = (*x)[0] + PADDING;
	rect->ymin = (*y)[0] - PADDING;
	rect->ymax = (*y)[0] + PADDING;
	for (i = 1; i <= n; i++) {
		if ((*x)[i] < rect->xmin + PADDING) rect->xmin = (*x)[i] - PADDING;
		else if ((*x)[i] > rect->xmax - PADDING) rect->xmax = (*x)[i] + PADDING;

		if ((*y)[i] < rect->ymin + PADDING) rect->ymin = (*y)[i] - PADDING;
		else if ((*y)[i] > rect->ymax - PADDING) rect->ymax = (*y)[i] + PADDING;
	}
	
	return n;
}

int main(void) {
	SDL_Window *win;
	SDL_Renderer *ren;
	SDL_Rect sub_v, windowsRect;
	TTF_Font *font;
	WINDOW fun_win;
	VIEWPORT fun_view;
	char ch;

	if (SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "Couldn't init video: %s\n", SDL_GetError());
		return (1);
	}

	/* Initialize the TTF library */
	if (TTF_Init() < 0) {
		fprintf(stderr, "Couldn't initialize TTF: %s\n", SDL_GetError());
		SDL_Quit();
		return (2);
	}

	font = TTF_OpenFont("FreeSans.ttf", DEFAULT_PTSIZE);
	if (font == NULL) {
		fprintf(stderr, "Couldn't load font\n");
	}

	windowsRect.x = 0;
	windowsRect.y = 0;
	windowsRect.w = 680;
	windowsRect.h = 440;

	win = SDL_CreateWindow("Draw Param Curve", windowsRect.x, windowsRect.y, windowsRect.w, windowsRect.h, SDL_WINDOW_SHOWN);
	if (win == NULL) {
		fprintf(stderr, "SDL_CreateWindow Error: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	ren = SDL_CreateRenderer(win, 0, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == NULL) {
		SDL_DestroyWindow(win);
		fprintf(stderr, "SDL_CreateRenderer Error: %s\n", SDL_GetError());
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);
	SDL_RenderPresent(ren);

	/* Workaround to initialize the view with my nvidia driver */
	SDL_Delay(100);

	SDL_SetRenderDrawColor(ren, BG_R, BG_G, BG_B, 255);
	SDL_RenderFillRect(ren, &windowsRect);
	SDL_SetRenderDrawColor(ren, 0, BG_G, BG_B, 255);
	GC_DrawText(ren, font, 255, 0, 0, 255, BG_R, BG_G, BG_B, 255, "Grafico Polilinea", 10, 10, shaded);
	SDL_SetRenderDrawColor(ren, BG_R, BG_G, BG_B, 255);

	sub_v.x = windowsRect.x + 10;
	sub_v.y = windowsRect.y + 50;
	sub_v.w = windowsRect.w - 20;
	sub_v.h = windowsRect.h - 60;

	fun_view.xmin = sub_v.x;
	fun_view.xmax = sub_v.x + sub_v.w - 1;
	fun_view.ymin = sub_v.y;
	fun_view.ymax = sub_v.y + sub_v.h - 1;


	SDL_SetRenderDrawColor(ren, 240, 240, 240, 255);
	SDL_RenderFillRect(ren, &sub_v);

	SDL_RenderPresent(ren);
		
	SDL_SetRenderDrawColor(ren, 0, 0, 255, 255);
	do {
		float *x, *y;
		int n = read_curve_param(&fun_win, &x, &y);
		draw_data(ren, n, fun_view, fun_win, x, y);

		SDL_RenderPresent(ren);

		free(x);
		free(y);
		printf("\n   <n> or <N> NEW DATA   <any-key> EXIT : ");
		scanf("%c%*c", &ch);
	} while ((ch == 'N') || (ch == 'n'));

	TTF_CloseFont(font);
	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	TTF_Quit();
	SDL_Quit();
	return (0);
}
